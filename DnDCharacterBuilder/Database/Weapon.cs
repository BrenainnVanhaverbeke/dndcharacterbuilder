//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DnDCharacterBuilder.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class Weapon
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Weapon()
        {
            this.Classes = new HashSet<Class>();
            this.Races = new HashSet<Race>();
        }
    
        public int WeaponId { get; set; }
        public string WeaponName { get; set; }
        public string WeaponType { get; set; }
        public bool Ranged { get; set; }
        public int Cost { get; set; }
        public string Damage { get; set; }
        public string DamageType { get; set; }
        public float Weight { get; set; }
        public string Properties { get; set; }
        public Nullable<int> Background_BackgroundId { get; set; }
    
        public virtual Background Background { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Class> Classes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Race> Races { get; set; }
    }
}
