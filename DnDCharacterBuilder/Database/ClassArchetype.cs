//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DnDCharacterBuilder.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class ClassArchetype
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ClassArchetype()
        {
            this.Features = new HashSet<Feature>();
        }
    
        public int ClassArchetypeId { get; set; }
        public string ClassArchetypeName { get; set; }
        public Nullable<int> Class_ClassId { get; set; }
    
        public virtual Class Class { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Feature> Features { get; set; }

        public override string ToString()
        {
            return ClassArchetypeName;
        }
    }
}
