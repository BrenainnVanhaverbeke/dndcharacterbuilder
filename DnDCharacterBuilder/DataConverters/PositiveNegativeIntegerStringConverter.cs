﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace DnDCharacterBuilder.DataConverters
{
    class PositiveNegativeIntegerStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int valueToConvert = (int)value;
            return string.Format("{0}{1}", GetSymbol(valueToConvert), valueToConvert);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private string GetSymbol(int number)
        {
            return (0 < number) ? "+" : "";
        }
    }
}
