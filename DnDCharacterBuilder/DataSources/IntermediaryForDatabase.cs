﻿using DnDCharacterBuilder.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Net;

namespace DnDCharacterBuilder.DataSources
{
    public class IntermediaryForDatabase
    {
        public List<Race> Races
        {
            get
            {
                using (DnDDatabaseEntities db = new DnDDatabaseEntities())
                {
                    return db.Races.Include(r => r.Features).Include(r => r.AbilityBonus).Include(r => r.AbilityBonus.Select(a => a.Ability)).ToList();
                }
            }
        }

        public List<Class> Classes
        {
            get
            {
                using (DnDDatabaseEntities db = new DnDDatabaseEntities())
                {
                    return db.Classes.Include(c => c.Features.Select(cf => cf.ClassArchetype)).Include(c => c.ClassArchetypes).ToList();
                }
            }
        }

        public List<Background> Backgrounds
        {
            get
            {
                using (DnDDatabaseEntities db = new DnDDatabaseEntities())
                {
                    return db.Backgrounds.Include(b => b.Feature).ToList();
                }
            }
        }

        public List<Skill> Skills
        {
            get
            {
                using (DnDDatabaseEntities db = new DnDDatabaseEntities())
                {
                    return db.Skills.Include(s => s.Ability).ToList();
                }
            }
        }

        public List<Ability> Abilities
        {
            get
            {
                using (DnDDatabaseEntities db = new DnDDatabaseEntities())
                {
                    return db.Abilities.OrderBy(a => a.AbilityId).ToList();
                }
            }
        }
    }
}