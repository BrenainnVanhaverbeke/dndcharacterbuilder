﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnDCharacterBuilder.Database;
using System.ComponentModel;
using System.Windows.Input;

namespace DnDCharacterBuilder.DataSources
{
    public class Character : INotifyPropertyChanged
    {
        #region Variables

        //Will be used to pull level specific features, calculate rough amount of HP
        private int level;
        private int proficiencyBonus;
        private Race characterRace;
        private Class characterClass;
        private ClassArchetype characterClassArchetype;
        private Background characterBackground;
        private List<CharacterAbility> characterAbilities;
        private List<CharacterSkill> characterSkills;
        private List<CharacterSavingThrow> characterSavingThrows;

        #endregion Variables

        #region Constructors

        public Character()
        {
            characterAbilities = new List<CharacterAbility>();
            characterSkills = new List<CharacterSkill>();
            characterSavingThrows = new List<CharacterSavingThrow>();
            Level = 1;
            InitialiseCharacter();
        }

        #endregion Constructors

        #region Properties

        public int Level
        {
            get { return level; }
            set
            {
                level = value;
                ProficiencyBonus = GetProficiencyBonus(value);
                NotifyPropertyChanged("Level");
                NotifyPropertyChanged("CharacterClassFeatures");
            }
        }

        public int ProficiencyBonus
        {
            get { return proficiencyBonus; }
            set
            {
                proficiencyBonus = value;
                UpdateBonusses();
                NotifyPropertyChanged("ProficiencyBonus");
            }
        }

        public Race CharacterRace
        {
            get { return characterRace; }
            set
            {
                characterRace = value;
                SetRacialAbilityBonus(value);
                NotifyPropertyChanged("CharacterRace");
                NotifyPropertyChanged("CharacterRacialFeatures");
            }
        }

        public Class CharacterClass
        {
            get { return characterClass; }
            set
            {
                characterClass = value;
                NotifyPropertyChanged("CharacterClass");
                NotifyPropertyChanged("CharacterClassFeatures");
            }
        }

        public ClassArchetype CharacterClassArchetype
        {
            get { return characterClassArchetype; }
            set
            {
                characterClassArchetype = value;
                NotifyPropertyChanged("CharacterClassArchetype");
                NotifyPropertyChanged("CharacterClassFeatures");
            }
        }

        public Background CharacterBackground
        {
            get { return characterBackground; }
            set
            {
                characterBackground = value;
                NotifyPropertyChanged("CharacterBackground");
                NotifyPropertyChanged("CharacterBackgroundFeature");
            }
        }

        public List<CharacterAbility> CharacterAbilities
        {
            get { return characterAbilities; }
        }

        public List<CharacterSkill> CharacterSkills
        {
            get { return characterSkills; }
        }

        public List<CharacterSavingThrow> CharacterSavingThrows
        {
            get { return characterSavingThrows; }
        }

        public List<Feature> CharacterRacialFeatures
        {
            get { return CharacterRace.Features.ToList(); }
        }

        public List<Feature> CharacterBackgroundFeature
        {
            get { return new List<Feature> { CharacterBackground.Feature }; }
        }

        public List<Feature> CharacterClassFeatures
        {
            get
            {
                return CharacterClass.Features.Where(cf => cf.RequiredLevel <= Level && (cf.ClassArchetype == null || cf.ClassArchetype.ClassArchetypeId == CharacterClassArchetype.ClassArchetypeId)).ToList();
            }
        }

        #endregion Properties

        #region Behavior

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void SetRacialAbilityBonus(Race race)
        {
            foreach (CharacterAbility characterAbility in CharacterAbilities)
            {
                characterAbility.RacialBonus = 0;
                foreach (AbilityBonu abilityBonus in race.AbilityBonus)
                {
                    if (characterAbility.Ability.AbilityId == abilityBonus.AbilityId)
                    {
                        characterAbility.RacialBonus = abilityBonus.Value;
                    }
                }
            }
        }

        private void InitialiseCharacter()
        {
            IntermediaryForDatabase db = new IntermediaryForDatabase();
            InitialiseAbilities(db);
            InitialiseSkills(db);
            InitialiseSavingThrows(db);
        }

        private void InitialiseAbilities(IntermediaryForDatabase db)
        {
            foreach (Ability ability in db.Abilities)
            {
                characterAbilities.Add(new CharacterAbility(this, ability));
            }
        }

        private void InitialiseSkills(IntermediaryForDatabase db)
        {
            foreach (Skill skill in db.Skills)
            {
                characterSkills.Add(new CharacterSkill(this, skill));
            }
        }

        private void InitialiseSavingThrows(IntermediaryForDatabase db)
        {
            foreach (Ability savingThrow in db.Abilities)
            {
                characterSavingThrows.Add(new CharacterSavingThrow(this, savingThrow));
            }
        }

        internal void UpdateBonusses()
        {
            UpdateSkillBonusses();
            UpdateSavingThrows();
        }

        private void UpdateSkillBonusses()
        {
            foreach (CharacterSkill characterSkill in CharacterSkills)
            {
                characterSkill.UpdateSkillBonus();
            }
        }

        private void UpdateSavingThrows()
        {
            foreach (CharacterSavingThrow characterSavingThrow in CharacterSavingThrows)
            {
                characterSavingThrow.UpdateSavingThrowBonus();
            }
        }

        private int GetProficiencyBonus(int level)
        {
            int[] proficiencyBonus = { 2, 3, 4, 5, 6 };
            return proficiencyBonus[(level - 1) / 4];
        }

        #endregion Behavior
    }
}