﻿using DnDCharacterBuilder.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnDCharacterBuilder.DataSources
{
    public class CharacterSavingThrow : INotifyPropertyChanged
    {
        #region Variables

        Character character;
        Ability savingThrow;
        bool proficient;
        int savingThrowBonus; 

        #endregion Variables

        #region Constructor

        public CharacterSavingThrow(Character character, Ability savingThrow)
        {
            this.character = character;
            this.savingThrow = savingThrow;
        }

        #endregion Constructor

        #region Properties

        public bool Proficient
        {
            get { return proficient; }
            set
            {
                proficient = value;
                UpdateSavingThrowBonus();
                NotifyPropertyChanged("Proficient");
            }
        }

        public string SavingThrowName { get { return savingThrow.AbilityName; } }

        public int SavingThrowBonus { get { return savingThrowBonus; } }

        #endregion Properties

        #region Behavior

        public event PropertyChangedEventHandler PropertyChanged;

        public void UpdateSavingThrowBonus()
        {
            CharacterAbility parentAbility = character.CharacterAbilities.Where(ca => ca.Ability.AbilityId == savingThrow.AbilityId).SingleOrDefault();
            savingThrowBonus = parentAbility.AbilityModifier + ((proficient) ? character.ProficiencyBonus : 0);
            NotifyPropertyChanged("SavingThrowBonus");
        }

        private void NotifyPropertyChanged(string propertyname)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyname));
        }

        #endregion Behavior
    }
}
