﻿using DnDCharacterBuilder.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnDCharacterBuilder.DataSources
{
    public class CharacterSkill : INotifyPropertyChanged
    {
        #region Variables

        Character character;
        Skill skill;
        bool proficient;
        int skillBonus;

        #endregion Variables

        #region Constructor

        public CharacterSkill(Character character, Skill skill)
        {
            this.character = character;
            this.skill = skill;
        }

        #endregion Constructor

        #region Properties

        public bool Proficient
        {
            get { return proficient; }
            set
            {
                proficient = value;
                UpdateSkillBonus();
                NotifyPropertyChanged("Proficient");
            }
        }

        public string SkillName
        {
            get { return string.Format("{0} ({1})",skill.SkillName, skill.Ability.AbilityName); } 
        }

        public int SkillBonus
        {
            get { return skillBonus; }
        }

        #endregion Properties

        #region Behavior

        public event PropertyChangedEventHandler PropertyChanged;

        public void UpdateSkillBonus()
        {
            CharacterAbility parentAbility = character.CharacterAbilities.Where(ca => ca.Ability.AbilityId == skill.Ability.AbilityId).SingleOrDefault();
            skillBonus = parentAbility.AbilityModifier + ((proficient) ? character.ProficiencyBonus : 0);
            NotifyPropertyChanged("SkillBonus");
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion Behavior
    }
}
