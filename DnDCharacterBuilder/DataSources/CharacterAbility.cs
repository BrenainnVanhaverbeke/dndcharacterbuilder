﻿using DnDCharacterBuilder.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnDCharacterBuilder.DataSources
{
    public class CharacterAbility : INotifyPropertyChanged
    {
        private readonly Character character;
        private readonly Ability ability;
        private int value;
        private int abilityModifier;
        private int racialBonus;
        private int totalValue;

        public CharacterAbility(Character character, Ability ability)
        {
            this.character = character;
            this.ability = ability;
            this.value = 8;
            this.abilityModifier = GetAbilityModifier(value);
            this.racialBonus = 0;
        }

        public Ability Ability
        {
            get { return ability;}
        }

        public int Value
        {
            get { return value; }
            set
            {
                this.value = value;
                TotalValue = value + racialBonus;
                NotifyPropertyChanged("Value");
            }
        }

        public int AbilityModifier
        {
            get { return abilityModifier; }
            set
            {
                abilityModifier = value;
                character.UpdateBonusses();
                NotifyPropertyChanged("AbilityModifier");
            }
        }

        public int RacialBonus
        {
            get { return racialBonus; }
            set
            {
                racialBonus = value;
                TotalValue = Value + value;
                NotifyPropertyChanged("RacialBonus");
            }
        }

        public int TotalValue
        {
            get { return totalValue; }
            set
            {
                totalValue = value;
                AbilityModifier = GetAbilityModifier(value);
                NotifyPropertyChanged("TotalValue");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private int GetAbilityModifier(int relatedAttribute)
        {
            //Lowest possible ability score is 0, highest possible ability is score is 30.
            int[] abilityModifiers = { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
             return abilityModifiers[relatedAttribute / 2];
        }

        public override bool Equals(object obj)
        {
            CharacterAbility comparingTo = obj as CharacterAbility;
            return comparingTo != null && comparingTo.Ability.AbilityId == Ability.AbilityId;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
