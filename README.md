This project started out of 2 reasons. The first being learning about .NET's WPF framework, and wanting to familiarise myself with the databinding system.
The second is that I wanted a tool that would help me keep track of my party's characters in DnD.

The current code works, as in ability/abilitybonus calculations work, as well as proficiency modifiers per level, as well as features per level/class/archetype.