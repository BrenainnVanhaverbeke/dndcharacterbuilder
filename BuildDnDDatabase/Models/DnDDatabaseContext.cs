﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace BuildDnDDatabase.Models
{
	class DnDDatabaseContext : DbContext
	{
		public DnDDatabaseContext() : this(false) { }

		public DnDDatabaseContext(bool enableLazyLoading) 
            : base("DnDDatabase")
		{
			Configuration.LazyLoadingEnabled = enableLazyLoading;
		}

		public DbSet<Race> Races { get; set; }
		public DbSet<Class> Classes { get; set; }
		public DbSet<ClassArchetype> ClassArchetypes { get; set; }
		public DbSet<Background> Backgrounds { get; set; }
		public DbSet<Ability> Abilities { get; set; }
		public DbSet<Skill> Skills { get; set; }
		public DbSet<Weapon> Weapons { get; set; }
		public DbSet<Tool> Tools { get; set; }
		public DbSet<Armor> Armors { get; set; }
		public DbSet<Language> Languages { get; set; }
		public DbSet<Feature> Features { get; set; }
        public DbSet<MovementSpeed> MovementSpeeds { get; set; }
        public DbSet<MovementType> MovementTypes { get; set; }
    }
}
