﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildDnDDatabase.Models
{
	class ClassArchetype
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ClassArchetypeId { get; set; }
		public string ClassArchetypeName { get; set; }

		//Navigation Properties

		public virtual Class Class { get; set; }
	}
}
