﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildDnDDatabase.Models
{
	class Weapon
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int WeaponId { get; set; }
		public string WeaponName { get; set; }
		public string WeaponType { get; set; }
		public bool Ranged { get; set; }
		public int Cost { get; set; }
		public string Damage { get; set; }
		public string DamageType { get; set; }
		public float Weight { get; set; }
		public string Properties { get; set; }

		//Navigation Properties
		public List<Class> ProficientClasses { get; set; }
		public List<Race> ProficientRaces { get; set; }
	}
}
