﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BuildDnDDatabase.Models
{
	class Ability
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int AbilityId { get; set; }
		public string AbilityName { get; set; }

		//NavigationProperties
		public virtual List<Class> ClassSavingthrows { get; set; }
		public virtual List<AbilityBonus> RacialBonus { get; set; }
		public virtual List<Skill> ParentOfSkill { get; set; }
	}
}
