﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildDnDDatabase.Models
{
	class Tool
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ToolId { get; set; }
		public string ToolName { get; set; }
		public string ToolType { get; set; }
		public int Cost { get; set; }
		public float Weight { get; set; }

		//Navigation Properties
		public List<Class> ProficientClasses { get; set; }
		public List<Background> ProficientBackgrounds { get; set; }
		public List<Race> ProficientRaces { get; set; }
	}
}
