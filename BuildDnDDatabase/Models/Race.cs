﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BuildDnDDatabase.Models
{
	class Race
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int RaceId { get; set; }
		public string RaceName { get; set; }
		public string Size { get; set; }

		//Navigation Properties

		public virtual List<MovementSpeed> MovementSpeeds { get; set; }
		public virtual List<Weapon> RaceWeaponProficiencies { get; set; }
		public virtual List<Armor> RaceArmorProficiencies { get; set; }
		public virtual List<Tool> RaceToolProficiencies { get; set; }
		public virtual List<Skill> RaceSkillProficiencies { get; set; }
		public virtual List<Feature> RacialFeatures { get; set; }
		public virtual List<Language> RacialLanguages { get; set; }
		public virtual List<AbilityBonus> AbilityBonusses { get; set; }
	}
}
