﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BuildDnDDatabase.Models;
using System.Data.Entity.Migrations;

namespace BuildDnDDatabase.Models
{
	class FillDb
	{
		const string bludgeoning = "Bludgeoning",
			         piercing = "Piercing",
			         slashing = "Slashing";

		public void Run()
		{
			//TODO: Figure out how to implement spellcasting per class/archetype and special points (sorcerery points, ki points, ..)
			using (DnDDatabaseContext db = new DnDDatabaseContext())
			{
                Console.WriteLine("Creating database...");
                db.SaveChanges();
                Console.WriteLine("Successfully created database!");
                #region Abilities
                Console.WriteLine("Adding Abilities...");
				Ability strength = new Ability { AbilityName = "Strength" };
				Ability dexterity = new Ability { AbilityName = "Dexterity" };
				Ability constitution = new Ability { AbilityName = "Constitution" };
				Ability intelligence = new Ability { AbilityName = "Intelligence" };
				Ability wisdom = new Ability { AbilityName = "Wisdom" };
				Ability charisma = new Ability { AbilityName = "Charisma" };
				db.Abilities.Add(strength);
				db.Abilities.Add(dexterity);
				db.Abilities.Add(constitution);
				db.Abilities.Add(intelligence);
				db.Abilities.Add(wisdom);
				db.Abilities.Add(charisma);
				db.SaveChanges();
                Console.WriteLine("Successfully added Abilities!");
                #endregion Abilities
                #region Skills
                Console.WriteLine("Adding Skills...");
				Skill acrobatics = new Skill { SkillName = "Acrobatics", ParentAbility = dexterity };
				Skill animalHandling = new Skill { SkillName = "Animal Handling", ParentAbility = wisdom };
				Skill arcana = new Skill { SkillName = "Arcana", ParentAbility = intelligence };
				Skill athletics = new Skill { SkillName = "Athletics", ParentAbility = strength };
				Skill deception = new Skill { SkillName = "Deception", ParentAbility = charisma };
				Skill history = new Skill { SkillName = "History", ParentAbility = intelligence };
				Skill insight = new Skill { SkillName = "Insight", ParentAbility = wisdom };
				Skill intimidation = new Skill { SkillName = "Intimidation", ParentAbility = charisma };
				Skill investigation = new Skill { SkillName = "Investigation", ParentAbility = intelligence };
				Skill medicine = new Skill { SkillName = "Medicine", ParentAbility = wisdom };
				Skill nature = new Skill { SkillName = "Nature", ParentAbility = intelligence };
				Skill perception = new Skill { SkillName = "Perception", ParentAbility = wisdom };
				Skill performance = new Skill { SkillName = "Performance", ParentAbility = charisma };
				Skill persuasion = new Skill { SkillName = "Persuasion", ParentAbility = charisma };
				Skill religion = new Skill { SkillName = "Religion", ParentAbility = intelligence };
				Skill sleightOfHand = new Skill { SkillName = "Sleight of Hand", ParentAbility = dexterity };
				Skill stealth = new Skill { SkillName = "Stealth", ParentAbility = dexterity };
				Skill survival = new Skill { SkillName = "Survival", ParentAbility = wisdom };
				db.Skills.Add(acrobatics);
				db.Skills.Add(animalHandling);
				db.Skills.Add(arcana);
				db.Skills.Add(athletics);
				db.Skills.Add(deception);
				db.Skills.Add(history);
				db.Skills.Add(insight);
				db.Skills.Add(intimidation);
				db.Skills.Add(investigation);
				db.Skills.Add(medicine);
				db.Skills.Add(nature);
				db.Skills.Add(perception);
				db.Skills.Add(performance);
				db.Skills.Add(persuasion);
				db.Skills.Add(religion);
				db.Skills.Add(sleightOfHand);
				db.Skills.Add(stealth);
				db.Skills.Add(survival);
				db.SaveChanges();
                Console.WriteLine("Successfully added Skills!");
                #endregion Skills
                #region Weapons
                Console.WriteLine("Adding Weapons...");
				Weapon club = new Weapon
				{
					WeaponName = "Club",
					WeaponType = "Simple",
					Ranged = false,
					Cost = 10,
					Damage = "1d4",
					DamageType = "Bludgeoning",
					Weight = 2,
					Properties = "Light"

				};
				Weapon dagger = new Weapon
				{
					WeaponName = "Dagger",
					WeaponType = "Simple",
					Ranged = false,
					Cost = 200,
					Damage = "1d4",
					DamageType = "Piercing",
					Weight = 1,
					Properties = "Light, Finesse, Thrown (range 20/60)"

				};
				Weapon greatclub = new Weapon
				{
					WeaponName = "Greatclub",
					WeaponType = "Simple",
					Ranged = false,
					Cost = 20,
					Damage = "1d8",
					DamageType = "Bludgeoning",
					Weight = 10,
					Properties = "Two-handed"

				};
				Weapon handaxe = new Weapon
				{
					WeaponName = "Handaxe",
					WeaponType = "Simple",
					Ranged = false,
					Cost = 500,
					Damage = "1d6",
					DamageType = "Slashing",
					Weight = 2,
					Properties = "Light, Thrown (range 20/60)"

				};
				Weapon javelin = new Weapon
				{
					WeaponName = "Javelin",
					WeaponType = "Simple",
					Ranged = false,
					Cost = 50,
					Damage = "1d6",
					DamageType = "Piercing",
					Weight = 2,
					Properties = "Thrown (range 30/120)"
				};
				Weapon lightHammer = new Weapon
				{
					WeaponName = "Light Hammer",
					WeaponType = "Simple",
					Ranged = false,
					Cost = 200,
					Damage = "1d4",
					DamageType = "Bludgeoning",
					Weight = 2,
					Properties = "Light, Thrown (range 20/60)"

				};
				Weapon mace = new Weapon
				{
					WeaponName = "Mace",
					WeaponType = "Simple",
					Ranged = false,
					Cost = 500,
					Damage = "1d6",
					DamageType = "Bludgeoning",
					Weight = 4,
					Properties = ""
				};
				Weapon quarterstaff = new Weapon
				{
					WeaponName = "Quarterstaff",
					WeaponType = "Simple",
					Ranged = false,
					Cost = 20,
					Damage = "1d6",
					DamageType = "Bludgeoning",
					Weight = 4,
					Properties = "Versatile (1d8)"

				};
				Weapon sickle = new Weapon
				{
					WeaponName = "Sickle",
					WeaponType = "Simple",
					Ranged = false,
					Cost = 100,
					Damage = "1d4",
					DamageType = "Slashing",
					Weight = 2,
					Properties = "Light"

				};
				Weapon spear = new Weapon
				{
					WeaponName = "Spear",
					WeaponType = "Simple",
					Ranged = false,
					Cost = 20,
					Damage = "1d6",
					DamageType = "Piercing",
					Weight = 3,
					Properties = "Thrown (range 20/60), Versatile (1d8)"

				};
				Weapon unarmedStrike = new Weapon
				{
					WeaponName = "Unarmed Strike",
					WeaponType = "Simple",
					Ranged = false,
					Cost = 0,
					Damage = "1",
					DamageType = "Bludgeoning",
					Weight = 0,
					Properties = ""

				};
				Weapon lightCrossbow = new Weapon
				{
					WeaponName = "Light Crossbow",
					WeaponType = "Simple",
					Ranged = true,
					Cost = 2500,
					Damage = "1d8",
					DamageType = "Piercing",
					Weight = 5,
					Properties = "Ammunition, Range 80/320, Loading, Two-handed"
				};
				Weapon dart = new Weapon
				{
					WeaponName = "Dart",
					WeaponType = "Simple",
					Ranged = true,
					Cost = 5,
					Damage = "1d4",
					DamageType = "Piercing",
					Weight = 0.25F,
					Properties = "Ammunition, Range 80/320, Loading, Two-handed"
				};
				Weapon shortbow = new Weapon
				{
					WeaponName = "Shortbow",
					WeaponType = "Simple",
					Ranged = true,
					Cost = 2500,
					Damage = "1d6",
					DamageType = "Piercing",
					Weight = 2,
					Properties = "Ammunition, Range 80/320, Two-handed"
				};
				Weapon sling = new Weapon
				{
					WeaponName = "Sling",
					WeaponType = "Simple",
					Ranged = true,
					Cost = 10,
					Damage = "1d4",
					DamageType = "Bludgeoning",
					Weight = 0,
					Properties = "Ammunition, Range 30/120"
				};
				Weapon battleaxe = new Weapon
				{
					WeaponName = "Battleaxe",
					WeaponType = "Martial",
					Ranged = false,
					Cost = 1000,
					Damage = "1d8",
					DamageType = "Slashing",
					Weight = 4,
					Properties = "Versatile (1d10)"
				};
				Weapon flail = new Weapon
				{
					WeaponName = "Flail",
					WeaponType = "Martial",
					Ranged = false,
					Cost = 1000,
					Damage = "1d8",
					DamageType = bludgeoning,
					Weight = 2,
					Properties = ""
				};
				Weapon glaive = new Weapon
				{
					WeaponName = "Glaive",
					WeaponType = "Martial",
					Ranged = false,
					Cost = 2000,
					Damage = "1d10",
					DamageType = slashing,
					Weight = 6,
					Properties = "Heavy, Reach, Two-handed"
				};
				Weapon greataxe = new Weapon
				{
					WeaponName = "Greataxe",
					WeaponType = "Martial",
					Ranged = false,
					Cost = 3000,
					Damage = "1d12",
					DamageType = slashing,
					Weight = 7,
					Properties = "Heavy, Two-handed"
				};
				Weapon greatsword = new Weapon
				{
					WeaponName = "Greatsword",
					WeaponType = "Martial",
					Ranged = false,
					Cost = 5000,
					Damage = "2d6",
					DamageType = slashing,
					Weight = 6,
					Properties = "Heavy, Two-handed"
				};
				Weapon halberd = new Weapon
				{
					WeaponName = "Halberd",
					WeaponType = "Martial",
					Ranged = false,
					Cost = 2000,
					Damage = "1d10",
					DamageType = slashing,
					Weight = 6,
					Properties = "Heavy, Reach, Two-handed"
				};
				Weapon lance = new Weapon
				{
					WeaponName = "Lance",
					WeaponType = "Martial",
					Ranged = false,
					Cost = 1000,
					Damage = "1d12",
					DamageType = piercing,
					Weight = 6,
					Properties = "Reach, Special"
				};
				Weapon longsword = new Weapon
				{
					WeaponName = "Longsword",
					WeaponType = "Martial",
					Ranged = false,
					Cost = 1500,
					Damage = "1d8",
					DamageType = slashing,
					Weight = 3,
					Properties = "Versatile (1d10)"
				};
				Weapon maul = new Weapon
				{
					WeaponName = "Maul",
					WeaponType = "Martial",
					Ranged = false,
					Cost = 1000,
					Damage = "2d6",
					DamageType = bludgeoning,
					Weight = 10,
					Properties = "Heavy, Two-handed"
				};
				Weapon morningstar = new Weapon
				{
					WeaponName = "Morningstar",
					WeaponType = "Martial",
					Ranged = false,
					Cost = 1500,
					Damage = "1d8",
					DamageType = bludgeoning,
					Weight = 4,
					Properties = ""
				};
				Weapon pike = new Weapon
				{
					WeaponName = "Pike",
					WeaponType = "Martial",
					Ranged = false,
					Cost = 500,
					Damage = "1d10",
					DamageType = piercing,
					Weight = 18,
					Properties = "Heavy, Reach, Two-handed"
				};
				Weapon rapier = new Weapon
				{
					WeaponName = "Rapier",
					WeaponType = "Martial",
					Ranged = false,
					Cost = 2500,
					Damage = "1d8",
					DamageType = piercing,
					Weight = 2,
					Properties = "Finesse"
				};
				Weapon scimitar = new Weapon
				{
					WeaponName = "Scimitar",
					WeaponType = "Martial",
					Ranged = false,
					Cost = 2500,
					Damage = "1d6",
					DamageType = slashing,
					Weight = 3,
					Properties = "Finesse, Light"
				};
				Weapon shortsword = new Weapon
				{
					WeaponName = "Shortsword",
					WeaponType = "Martial",
					Ranged = false,
					Cost = 1000,
					Damage = "1d6",
					DamageType = slashing,
					Weight = 2,
					Properties = "Finesse, Light"
				};
				Weapon trident = new Weapon
				{
					WeaponName = "Trident",
					WeaponType = "Martial",
					Ranged = false,
					Cost = 500,
					Damage = "1d6",
					DamageType = piercing,
					Weight = 4,
					Properties = "Thrown (range 20/60), Versatile (1d8)"
				};
				Weapon warPick = new Weapon
				{
					WeaponName = "War Pick",
					WeaponType = "Martial",
					Ranged = false,
					Cost = 500,
					Damage = "1d8",
					DamageType = piercing,
					Weight = 2,
					Properties = ""
				};
				Weapon warhammer = new Weapon
				{
					WeaponName = "Warhammer",
					WeaponType = "Martial",
					Ranged = false,
					Cost = 1500,
					Damage = "1d8",
					DamageType = bludgeoning,
					Weight = 2,
					Properties = "Versatile (1d10)"
				};
				Weapon whip = new Weapon
				{
					WeaponName = "Whip",
					WeaponType = "Martial",
					Ranged = false,
					Cost = 200,
					Damage = "1d4",
					DamageType = slashing,
					Weight = 3,
					Properties = "Finesse, Reach"
				};
				Weapon blowgun = new Weapon
				{
					WeaponName = "Blowgun",
					WeaponType = "Martial",
					Ranged = true,
					Cost = 1000,
					Damage = "1",
					DamageType = piercing,
					Weight = 1,
					Properties = "Ammunition, Range (25/100), Loading"
				};
				Weapon handCrossbow = new Weapon
				{
					WeaponName = "Hand Crossbow",
					WeaponType = "Martial",
					Ranged = true,
					Cost = 7500,
					Damage = "1d6",
					DamageType = piercing,
					Weight = 3,
					Properties = "Ammunition, Range (30/120), Light, Loading"
				};
				Weapon heavyCrossbow = new Weapon
				{
					WeaponName = "Heavy Crossbow",
					WeaponType = "Martial",
					Ranged = true,
					Cost = 5000,
					Damage = "1d10",
					DamageType = piercing,
					Weight = 18,
					Properties = "Ammunition, Range (100/400), Heavy, Loading, Two-handed"
				};
				Weapon longbow = new Weapon
				{
					WeaponName = "Longbow",
					WeaponType = "Martial",
					Ranged = true,
					Cost = 5000,
					Damage = "1d8",
					DamageType = piercing,
					Weight = 2,
					Properties = "Ammunition, Range (150/600), Heavy, Two-handed"
				};
				Weapon net = new Weapon
				{
					WeaponName = "Net",
					WeaponType = "Martial",
					Ranged = true,
					Cost = 1000,
					Damage = "-",
					DamageType = "-",
					Weight = 3,
					Properties = "Special, Thrown (range 5/15)"
				};
				db.Weapons.Add(club);
				db.Weapons.Add(dagger);
				db.Weapons.Add(greatclub);
				db.Weapons.Add(handaxe);
				db.Weapons.Add(javelin);
				db.Weapons.Add(lightHammer);
				db.Weapons.Add(mace);
				db.Weapons.Add(quarterstaff);
				db.Weapons.Add(sickle);
				db.Weapons.Add(spear);
				db.Weapons.Add(unarmedStrike);
				db.Weapons.Add(lightCrossbow);
				db.Weapons.Add(dart);
				db.Weapons.Add(shortbow);
				db.Weapons.Add(sling);
				db.Weapons.Add(battleaxe);
				db.Weapons.Add(flail);
				db.Weapons.Add(glaive);
				db.Weapons.Add(greataxe);
				db.Weapons.Add(greatsword);
				db.Weapons.Add(halberd);
				db.Weapons.Add(lance);
				db.Weapons.Add(longsword);
				db.Weapons.Add(maul);
				db.Weapons.Add(morningstar);
				db.Weapons.Add(pike);
				db.Weapons.Add(rapier);
				db.Weapons.Add(scimitar);
				db.Weapons.Add(shortsword);
				db.Weapons.Add(trident);
				db.Weapons.Add(warPick);
				db.Weapons.Add(warhammer);
				db.Weapons.Add(whip);
				db.Weapons.Add(blowgun);
				db.Weapons.Add(handCrossbow);
				db.Weapons.Add(heavyCrossbow);
				db.Weapons.Add(longbow);
				db.Weapons.Add(net);
				db.SaveChanges();
                Console.WriteLine("Successfully added Weapons!");
                #endregion Weapons
                #region Tools
                Console.WriteLine("Adding Tools...");
				Tool alchemistSupplies = new Tool
				{
					ToolName = "Alchemist's supplies",
					ToolType = "Artisan's tools",
					Cost = 5000,
					Weight = 8
				};
				Tool brewersSupplies = new Tool
				{
					ToolName = "Brewer's Supplies",
					ToolType = "Artisan's tools",
					Cost = 2000,
					Weight = 9
				};
				Tool calligrapherSupplies = new Tool
				{
					ToolName = "Calligrapher's supplies",
					ToolType = "Artisan's tools",
					Cost = 1000,
					Weight = 5
				};
				Tool carpenterTools = new Tool
				{
					ToolName = "Carpenter's tools",
					ToolType = "Artisan's tools",
					Cost = 800,
					Weight = 6
				};
				Tool cartographerTools = new Tool
				{
					ToolName = "Cartographer's tools",
					ToolType = "Artisan's tools",
					Cost = 1500,
					Weight = 6
				};
				Tool cobblerTools = new Tool
				{
					ToolName = "Cobbler's tools",
					ToolType = "Artisan's tools",
					Cost = 500,
					Weight = 5
				};
				Tool cookUtensils = new Tool
				{
					ToolName = "Cook's utensils",
					ToolType = "Artisan's tools",
					Cost = 100,
					Weight = 8
				};
				Tool glassblowerTools = new Tool
				{
					ToolName = "Glassblower's tools",
					ToolType = "Artisan's tools",
					Cost = 3000,
					Weight = 5
				};
				Tool jewelerTools = new Tool
				{
					ToolName = "Jeweler's tools",
					ToolType = "Artisan's tools",
					Cost = 2500,
					Weight = 2
				};
				Tool leatherworkerTools = new Tool
				{
					ToolName = "Leatherworker's tools",
					ToolType = "Artisan's tools",
					Cost = 500,
					Weight = 5
				};
				Tool masonTools = new Tool
				{
					ToolName = "Mason's tools",
					ToolType = "Artisan's tools",
					Cost = 1000,
					Weight = 8
				};
				Tool painterSupplies = new Tool
				{
					ToolName = "Painter's supplies",
					ToolType = "Artisan's tools",
					Cost = 1000,
					Weight = 5
				};
				Tool potterTools = new Tool
				{
					ToolName = "Potter's tools",
					ToolType = "Artisan's tools",
					Cost = 1000,
					Weight = 3
				};
				Tool smithTools = new Tool
				{
					ToolName = "Smith's tools",
					ToolType = "Artisan's tools",
					Cost = 2000,
					Weight = 8
				};
				Tool tinkerTools = new Tool
				{
					ToolName = "Tinker's tools",
					ToolType = "Artisan's tools",
					Cost = 5000,
					Weight = 10
				};
				Tool weaverTools = new Tool
				{
					ToolName = "Weaver's tools",
					ToolType = "Artisan's tools",
					Cost = 100,
					Weight = 5
				};
				Tool woodcarverTools = new Tool
				{
					ToolName = "Woodcarver's tools",
					ToolType = "Artisan's tools",
					Cost = 100,
					Weight = 5
				};
				Tool disguiseKit = new Tool
				{
					ToolName = "Disguise kit",
					ToolType = "Generic",
					Cost = 2500,
					Weight = 3
				};
				Tool forgeryKit = new Tool
				{
					ToolName = "Forgery kit",
					ToolType = "Generic",
					Cost = 1500,
					Weight = 5
				};
				Tool herbalismKit = new Tool
				{
					ToolName = "Herbalism kit",
					ToolType = "Generic",
					Cost = 500,
					Weight = 3
				};
				Tool navigatorTools = new Tool
				{
					ToolName = "Navigator's tools",
					ToolType = "Generic",
					Cost = 2500,
					Weight = 2
				};
				Tool poisonerKit = new Tool
				{
					ToolName = "Poisoner's kit",
					ToolType = "Generic",
					Cost = 5000,
					Weight = 2
				};
				Tool thievesTools = new Tool
				{
					ToolName = "Thieves' tools",
					ToolType = "Generic",
					Cost = 2500,
					Weight = 1
				};
				Tool diceSet = new Tool
				{
					ToolName = "Dice set",
					ToolType = "Gaming set",
					Cost = 10,
					Weight = 0
				};
				Tool dragonchessSet = new Tool
				{
					ToolName = "Dragonchess set",
					ToolType = "Gaming set",
					Cost = 100,
					Weight = 0.5F
				};
				Tool cardSet = new Tool
				{
					ToolName = "Playing card set",
					ToolType = "Gaming set",
					Cost = 50,
					Weight = 0
				};
				Tool threeDragonAnteSet = new Tool
				{
					ToolName = "Three-Dragon Ante set",
					ToolType = "Gaming set",
					Cost = 100,
					Weight = 0
				};
				Tool bagpipes = new Tool
				{
					ToolName = "Bagpipes",
					ToolType = "Musical instrument",
					Cost = 3000,
					Weight = 6
				};
				Tool drum = new Tool
				{
					ToolName = "Drum",
					ToolType = "Musical instrument",
					Cost = 600,
					Weight = 3
				};
				Tool dulcimer = new Tool
				{
					ToolName = "Dulcimer",
					ToolType = "Musical instrument",
					Cost = 2500,
					Weight = 10
				};
				Tool flute = new Tool
				{
					ToolName = "Flute",
					ToolType = "Musical instrument",
					Cost = 200,
					Weight = 1
				};
				Tool lute = new Tool
				{
					ToolName = "Lute",
					ToolType = "Musical instrument",
					Cost = 3500,
					Weight = 2
				};
				Tool lyre = new Tool
				{
					ToolName = "Lyre",
					ToolType = "Musical instrument",
					Cost = 3000,
					Weight = 6
				};
				Tool horn = new Tool
				{
					ToolName = "Horn",
					ToolType = "Musical instrument",
					Cost = 3000,
					Weight = 6
				};
				Tool panFlute = new Tool
				{
					ToolName = "Panflute",
					ToolType = "Musical instrument",
					Cost = 1200,
					Weight = 2
				};
				Tool shawm = new Tool
				{
					ToolName = "Shawm",
					ToolType = "Musical instrument",
					Cost = 200,
					Weight = 1
				};
				Tool viol = new Tool
				{
					ToolName = "Viol",
					ToolType = "Musical instrument",
					Cost = 3000,
					Weight = 1
				};
				Tool mount = new Tool
				{
					ToolName = "Mount",
					ToolType = "Vehicle, land",
					Cost = 0,
					Weight = 0
				};
				Tool vehicleLand = new Tool
				{
					ToolName = "Carriages",
					ToolType = "Vehicle, land",
					Cost = 0,
					Weight = 0
				};
				Tool vehicleWater = new Tool
				{
					ToolName = "Boats",
					ToolType = "Vehicle, water",
					Cost = 0,
					Weight = 0
				};
				db.Tools.Add(alchemistSupplies);
				db.Tools.Add(brewersSupplies);
				db.Tools.Add(calligrapherSupplies);
				db.Tools.Add(carpenterTools);
				db.Tools.Add(cartographerTools);
				db.Tools.Add(cobblerTools);
				db.Tools.Add(cookUtensils);
				db.Tools.Add(glassblowerTools);
				db.Tools.Add(jewelerTools);
				db.Tools.Add(leatherworkerTools);
				db.Tools.Add(masonTools);
				db.Tools.Add(painterSupplies);
				db.Tools.Add(potterTools);
				db.Tools.Add(smithTools);
				db.Tools.Add(tinkerTools);
				db.Tools.Add(weaverTools);
				db.Tools.Add(woodcarverTools);
				db.Tools.Add(disguiseKit);
				db.Tools.Add(forgeryKit);
				db.Tools.Add(diceSet);
				db.Tools.Add(dragonchessSet);
				db.Tools.Add(cardSet);
				db.Tools.Add(threeDragonAnteSet);
				db.Tools.Add(herbalismKit);
				db.Tools.Add(bagpipes);
				db.Tools.Add(drum);
				db.Tools.Add(dulcimer);
				db.Tools.Add(flute);
				db.Tools.Add(lute);
				db.Tools.Add(lyre);
				db.Tools.Add(horn);
				db.Tools.Add(panFlute);
				db.Tools.Add(shawm);
				db.Tools.Add(viol);
				db.Tools.Add(navigatorTools);
				db.Tools.Add(poisonerKit);
				db.Tools.Add(thievesTools);
				db.SaveChanges();
                Console.WriteLine("Succesfully added Tools!");
                #endregion Tools
                #region Armors
                Console.WriteLine("Adding Armors...");
				Armor padded = new Armor
				{
					ArmorName = "Padded",
					ArmorType = "Light",
					Cost = 500,
					AC = "11 + Dex modifier",
					Strength = 0,
					StealthDisadvantage = true,
					Weight = 8
				};
				Armor leather = new Armor
				{
					ArmorName = "Leather",
					ArmorType = "Light",
					Cost = 1000,
					AC = "11 + Dex modifier",
					Strength = 0,
					StealthDisadvantage = false,
					Weight = 10
				};
				Armor studdedLeather = new Armor
				{
					ArmorName = "Studded Leather",
					ArmorType = "Light",
					Cost = 4500,
					AC = "12 + Dex modifier",
					Strength = 0,
					StealthDisadvantage = false,
					Weight = 13
				};
				Armor hide = new Armor
				{
					ArmorName = "Hide",
					ArmorType = "Medium",
					Cost = 1000,
					AC = "12 + Dex modifier (max 2)",
					Strength = 0,
					StealthDisadvantage = false,
					Weight = 12
				};
				Armor chainShirt = new Armor
				{
					ArmorName = "Chain shirt",
					ArmorType = "Medium",
					Cost = 5000,
					AC = "13 + Dex modifier (max 2)",
					Strength = 0,
					StealthDisadvantage = false,
					Weight = 20
				};
				Armor scaleMail = new Armor
				{
					ArmorName = "Scale mail",
					ArmorType = "Medium",
					Cost = 5000,
					AC = "14 + Dex modifier (max 2)",
					Strength = 0,
					StealthDisadvantage = true,
					Weight = 45
				};
				Armor breastplate = new Armor
				{
					ArmorName = "Breastplate",
					ArmorType = "Medium",
					Cost = 40000,
					AC = "14 + Dex modifier (max 2)",
					Strength = 0,
					StealthDisadvantage = false,
					Weight = 20
				};
				Armor halfPlate = new Armor
				{
					ArmorName = "Half plate",
					ArmorType = "Medium",
					Cost = 75000,
					AC = "15 + Dex modifier (max 2)",
					Strength = 0,
					StealthDisadvantage = true,
					Weight = 40
				};
				Armor ringMail = new Armor
				{
					ArmorName = "Ring mail",
					ArmorType = "Heavy",
					Cost = 3000,
					AC = "14",
					Strength = 0,
					StealthDisadvantage = true,
					Weight = 40
				};
				Armor chainMail = new Armor
				{
					ArmorName = "Chain mail",
					ArmorType = "Heavy",
					Cost = 7500,
					AC = "16",
					Strength = 13,
					StealthDisadvantage = true,
					Weight = 55
				};
				Armor splint = new Armor
				{
					ArmorName = "Splint",
					ArmorType = "Heavy",
					Cost = 20000,
					AC = "17",
					Strength = 15,
					StealthDisadvantage = true,
					Weight = 60
				};
				Armor plate = new Armor
				{
					ArmorName = "Plate",
					ArmorType = "Heavy",
					Cost = 150000,
					AC = "18",
					Strength = 15,
					StealthDisadvantage = true,
					Weight = 65
				};
				Armor shield = new Armor
				{
					ArmorName = "Shield",
					ArmorType = "Shield",
					Cost = 1000,
					AC = "+2",
					Strength = 0,
					StealthDisadvantage = false,
					Weight = 6
				};
				db.Armors.Add(padded);
				db.Armors.Add(leather);
				db.Armors.Add(studdedLeather);
				db.Armors.Add(hide);
				db.Armors.Add(chainShirt);
				db.Armors.Add(scaleMail);
				db.Armors.Add(breastplate);
				db.Armors.Add(halfPlate);
				db.Armors.Add(ringMail);
				db.Armors.Add(chainMail);
				db.Armors.Add(splint);
				db.Armors.Add(plate);
				db.Armors.Add(shield);
				db.SaveChanges();
                Console.WriteLine("Successfully added Armors!");
                #endregion Armors
                #region Languages
                Console.WriteLine("Adding Languages...");
				Language common = new Language { LanguageName = "Common", ScriptType = "Common" };
				Language dwarvish = new Language { LanguageName = "Dwarvish", ScriptType = "Dwarvish" };
				Language elvish = new Language { LanguageName = "Elvish", ScriptType = "Elvish" };
				Language giant = new Language { LanguageName = "Giant", ScriptType = "Dwarvish" };
				Language gnomish = new Language { LanguageName = "Gnomish", ScriptType = "Dwarvish" };
				Language goblin = new Language { LanguageName = "Goblin", ScriptType = "Dwarvish" };
				Language halfling = new Language { LanguageName = "Halfling", ScriptType = "Halfling" };
				Language orc = new Language { LanguageName = "Orcish", ScriptType = "Dwarvish" };
				Language abyssal = new Language { LanguageName = "Abyssal", ScriptType = "Infernal" };
				Language celestial = new Language { LanguageName = "Celestial", ScriptType = "Celestial" };
				Language draconic = new Language { LanguageName = "Draconic", ScriptType = "Draconic" };
				Language deepSpeech = new Language { LanguageName = "Deep Speech", ScriptType = "" };
				Language infernal = new Language { LanguageName = "Infernal", ScriptType = "Infernal" };
				Language primordial = new Language { LanguageName = "Primordial", ScriptType = "Dwarvish" };
				Language auran = new Language { LanguageName = "Auran", ScriptType = "Dwarvish" };
				Language aquan = new Language { LanguageName = "Aquan", ScriptType = "Dwarvish" };
				Language ignan = new Language { LanguageName = "Ignan", ScriptType = "Dwarvish" };
				Language terran = new Language { LanguageName = "Terran", ScriptType = "Dwarvish" };
				Language sylvan = new Language { LanguageName = "Sylvan", ScriptType = "Elvish" };
				Language undercommon = new Language { LanguageName = "Undercommon", ScriptType = "Elvish" };
				Language druidic = new Language { LanguageName = "Druidic", ScriptType = "Druidic" };
				Language thievesCant = new Language { LanguageName = "Thieves' Cant", ScriptType = "Thieves' Cant" };
				Language aarakocraLanguage = new Language { LanguageName = "Aarakocra", ScriptType = "Aarakocra" };
				db.Languages.Add(common);
				db.Languages.Add(dwarvish);
				db.Languages.Add(elvish);
				db.Languages.Add(giant);
				db.Languages.Add(gnomish);
				db.Languages.Add(goblin);
				db.Languages.Add(halfling);
				db.Languages.Add(orc);
				db.Languages.Add(abyssal);
				db.Languages.Add(celestial);
				db.Languages.Add(draconic);
				db.Languages.Add(deepSpeech);
				db.Languages.Add(infernal);
				db.Languages.Add(primordial);
				db.Languages.Add(auran);
				db.Languages.Add(aquan);
				db.Languages.Add(ignan);
				db.Languages.Add(terran);
				db.Languages.Add(sylvan);
				db.Languages.Add(undercommon);
				db.Languages.Add(druidic);
				db.Languages.Add(thievesCant);
				db.SaveChanges();
                Console.WriteLine("Successfully added Languages!");
                #endregion Languages
                #region MovementTypes
                Console.WriteLine("Adding MovementTypes...");
				MovementType walking = new MovementType { MovementName = "Walking speed" };
				MovementType climbing = new MovementType { MovementName = "Climbing speed" };
				MovementType swimming = new MovementType { MovementName = "Swimming speed" };
				MovementType flying = new MovementType { MovementName = "Flying speed" };
				MovementType burrowing = new MovementType { MovementName = "Burrowing speed" };
                db.MovementTypes.Add(walking);
                db.MovementTypes.Add(climbing);
                db.MovementTypes.Add(swimming);
                db.MovementTypes.Add(flying);
                db.MovementTypes.Add(burrowing);
                db.SaveChanges();
                Console.WriteLine("Successfully added MovementTypes!");
                #endregion MovementTypes
                #region Racial Features
                Console.WriteLine("Adding Racial Features...");
				#region GenericFeatures
				Feature darkvision = new Feature
				{
					FeatureName = "Darkvision",
					FeatureDescription = "You can see in dim light within 60 feet of you as if it were bright light, and in darkness as if it were dim light. You can't discern color in darkness, only shades of gray.",
				};
                Feature superiorDarkvision = new Feature
                {
					FeatureName = "Superior Darkvision",
					FeatureDescription = "You can see in dim light within 120 feet of you as if it were bright light, and in darkness as if it were dim light. You can't discern color in darkness, only shades of gray.",
				};
                Feature poisonResilience = new Feature
                {
					FeatureName = "Poison Resilience",
					FeatureDescription = "You have advantage on saving throws against poison, and you have resistance against poison damage.",
				};
                Feature fireResistance = new Feature
                {
					FeatureName = "Fire Resistance",
					FeatureDescription = "You have resistance to fire damage."
				};
                #endregion GenericFeatures
                #region Dwarf Features
                Feature stonecunning = new Feature
                {
					FeatureName = "Stonecunning",
					FeatureDescription = "Whenever you make an Intelligence (History) check related to the origin of stonework, you are considered proficient in the History skill and add double your proficiency bonus to the check, instead of your proficiency bonus.",
				};
                Feature dwarvenToughness = new Feature
                {
					FeatureName = "Dwarven Toughness",
					FeatureDescription = "Your hit point maximum increases by 1, and it increases by 1 every time you gain a level.",
				};
                #endregion Dwarf Features
                #region Elf Features
                Feature feyAncestry = new Feature
                {
					FeatureName = "Fey Ancestry",
					FeatureDescription = "You have advantage on saving against being charmed, and magic can’t put you to sleep.",
				};
                Feature trance = new Feature
                {
					FeatureName = "Trance",
					FeatureDescription = "Elves don’t need to sleep. Instead, they meditate deeply, remaining semiconscious, for 4 hours a day. (The Common word for such meditation is “trance.”) While meditating, you can dream after a fashion; such dreams are actually mental exercises that have become reflexive through years of practice. After resting in this way, you gain the same benefit that a human does from 8 hours of sleep.",
				};
                Feature highElfCantrip = new Feature
                {
					FeatureName = "High Elf Cantrip",
					FeatureDescription = "You know one cantrip of your choice from the wizard spell list. Your spellcasting ability for this feature is Intelligence.",
				};
                Feature maskOfTheWild = new Feature
                {
					FeatureName = "Mask of the Wild",
					FeatureDescription = "You can attempt to hide even when you are only lightly obscured by foliage, heavy rain, falling snow, mist, and other natural phenomena.",
				};
                Feature sunlightSensitivity = new Feature
                {
					FeatureName = "Sunlight Sensitivity",
					FeatureDescription = "You have disadvantage on attack rolls and on Wisdom (Perception) checks that rely on sight when you, the target of your attack, or whatever you are trying to perceive is in direct sunlight.",
				};
                Feature drowMagic = new Feature
                {
					FeatureName = "Drow Magic",
					FeatureDescription = "You know the dancing lights cantrip. When you reach 3rd level, you can cast the faerie fire spell once per day. When you reach 5th level,	you can also cast the darkness spell once per day. Charisma is your spellcasting ability for these spells.",
				};
				#endregion Elf Features
				#region Halfling Features
				Feature lucky = new Feature
				{
					FeatureName = "Lucky",
					FeatureDescription = "When you roll a 1 on an attack roll, ability check, or saving throw, you can reroll the die and must use the new roll.",
				};
				Feature brave = new Feature
				{
					FeatureName = "Brave",
					FeatureDescription = "You have advantage on saving throws against being frightened.",
				};
				Feature halflingNimbleness = new Feature
				{
					FeatureName = "Halfling Nimbleness",
					FeatureDescription = "You can move through the space of any creature that is of a size larger than yours.",
				};
				Feature naturallyStealthy = new Feature
				{
					FeatureName = "Naturally Stealthy",
					FeatureDescription = "You can attempt to hide even when you are obscured only by a creature that is at least one size larger than you.",
				};
				#endregion Halfling Features
				#region Dragonborn Features
				Feature draconicAncestry = new Feature
				{
					FeatureName = "Draconic Ancestry",
					FeatureDescription = "You have draconic ancestry. Choose one type of dragon from the Draconic Ancestry table. Your breath weapon and damage resistance are determined by the dragon type, as shown in the table."
				};
				Feature breathWeapon = new Feature
				{
					FeatureName = "Breath Weapon",
					FeatureDescription = "You can use your action to exhale destructive energy. Your draconic ancestry determines the size, shape, and damage type of the exhalation. When you use your breath weapon, each creature in the area o f the exhalation must make a saving throw, the type of which is determined by your draconic ancestry.The DC for this saving throw equals 8 + your Constitution modifier + your proficiency bonus. A creature takes 2d6 damage on a failed save, and half as much damage on a successful one.The damage increases to 3d6 at 6th level, 4d6 at 11th level, and 5d6 at 16th level. After you use your breath weapon, you can’t use it again until you complete a short or long rest."
				};
				Feature damageResistance = new Feature
				{
					FeatureName = "Damage Resistance",
					FeatureDescription = "You have resistance to the damage type associated with your draconic ancestry."
				};
				#endregion Dragonborn Features
				#region Gnome Features
				Feature gnomeCunning = new Feature
				{
					FeatureName = "Gnome Cunning",
					FeatureDescription = "You have advantage on all Intelligence, Wisdom and Charisma saving throws against magic."
				};
				Feature naturalIllusionist = new Feature
				{
					FeatureName = "Natural Illusionist",
					FeatureDescription = "You know the minor illusion cantrip. Intelligence is your spellcasting ability for it."
				};
				Feature speakWithSmallBeasts = new Feature
				{
					FeatureName = "Speak with Small Beasts",
					FeatureDescription = "Through sounds and gestures, you can communicate simple ideas with Small or smaller beasts.Forest gnomes love animals and often keep squirrels, badgers, rabbits, moles, woodpeckers, and other creatures as beloved pets."
				};
				Feature artificersLore = new Feature
				{
					FeatureName = "Artificer's Lore",
					FeatureDescription = "Whenever you make an Intelligence (History) check related to magic items,	alchemical objects, or technological devices, you can add twice your proficiency bonus, instead o f any proficiency bonus you normally apply.",
				};
				Feature tinker = new Feature
				{
					FeatureName = "Tinker",
					FeatureDescription = "You have proficiency with artisan’s tools (tinker’s tools).Using those tools, you can spend 1 hour and 10 gp worth o f materials to construct a Tiny clockwork device(AC 5, 1 hp).The device ceases to function after 24 hours(unless you spend 1 hour repairing it to keep the device functioning), or when you use your action to dismantle it; at that time, you can reclaim the materials used to create it.You can have up to three such devices active at a time. When you create a device, choose one of the following options: Clockwork Toy. This toy is a clockwork animal, monster, or person, such as a frog, mouse, bird, dragon, or soldier.When placed on the ground, the toy moves 5 feet across the ground on each of your turns in a random direction. It makes noises as appropriate to the creature it represents. Fire Starter. The device produces a miniature flame, which you can use to light a candle, torch, or campfire. Using the device requires your action. Music Box. When opened, this music box plays a single song at a moderate volume. The box stops playing when it reaches the song’s end or when it is closed."
				};
				Feature stoneCamouflage = new Feature
				{
					FeatureName = "Stone Camouflage",
					FeatureDescription = "You have advantage on Dexterity(stealth) checks to hide in rocky terrain."
				};
				#endregion Gnome Features
				#region Half-Orc Features
				Feature relentlessEndurance = new Feature
				{
					FeatureName = "Relentless Endurance",
					FeatureDescription = "When you are reduced to 0 hit points but not killed outright,	you can drop to 1 hitpoint instead. You can’t use this ClassFeature again until you finish a long rest."
				};
				Feature savageAttacks = new Feature
				{
					FeatureName = "Savage Attacks",
					FeatureDescription = "When you score a critical hit with a melee weapon attack, you can roll one of the weapon’s damage dice one additional time and add it to the extra damage of the critical hit."
				};
				#endregion Half-Orc Features
				#region Tiefling Features
				Feature infernalLegacy = new Feature
				{
					FeatureName = "Infernal Legacy",
					FeatureDescription = "You know the thaumaturgy cantrip. Once you reach 3rd level, you can cast the hellish rebuke spell once per day as a 2nd-level spell. Once you reach 5th level, you can also cast the darkness spell once per day. Charisma is your spellcasting ability for these spells."
				};
				#endregion Tiefling Features
				#region Aarakocra Features
				Feature flight = new Feature
				{
					FeatureName = "Flight",
					FeatureDescription = "You have a flying speed of 50ft. To use this speed, you can't be wearing medium or heavy armor."
				};
				Feature talons = new Feature
				{
					FeatureName = "Talons",
					FeatureDescription = "You are proficient with your unarmed strikes, which deal 1d4 damage on a hit."
				};
				#endregion Aarakocra Features
				#region Genasi Features
				Feature unendingBreath = new Feature
				{
					FeatureName = "Unending Breath",
					FeatureDescription = "You can hold your breath indefinitely while you're not incapacitated."
				};
				Feature mingleWithTheWind = new Feature
				{
					FeatureName = "Mingle with the Wind",
					FeatureDescription = "You can cast the levitate spell once with this trait, requiring no material components, and you regain the ability to cast it this way when you finish a long rest.Constitution is your spellcasting ability for this spell."
				};
				Feature earthWalk = new Feature
				{
					FeatureName = "Earth Walk",
					FeatureDescription = "You can move across difficult terrain made of earth or stone without expending extra movement."
				};
				Feature mergeWithStone = new Feature
				{
					FeatureName = "Merge with Stone",
					FeatureDescription = "You can cast the pass without trace spell once with this trait, requiring no material components, and you regain the ability to cast it this way when you finish a long rest.Constitution is your spellcasting ability for this spell."
				};
				Feature fireGenasiDarkvision = new Feature
				{
					FeatureName = "Darkvision",
					FeatureDescription = "You can see in dim light within 60 feet of you as if it were bright light, and in darkness as if it were dim light.Your ties to the Elemental Plane of Fire make your darkvision unusual: everything you see in darkness is in a shade of red."
				};
				Feature reachToTheBlaze = new Feature
				{
					FeatureName = "Reach to the Blaze",
					FeatureDescription = "You know the produce flame cantrip. Once you reach 3rd level, you can cast the burning hands spell once with this trait as a 1st-level spell, and you regain the ability to cast it this way when you finish a long rest. Constitution is your spellcasting ability for these spells."
				};
				Feature acidResistance = new Feature
				{
					FeatureName = "Acid Resistance",
					FeatureDescription = "You have resistance to acid damage."
				};
				Feature amphibious = new Feature
				{
					FeatureName = "Amphibious",
					FeatureDescription = "You can breathe air and water."
				};
				Feature callToTheWave = new Feature
				{
					FeatureName = "Call to the Wave",
					FeatureDescription = "You know the shape water cantrip (see chapter 2).When you reach 3rd level, you can cast the create or destroy water spell as a 2nd-level spell once with this trait, and you regain the ability to cast it this way when you finish a long rest. Constitution is your spellcasting ability for these spells."
				};
				#endregion Genasi Features
				#region Goliath Features
				Feature stonesEndurance = new Feature
				{
					FeatureName = "Stone's Endurance",
					FeatureDescription = "You can focus yourself to occasionally shrug off injury.When you take damage, you can use your reaction to roll a d12.Add your Constitution modifier to the number rolled, and reduce the damage by that total. After you use this trait, you can’t use it again until you finish a short or long rest."
				};
				Feature powerfulBuild = new Feature
				{
					FeatureName = "Powerful Build",
					FeatureDescription = "You count as one size larger when determining your carrying capacity and the weight you can push, drag, or lift."
				};
				Feature mountainBorn = new Feature
				{
					FeatureName = "Mountain Born",
					FeatureDescription = "You’re acclimated to high altitude, including elevations above 20,000 feet. You’re also naturally adapted to cold climates, as described in chapter 5 of the Dungeon Master’s Guide."
				};
				#endregion Goliath Features
				db.Features.Add(darkvision);
				db.Features.Add(superiorDarkvision);
				db.Features.Add(poisonResilience);
				db.Features.Add(fireResistance);
				db.Features.Add(stonecunning);
				db.Features.Add(dwarvenToughness);
				db.Features.Add(feyAncestry);
				db.Features.Add(trance);
				db.Features.Add(highElfCantrip);
				db.Features.Add(maskOfTheWild);
				db.Features.Add(sunlightSensitivity);
				db.Features.Add(drowMagic);
				db.Features.Add(lucky);
				db.Features.Add(brave);
				db.Features.Add(halflingNimbleness);
				db.Features.Add(naturallyStealthy);
				db.Features.Add(draconicAncestry);
				db.Features.Add(breathWeapon);
				db.Features.Add(damageResistance);
				db.Features.Add(gnomeCunning);
				db.Features.Add(naturalIllusionist);
				db.Features.Add(speakWithSmallBeasts);
				db.Features.Add(artificersLore);
				db.Features.Add(tinker);
				db.Features.Add(stoneCamouflage);
				db.Features.Add(relentlessEndurance);
				db.Features.Add(savageAttacks);
				db.Features.Add(infernalLegacy);
				db.Features.Add(flight);
				db.Features.Add(talons);
				db.Features.Add(unendingBreath);
				db.Features.Add(mingleWithTheWind);
				db.Features.Add(earthWalk);
				db.Features.Add(mergeWithStone);
				db.Features.Add(fireGenasiDarkvision);
				db.Features.Add(reachToTheBlaze);
				db.Features.Add(acidResistance);
				db.Features.Add(amphibious);
				db.Features.Add(callToTheWave);
				db.Features.Add(stonesEndurance);
				db.Features.Add(powerfulBuild);
				db.Features.Add(mountainBorn);
				db.SaveChanges();
                Console.WriteLine("Successfully added Racial Features!");
                #endregion Racial Features
                #region Races
                Console.WriteLine("Adding Races...");
				#region Dwarves
				Race hillDwarf = new Race
				{
					RaceName = "Hill Dwarf",
					Size = "Medium",
					RaceWeaponProficiencies = new List<Weapon>
					{
						battleaxe,
						handaxe,
						lightHammer,
						warhammer
					},
					RaceToolProficiencies = new List<Tool>
					{
						smithTools,
						brewersSupplies,
						masonTools
					},
					RacialFeatures = new List<Feature>
					{
						darkvision,
						poisonResilience,
						stonecunning,
					},
					RacialLanguages = new List<Language>
					{
						common,
						dwarvish
					},
					AbilityBonusses = new List<AbilityBonus>
					{
						new AbilityBonus { Ability = constitution, Value = 2 },
						new AbilityBonus { Ability = wisdom, Value = 1 }
					}
				};
				Race mountainDwarf = new Race
				{
					RaceName = "Mountain Dwarf",
					MovementSpeeds = new List<MovementSpeed> { new MovementSpeed { MovementType = walking, Speed = 25 } },
					Size = "Medium",
					RaceWeaponProficiencies = new List<Weapon>
					{
						battleaxe,
						handaxe,
						lightHammer,
						warhammer
					},
					RaceToolProficiencies = new List<Tool>
					{
						smithTools,
						brewersSupplies,
						masonTools
					},
					RaceArmorProficiencies = db.Armors.Where(a => !a.ArmorType.Equals("Heavy")).ToList(),
					RacialFeatures = new List<Feature>
					{
						darkvision,
						poisonResilience,
						stonecunning
					},
					RacialLanguages = new List<Language>
					{
						common,
						dwarvish
					},
					AbilityBonusses = new List<AbilityBonus>
					{
						new AbilityBonus { Ability = constitution, Value = 2 },
						new AbilityBonus { Ability = wisdom, Value = 1 }
					}
				};
				#endregion Dwarves
				#region Elves
				Race highElf = new Race
				{
					RaceName = "High Elf",
					MovementSpeeds = new List<MovementSpeed> { new MovementSpeed { MovementType = walking, Speed = 30 } },
					Size = "Medium",
					RaceWeaponProficiencies = new List<Weapon>
					{
						longsword,
						shortsword,
						shortbow,
						longbow
					},
					RaceSkillProficiencies = new List<Skill> { perception },
					RacialFeatures = new List<Feature>
					{
						darkvision,
						feyAncestry,
						trance,
						highElfCantrip
					},
					RacialLanguages = new List<Language>
					{
						common,
						elvish
					},
					AbilityBonusses = new List<AbilityBonus>
					{
						new AbilityBonus { Ability = dexterity, Value = 2 },
						new AbilityBonus { Ability = intelligence, Value = 1 }
					}
				};
				Race woodElf = new Race
				{
					RaceName = "Wood Elf",
					MovementSpeeds = new List<MovementSpeed> { new MovementSpeed { MovementType = walking, Speed = 35 } },
					Size = "Medium",
					RaceSkillProficiencies = new List<Skill> { perception },
					RacialLanguages = new List<Language>
					{
						common,
						elvish
					},
					RacialFeatures = new List<Feature>
					{
						darkvision,
						feyAncestry,
						trance,
						maskOfTheWild
					},
					RaceWeaponProficiencies = new List<Weapon>
					{
						longsword,
						shortsword,
						shortbow,
						longbow
					},
					AbilityBonusses = new List<AbilityBonus>
					{
						new AbilityBonus { Ability = dexterity, Value = 2 },
						new AbilityBonus { Ability = wisdom, Value = 1 }
					}
				};
				Race darkElf = new Race
				{
					RaceName = "Dark Elf",
					MovementSpeeds = new List<MovementSpeed> { new MovementSpeed { MovementType = walking, Speed = 30 } },
					Size = "Medium",
					RaceSkillProficiencies = new List<Skill> { perception },
					RacialFeatures = new List<Feature>
					{
						superiorDarkvision,
						feyAncestry,
						trance,
						drowMagic
					},
					RacialLanguages = new List<Language>
					{
						common,
						elvish
					},
					RaceWeaponProficiencies = new List<Weapon>
					{
						rapier,
						shortsword,
						handCrossbow
					},
					AbilityBonusses = new List<AbilityBonus>
					{
						new AbilityBonus { Ability = dexterity, Value = 2 },
						new AbilityBonus { Ability = charisma, Value = 1 }
					}
				};
				#endregion Elves
				#region Halflings
				Race lightfootHalfling = new Race
				{
					RaceName = "Lightfoot Halfling",
					MovementSpeeds = new List<MovementSpeed> { new MovementSpeed { MovementType = walking, Speed = 25 } },
					Size = "Small",
					RacialFeatures = new List<Feature>
					{
						lucky,
						brave,
						halflingNimbleness,
						naturallyStealthy
					},
					RacialLanguages = new List<Language>
					{
						common,
						halfling
					},
					AbilityBonusses = new List<AbilityBonus>
					{
						new AbilityBonus { Ability = dexterity, Value = 2 },
						new AbilityBonus { Ability = charisma, Value = 1 }
					}
				};
				Race stoutHalfling = new Race
				{
					RaceName = "Stout Halfling",
					MovementSpeeds = new List<MovementSpeed> { new MovementSpeed { MovementType = walking, Speed = 25 } },
					Size = "Small",
					RacialFeatures = new List<Feature>
					{
						lucky,
						brave,
						halflingNimbleness,
						poisonResilience
					},
					RacialLanguages = new List<Language>
					{
						common,
						halfling
					},
					AbilityBonusses = new List<AbilityBonus>
					{
						new AbilityBonus { Ability = dexterity, Value = 2 },
						new AbilityBonus { Ability = constitution, Value = 1 }
					}
				};
				#endregion Halflings
				#region Human
				Race human = new Race
				{
					RaceName = "Human",
					MovementSpeeds = new List<MovementSpeed> { new MovementSpeed { MovementType = walking, Speed = 30 } },
					Size = "Medium",
					RacialLanguages = new List<Language> { common },
					AbilityBonusses = new List<AbilityBonus>
					{
						new AbilityBonus { Ability = strength, Value = 1 },
						new AbilityBonus { Ability = dexterity, Value = 1 },
						new AbilityBonus { Ability = constitution, Value = 1 },
						new AbilityBonus { Ability = intelligence, Value = 1 },
						new AbilityBonus { Ability = wisdom, Value = 1 },
						new AbilityBonus { Ability = charisma, Value = 1 },
					}
				};
				#endregion Human
				#region Dragonborn
				Race dragonborn = new Race
				{
					RaceName = "Dragonborn",
					MovementSpeeds = new List<MovementSpeed> { new MovementSpeed { MovementType = walking, Speed = 30 } },
					Size = "Medium",
					RacialFeatures = new List<Feature>
					{
						draconicAncestry,
						breathWeapon,
						damageResistance
					},
					RacialLanguages = new List<Language>
					{
						common,
						draconic
					},
					AbilityBonusses = new List<AbilityBonus>
					{
						new AbilityBonus { Ability = strength, Value = 2 },
						new AbilityBonus { Ability = charisma, Value = 1 }
					}
				};
				#endregion Dragonborn
				#region Gnomes
				Race forestGnome = new Race
				{
					RaceName = "Forest Gnome",
					MovementSpeeds = new List<MovementSpeed> { new MovementSpeed { MovementType = walking, Speed = 25 } },
					Size = "Small",
					RacialFeatures = new List<Feature>
					{
						darkvision,
						gnomeCunning,
						naturalIllusionist,
						speakWithSmallBeasts
					},
					RacialLanguages = new List<Language>
					{
						common,
						gnomish
					},
					AbilityBonusses = new List<AbilityBonus>
					{
						new AbilityBonus { Ability = intelligence, Value = 2 },
						new AbilityBonus { Ability = dexterity, Value = 1 }
					}
				};
				Race rockGnome = new Race
				{
					RaceName = "Rock Gnome",
					MovementSpeeds = new List<MovementSpeed> { new MovementSpeed { MovementType = walking, Speed = 25 } },
					Size = "Small",
					RacialFeatures = new List<Feature>
					{
						darkvision,
						gnomeCunning,
						artificersLore,
						tinker
					},
					RacialLanguages = new List<Language>
					{
						common,
						gnomish
					},
					AbilityBonusses = new List<AbilityBonus>
					{
						new AbilityBonus { Ability = intelligence, Value = 2 },
						new AbilityBonus { Ability = constitution, Value = 1 }
					}
				};
				Race deepGnome = new Race
				{
					RaceName = "Deep Gnome",
					MovementSpeeds = new List<MovementSpeed> { new MovementSpeed { MovementType = walking, Speed = 25 } },
					Size = "Small",
					RacialFeatures = new List<Feature>
					{
						superiorDarkvision,
						gnomeCunning,
						stoneCamouflage
					},
					RacialLanguages = new List<Language>
					{
						common,
						gnomish,
						undercommon
					},
					AbilityBonusses = new List<AbilityBonus>
					{
						new AbilityBonus { Ability = intelligence, Value = 2 },
						new AbilityBonus { Ability = dexterity, Value = 1 }
					}
				};
				#endregion Gnomes
				#region Half-Elf
				Race halfElf = new Race
				{
					RaceName = "Half-Elf",
					MovementSpeeds = new List<MovementSpeed> { new MovementSpeed { MovementType = walking, Speed = 30 } },
					Size = "Medium",
					RacialFeatures = new List<Feature>
					{
						darkvision,
						feyAncestry
					},
					RacialLanguages = new List<Language>
					{
						common,
						elvish
					},
					AbilityBonusses = new List<AbilityBonus>
					{
						new AbilityBonus { Ability = charisma, Value = 2 }
					}
				};
				#endregion Half-Elf
				#region Half-Orc
				Race halfOrc = new Race
				{
					RaceName = "Half-Orc",
					MovementSpeeds = new List<MovementSpeed> { new MovementSpeed { MovementType = walking, Speed = 30 } },
					Size = "Medium",
					RacialFeatures = new List<Feature>
					{
						darkvision,
						relentlessEndurance,
						savageAttacks
					},
					RacialLanguages = new List<Language>
					{
						common,
						orc
					},
					RaceSkillProficiencies = new List<Skill> { intimidation },
					AbilityBonusses = new List<AbilityBonus>
					{
						new AbilityBonus { Ability = strength, Value = 2 },
						new AbilityBonus { Ability = constitution, Value = 1 }
					}
				};
				#endregion Half-Orc
				#region Tiefling
				Race tiefling = new Race
				{
					RaceName = "Tiefling",
					MovementSpeeds = new List<MovementSpeed> { new MovementSpeed { MovementType = walking, Speed = 30 } },
					Size = "Medium",
					RacialFeatures = new List<Feature>
					{
						darkvision,
						fireResistance,
						infernalLegacy
					},
					RacialLanguages = new List<Language>
					{
						common,
						infernal
					},
					AbilityBonusses = new List<AbilityBonus>
					{
						new AbilityBonus { Ability = intelligence, Value = 1 },
						new AbilityBonus { Ability = charisma, Value = 2 }
					}
				};
				#endregion Tiefling
				#region Aarakocra
				Race aarakocraRace = new Race
				{
					RaceName = "Aarakocra",
					MovementSpeeds = new List<MovementSpeed>
					{
						new MovementSpeed { MovementType = walking, Speed = 25 },
						new MovementSpeed { MovementType = flying, Speed = 50 }
					},
					Size = "Medium",
					RacialLanguages = new List<Language>
					{
						common,
						auran,
						aarakocraLanguage
					},
					AbilityBonusses = new List<AbilityBonus>
					{
						new AbilityBonus { Ability = dexterity, Value = 2 },
						new AbilityBonus { Ability = wisdom, Value = 1 }
					},
					RacialFeatures = new List<Feature>
					{
						flight,
						talons
					}
				};
				#endregion Aarakocra
				#region Genasi
				Race airGenasi = new Race
				{
					RaceName = "Air Genasi",
					MovementSpeeds = new List<MovementSpeed> { new MovementSpeed { MovementType = walking, Speed = 30 } },
					Size = "Medium",
					RacialFeatures = new List<Feature>
					{
						unendingBreath,
						mingleWithTheWind
					},
					RacialLanguages = new List<Language>
					{
						common,
						primordial
					},
					AbilityBonusses = new List<AbilityBonus>
					{
						new AbilityBonus { Ability = constitution, Value = 2 },
						new AbilityBonus { Ability = dexterity, Value = 1 }
					}
				};
				Race earthGenasi = new Race
				{
					RaceName = "Earth Genasi",
					MovementSpeeds = new List<MovementSpeed> { new MovementSpeed { MovementType = walking, Speed = 30 } },
					Size = "Medium",
					RacialFeatures = new List<Feature>
					{
						earthWalk,
						mergeWithStone
					},
					RacialLanguages = new List<Language>
					{
						common,
						primordial
					},
					AbilityBonusses = new List<AbilityBonus>
					{
						new AbilityBonus { Ability = constitution, Value = 2 },
						new AbilityBonus { Ability = strength, Value = 1 }
					}
				};
				Race fireGenasi = new Race
				{
					RaceName = "Fire Genasi",
					MovementSpeeds = new List<MovementSpeed> { new MovementSpeed { MovementType = walking, Speed = 30 } },
					Size = "Medium",
					RacialFeatures = new List<Feature>
					{
						fireGenasiDarkvision,
						fireResistance,
						reachToTheBlaze
					},
					RacialLanguages = new List<Language>
					{
						common,
						primordial
					},
					AbilityBonusses = new List<AbilityBonus>
					{
						new AbilityBonus { Ability = constitution, Value = 2 },
						new AbilityBonus { Ability = intelligence, Value = 1 }
					}
				};
				Race waterGenasi = new Race
				{
					RaceName = "Water Genasi",
					MovementSpeeds = new List<MovementSpeed>
					{
						new MovementSpeed { MovementType = walking, Speed = 30 },
						new MovementSpeed { MovementType = swimming, Speed = 30 }
					},
					Size = "Medium",
					RacialFeatures = new List<Feature>
					{
						acidResistance,
						amphibious,
						callToTheWave
					},
					RacialLanguages = new List<Language>
					{
						common,
						primordial
					},
					AbilityBonusses = new List<AbilityBonus>
					{
						new AbilityBonus { Ability = constitution, Value = 2 },
						new AbilityBonus { Ability = wisdom, Value = 1 }
					}
				};
				#endregion Genasi
				#region Goliath
				Race goliath = new Race
				{
					RaceName = "Goliath",
					MovementSpeeds = new List<MovementSpeed> { new MovementSpeed { MovementType = walking, Speed = 30 } },
					Size = "Medium",
					RacialFeatures = new List<Feature>
					{
						stonesEndurance,
						powerfulBuild,
						mountainBorn
					},
					RacialLanguages = new List<Language>
					{
						common,
						giant
					},
					RaceSkillProficiencies = new List<Skill> { athletics },
					AbilityBonusses = new List<AbilityBonus>
					{
						new AbilityBonus { Ability = strength, Value = 2 },
						new AbilityBonus { Ability = constitution, Value = 1 }
					}
				};
				#endregion Goliath
				db.Races.Add(aarakocraRace);
				db.Races.Add(dragonborn);
				db.Races.Add(hillDwarf);
				db.Races.Add(mountainDwarf);
				db.Races.Add(darkElf);
				db.Races.Add(highElf);
				db.Races.Add(woodElf);
				db.Races.Add(airGenasi);
				db.Races.Add(earthGenasi);
				db.Races.Add(fireGenasi);
				db.Races.Add(waterGenasi);
				db.Races.Add(deepGnome);
				db.Races.Add(forestGnome);
				db.Races.Add(rockGnome);
				db.Races.Add(goliath);
				db.Races.Add(halfElf);
				db.Races.Add(halfOrc);
				db.Races.Add(lightfootHalfling);
				db.Races.Add(stoutHalfling);
				db.Races.Add(human);
				db.Races.Add(tiefling);
				db.SaveChanges();
                Console.WriteLine("Successfully added Races!");
                #endregion Races
                #region Classes
                Console.WriteLine("Adding Classes...");
                Class barbarian = new Class
				{
					ClassName = "Barbarian",
					HitDie = 12,
					SkillCap = 2,
					ClassSkillProficiencies = new List<Skill>
					{
						animalHandling,
						athletics,
						intimidation,
						nature,
						perception,
						survival
					},
					ClassSavingThrowProficiencies = new List<Ability>
					{
						strength,
						constitution
					},
					ClassWeaponProficiencies = db.Weapons.Select(w => w).ToList(),
					ClassArmorProficiencies = db.Armors.Where(a => !a.ArmorType.Equals("Heavy")).ToList()
				};
				Class bard = new Class
				{
					ClassName = "Bard",
					HitDie = 8,
					SkillCap = 3,
					ClassSkillProficiencies = db.Skills.Select(s => s).ToList(),
					ClassSavingThrowProficiencies = new List<Ability>
					{
						dexterity,
						charisma
					},
					ClassArmorProficiencies = db.Armors.Where(a => a.ArmorType.Equals("Light")).ToList(),
					ClassWeaponProficiencies = (new List<Weapon>(db.Weapons.Where(w => w.WeaponType.Equals("Simple")))
					{
						handCrossbow,
						longsword,
						rapier,
						shortsword
					})
				};
				Class cleric = new Class
				{
					ClassName = "Cleric",
					HitDie = 8,
					SkillCap = 2,
					ClassSkillProficiencies = new List<Skill>
					{
						history,
						insight,
						medicine,
						persuasion,
						religion
					},
					ClassSavingThrowProficiencies = new List<Ability>
					{
						wisdom,
						charisma
					},
					ClassWeaponProficiencies = db.Weapons.Where(w => w.WeaponType.Equals("Simple")).ToList(),
					ClassArmorProficiencies = db.Armors.Where(a => !a.ArmorType.Equals("Heavy")).ToList()
				};
				Class druid = new Class
				{
					ClassName = "Druid",
					HitDie = 8,
					SkillCap = 2,
					ClassSkillProficiencies = new List<Skill>
					{
						arcana,
						animalHandling,
						insight,
						medicine,
						nature,
						perception,
						religion,
						survival
					},
					ClassSavingThrowProficiencies = new List<Ability>
					{
						intelligence,
						wisdom
					},
					ClassWeaponProficiencies = new List<Weapon>
					{
						club,
						dagger,
						dart,
						javelin,
						mace,
						quarterstaff,
						scimitar,
						sickle,
						sling,
						spear
					},
					ClassArmorProficiencies = db.Armors.Where(a => !a.ArmorType.Equals("Heavy")).ToList(),
					ClassLanguages = new List<Language> { druidic }
				};
				Class fighter = new Class
				{
					ClassName = "Fighter",
					HitDie = 10,
					SkillCap = 2,
					ClassSkillProficiencies = new List<Skill>
					{
						acrobatics,
						animalHandling,
						athletics,
						history,
						insight,
						intimidation,
						perception,
						survival
					},
					ClassSavingThrowProficiencies = new List<Ability>
					{
						strength,
						constitution
					},
					ClassWeaponProficiencies = db.Weapons.Select(w => w).ToList(),
					ClassArmorProficiencies = db.Armors.Select(a => a).ToList()
				};
				Class monk = new Class
				{
					ClassName = "Monk",
					HitDie = 8,
					SkillCap = 2,
					ClassSkillProficiencies = new List<Skill>
					{
						acrobatics,
						athletics,
						history,
						insight,
						religion,
						stealth
					},
					ClassSavingThrowProficiencies = new List<Ability>
					{
						strength,
						dexterity
					},
					ClassWeaponProficiencies = new List<Weapon>(db.Weapons.Where(w => w.WeaponType.Equals("Simple")))
					{
						shortsword
					},
					ClassArmorProficiencies = new List<Armor>()
				};
				Class paladin = new Class
				{
					ClassName = "Paladin",
					HitDie = 10,
					SkillCap = 2,
					ClassSkillProficiencies = new List<Skill>
					{
						athletics,
						insight,
						intimidation,
						medicine,
						persuasion,
						religion
					},
					ClassSavingThrowProficiencies = new List<Ability>
					{
						wisdom,
						charisma
					},
					ClassWeaponProficiencies = db.Weapons.Select(w => w).ToList(),
					ClassArmorProficiencies = db.Armors.Select(w => w).ToList()
				};
				Class ranger = new Class
				{
					ClassName = "Ranger",
					HitDie = 10,
					SkillCap = 3,
					ClassSkillProficiencies = new List<Skill>
					{
						animalHandling,
						athletics,
						insight,
						investigation,
						nature,
						perception,
						stealth,
						survival
					},
					ClassSavingThrowProficiencies = new List<Ability>
					{
						strength,
						dexterity
					},
					ClassWeaponProficiencies = db.Weapons.Select(w => w).ToList(),
					ClassArmorProficiencies = db.Armors.Where(a => !a.ArmorType.Equals("Heavy")).ToList()
				};
				Class rogue = new Class
				{
					ClassName = "Rogue",
					HitDie = 8,
					SkillCap = 4,
					ClassSkillProficiencies = new List<Skill>
					{
						acrobatics,
						athletics,
						deception,
						insight,
						intimidation,
						investigation,
						perception,
						performance,
						persuasion,
						sleightOfHand,
						stealth
					},
					ClassSavingThrowProficiencies = new List<Ability>
					{
						dexterity,
						intelligence
					},
					ClassWeaponProficiencies = new List<Weapon>(db.Weapons.Where(w => w.WeaponType.Equals("Simple")))
					{
						handCrossbow,
						longsword,
						rapier,
						shortsword
					},
					ClassArmorProficiencies = new List<Armor>(db.Armors.Where(a => a.ArmorType.Equals("Light"))),
					ClassLanguages = new List<Language> { thievesCant }
				};
				Class sorcerer = new Class
				{
					ClassName = "Sorcerer",
					HitDie = 6,
					SkillCap = 2,
					ClassSkillProficiencies = new List<Skill>
					{
						arcana,
						deception,
						insight,
						intimidation,
						persuasion,
						religion
					},
					ClassSavingThrowProficiencies = new List<Ability>
					{
						constitution,
						charisma
					},
					ClassWeaponProficiencies = new List<Weapon>
					{
						dagger,
						dart,
						sling,
						quarterstaff,
						lightCrossbow
					},
					ClassArmorProficiencies = new List<Armor>()
				};
				Class warlock = new Class
				{
					ClassName = "Warlock",
					HitDie = 8,
					SkillCap = 2,
					ClassSkillProficiencies = new List<Skill>
					{
						arcana,
						deception,
						history,
						intimidation,
						investigation,
						nature,
						religion
					},
					ClassSavingThrowProficiencies = new List<Ability>
					{
						wisdom,
						charisma
					},
					ClassWeaponProficiencies = db.Weapons.Where(w => w.WeaponType.Equals("Simple")).ToList(),
					ClassArmorProficiencies = db.Armors.Where(a => a.ArmorType.Equals("Light")).ToList()
				};
				Class wizard = new Class
				{
					ClassName = "Wizard",
					HitDie = 6,
					SkillCap = 2,
					ClassSkillProficiencies = new List<Skill>
					{
						arcana,
						history,
						insight,
						investigation,
						medicine,
						religion
					},
					ClassSavingThrowProficiencies = new List<Ability>
					{
						intelligence,
						wisdom
					},
					ClassWeaponProficiencies = new List<Weapon>
					{
						dagger,
						dart,
						sling,
						quarterstaff,
						lightCrossbow
					},
					ClassArmorProficiencies = new List<Armor>()
				};
				db.Classes.Add(barbarian);
				db.Classes.Add(bard);
				db.Classes.Add(cleric);
				db.Classes.Add(druid);
				db.Classes.Add(fighter);
				db.Classes.Add(monk);
				db.Classes.Add(paladin);
				db.Classes.Add(ranger);
				db.Classes.Add(rogue);
				db.Classes.Add(sorcerer);
				db.Classes.Add(warlock);
				db.Classes.Add(wizard);
				db.SaveChanges();
                Console.WriteLine("Successfully added Classes!");
                #endregion Classes
                #region ClassArchetypes
                Console.WriteLine("Adding ClassArchetypes...");
				ClassArchetype berserker = new ClassArchetype
				{
					ClassArchetypeName = "Path of the Berserker",
					Class = barbarian
				};
				ClassArchetype totemWarrior = new ClassArchetype
				{
					ClassArchetypeName = "Path of the Totem Warrior",
					Class = barbarian
				};
				ClassArchetype collegeOfLore = new ClassArchetype
				{
					ClassArchetypeName = "College of Lore",
					Class = bard
				};
				ClassArchetype collegeOfValor = new ClassArchetype
				{
					ClassArchetypeName = "College of Valor",
					Class = bard
				};
				ClassArchetype knowledgeDomain = new ClassArchetype
				{
					ClassArchetypeName = "Domain of Knowledge",
					Class = cleric
				};
				ClassArchetype lifeDomain = new ClassArchetype
				{
					ClassArchetypeName = "Domain of Life",
					Class = cleric
				};
				ClassArchetype lightDomain = new ClassArchetype
				{
					ClassArchetypeName = "Domain of Light",
					Class = cleric
				};
				ClassArchetype natureDomain = new ClassArchetype
				{
					ClassArchetypeName = "Domain of Nature",
					Class = cleric
				};
				ClassArchetype tempestDomain = new ClassArchetype
				{
					ClassArchetypeName = "Domain of Tempest",
					Class = cleric
				};
				ClassArchetype trickeryDomain = new ClassArchetype
				{
					ClassArchetypeName = "Domain of Trickery",
					Class = cleric
				};
				ClassArchetype warDomain = new ClassArchetype
				{
					ClassArchetypeName = "Domain of Knowledge",
					Class = cleric
				};
				ClassArchetype circleOfTheMoon = new ClassArchetype
				{
					ClassArchetypeName = "Cirle of the Moon",
					Class = druid
				};
				ClassArchetype circleOfTheLand = new ClassArchetype
				{
					ClassArchetypeName = "Cirle of the Moon",
					Class = druid
				};
				ClassArchetype champion = new ClassArchetype
				{
					ClassArchetypeName = "Champion",
					Class = fighter
				};
				ClassArchetype battleMaster = new ClassArchetype
				{
					ClassArchetypeName = "battleMaster",
					Class = fighter
				};
				ClassArchetype eldritchKnight = new ClassArchetype
				{
					ClassArchetypeName = "Eldritch Knight",
					Class = fighter
				};
				ClassArchetype wayOpenHand = new ClassArchetype
				{
					ClassArchetypeName = "Way of the Open Hand",
					Class = monk
				};
				ClassArchetype wayShadow = new ClassArchetype
				{
					ClassArchetypeName = "Way of Shadow",
					Class = monk
				};
				ClassArchetype wayElements = new ClassArchetype
				{
					ClassArchetypeName = "Way of the Four Elements",
					Class = monk
				};
				ClassArchetype oathDevotion = new ClassArchetype
				{
					ClassArchetypeName = "Oath of Devotion",
					Class = paladin
				};
				ClassArchetype oathAncients = new ClassArchetype
				{
					ClassArchetypeName = "Oath of the Ancients",
					Class = paladin
				};
				ClassArchetype oathVengeance= new ClassArchetype
				{
					ClassArchetypeName = "Oath of Vengeance",
					Class = paladin
				};
				ClassArchetype hunter = new ClassArchetype
				{
					ClassArchetypeName = "Hunter",
					Class = ranger
				};
				ClassArchetype beastMaster = new ClassArchetype
				{
					ClassArchetypeName = "Beast Master",
					Class = ranger
				};
				ClassArchetype assassin = new ClassArchetype
				{
					ClassArchetypeName = "Assassin",
					Class = rogue
				};
				ClassArchetype thief = new ClassArchetype
				{
					ClassArchetypeName = "Thief",
					Class = rogue
				};
				ClassArchetype arcaneTrickster = new ClassArchetype
				{
					ClassArchetypeName = "Arcane Trickster",
					Class = rogue
				};
				ClassArchetype wildMagic = new ClassArchetype
				{
					ClassArchetypeName = "Wild Magic",
					Class = sorcerer
				};
				ClassArchetype draconicBloodline = new ClassArchetype
				{
					ClassArchetypeName = "Draconic Bloodline",
					Class = sorcerer
				};
				ClassArchetype archfey = new ClassArchetype
				{
					ClassArchetypeName = "Archfey",
					Class = warlock
				};
				ClassArchetype fiend = new ClassArchetype
				{
					ClassArchetypeName = "Fiend",
					Class = warlock
				};
				ClassArchetype greatOldOne = new ClassArchetype
				{
					ClassArchetypeName = "Great Old One",
					Class = warlock
				};
				ClassArchetype abjuration = new ClassArchetype
				{
					ClassArchetypeName = "School of Abjuration",
					Class = wizard
				};
				ClassArchetype conjuration = new ClassArchetype
				{
					ClassArchetypeName = "School of Conjuration",
					Class = wizard
				};
				ClassArchetype divination = new ClassArchetype
				{
					ClassArchetypeName = "School of Divination",
					Class = wizard
				};
				ClassArchetype enchantment = new ClassArchetype
				{
					ClassArchetypeName = "School of Enchantment",
					Class = wizard
				};
				ClassArchetype evocation = new ClassArchetype
				{
					ClassArchetypeName = "School of Evocation",
					Class = wizard
				};
				ClassArchetype illusion = new ClassArchetype
				{
					ClassArchetypeName = "School of Illusion",
					Class = wizard
				};
				ClassArchetype necromancy = new ClassArchetype
				{
					ClassArchetypeName = "School of Necromancy",
					Class = wizard
				};
				ClassArchetype transmutation = new ClassArchetype
				{
					ClassArchetypeName = "School of Trasmutation",
					Class = wizard
				};
				db.ClassArchetypes.Add(berserker);
				db.ClassArchetypes.Add(totemWarrior);
				db.ClassArchetypes.Add(collegeOfLore);
				db.ClassArchetypes.Add(collegeOfValor);
				db.ClassArchetypes.Add(knowledgeDomain);
				db.ClassArchetypes.Add(lifeDomain);
				db.ClassArchetypes.Add(lightDomain);
				db.ClassArchetypes.Add(natureDomain);
				db.ClassArchetypes.Add(tempestDomain);
				db.ClassArchetypes.Add(trickeryDomain);
				db.ClassArchetypes.Add(warDomain);
				db.ClassArchetypes.Add(circleOfTheLand);
				db.ClassArchetypes.Add(circleOfTheMoon);
				db.ClassArchetypes.Add(champion);
				db.ClassArchetypes.Add(battleMaster);
				db.ClassArchetypes.Add(eldritchKnight);
				db.ClassArchetypes.Add(wayElements);
				db.ClassArchetypes.Add(wayOpenHand);
				db.ClassArchetypes.Add(wayShadow);
				db.ClassArchetypes.Add(oathAncients);
				db.ClassArchetypes.Add(oathDevotion);
				db.ClassArchetypes.Add(oathVengeance);
				db.ClassArchetypes.Add(hunter);
				db.ClassArchetypes.Add(beastMaster);
				db.ClassArchetypes.Add(thief);
				db.ClassArchetypes.Add(assassin);
				db.ClassArchetypes.Add(arcaneTrickster);
				db.ClassArchetypes.Add(wildMagic);
				db.ClassArchetypes.Add(draconicBloodline);
				db.ClassArchetypes.Add(archfey);
				db.ClassArchetypes.Add(fiend);
				db.ClassArchetypes.Add(greatOldOne);
				db.ClassArchetypes.Add(abjuration);
				db.ClassArchetypes.Add(conjuration);
				db.ClassArchetypes.Add(divination);
				db.ClassArchetypes.Add(enchantment);
				db.ClassArchetypes.Add(evocation);
				db.ClassArchetypes.Add(illusion);
				db.ClassArchetypes.Add(necromancy);
				db.ClassArchetypes.Add(transmutation);
                Console.WriteLine("Successfully added ClassArchetypes!");
                #endregion ClassArchetypes
                #region ClassFeatures
                Console.WriteLine("Adding Class Features...");
				//TODO: Insert improving features multiple times, then in code select the highest level one only.
				#region Barbarian Features
				Feature rage = new Feature
				{
					FeatureName = "Rage",
					FeatureDescription = "In battle, you fight with primal ferocity. On your turn, you can enter a rage as a bonus action. While raging, you gain the following benefits if you aren’t wearing heavy armor: \r\n\t You have advantage on Strength checks and Strength saving throws. \r\n\t When you make a melee weapon attack using Strength, you gain a bonus to the damage roll that increases as you gain levels as a barbarian, as shown in the Rage Damage column of the Barbarian table. \r\n\t You have resistance to bludgeoning, piercing, and slashing damage. If you are able to cast spells, you can’t cast them or concentrate on them while raging. Your rage lasts for 1 minute.It ends early if you are knocked unconscious or if your turn ends and you haven’t attacked a hostile creature since your last turn or taken damage since then. You can also end your rage on your turn as a bonus action. Once you have raged the number of times shown for your barbarian level in the Rages column of the Barbarian table, you must finish a long rest before you can rage again.",
					RequiredLevel = 1,
					FeatureForClass = barbarian
				};
				Feature unarmoredDefense = new Feature
				{
					FeatureName = "Unarmored Defense",
					FeatureDescription = "While you are not wearing any armor, your Armor Class equals 10 + your Dexterity modifier + your Constitution modifier.You can use a shield and still gain this benefit.",
					RequiredLevel = 1,
					FeatureForClass = barbarian
				};
				Feature recklessAttack = new Feature
				{
					FeatureName = "Reckless Attack",
					FeatureDescription = "You can throw aside all concern for defense to attack with fierce desperation.When you make your first attack on your turn, you can decide to attack recklessly.Doing so gives you advantage on melee weapon attack rolls using Strength during this turn, but attack rolls against you have advantage until your next turn.",
					RequiredLevel = 2,
					FeatureForClass = barbarian
				};
				Feature dangerSense = new Feature
				{
					FeatureName = "Danger Sense",
					FeatureDescription = "You gain an uncanny sense of when things nearby aren’t as they should be, giving you an edge when you dodge away from danger. You have advantage on Dexterity saving throws against effects that you can see, such as traps and spells. To gain this benefit, you can’t be blinded, deafened, or incapacitated.",
					RequiredLevel = 2,
					FeatureForClass = barbarian
				};
				Feature extraAttack = new Feature
				{
					FeatureName = "Extra Attack",
					FeatureDescription = "Beginning at 5th level, you can attack twice, instead of once, whenever you take the Attack action on your turn.",
					RequiredLevel = 5,
					FeatureForClass = barbarian
				};
				Feature fastMovement = new Feature
				{
					FeatureName = "Fast Movement",
					FeatureDescription = "Your speed increases by 10 feet while you aren’t wearing heavy armor.",
					RequiredLevel = 5,
					FeatureForClass = barbarian
				};
				Feature feralInstinct = new Feature
				{
					FeatureName = "Feral Instinct",
					FeatureDescription = "By 7th level, your instincts are so honed that you have advantage on initiative rolls. Additionally, if you are surprised at the beginning of combat and aren’t incapacitated, you can act normally on your first turn, but only if you enter your rage before doing anything else on that turn.",
					RequiredLevel = 7,
					FeatureForClass = barbarian
				};
				Feature brutalCritical = new Feature
				{
					FeatureName = "Brutal Critical",
					FeatureDescription = "You can roll 1 additional weapon damage die when determining the extra damage for a critical hit with a melee attack. This increases to 2 additional dice at 13th level and three additional dice at 17th level",
					RequiredLevel = 7,
					FeatureForClass = barbarian
				};
				Feature relentlessRage = new Feature
				{
					FeatureName = "Relentless Rage",
					FeatureDescription = "Your rage can keep you fighting despite grievous wounds.If you drop to 0 hit points while you’re raging and don’t die outright, you can make a DC 10 Constitution saving throw. If you succeed, you drop to 1 hit point instead. Each time you use this feature after the first, the DC increases by 5. When you finish a short or long rest, the DC resets to 10.",
					RequiredLevel = 11,
					FeatureForClass = barbarian
				};
				Feature persistantRage = new Feature
				{
					FeatureName = "Persistant Rage",
					FeatureDescription = "Your rage is so fierce that it ends early only if you fall unconscious or if you choose to end it.",
					RequiredLevel = 15,
					FeatureForClass = barbarian
				};
				Feature indomitableMight = new Feature
				{
					FeatureName = "Indomitable Might",
					FeatureDescription = "If your total for a Strength check is less than your Strength score, you can use that score in place of the total.",
					RequiredLevel = 18,
					FeatureForClass = barbarian
				};
				Feature primalChampion = new Feature
				{
					FeatureName = "Primal Champion",
					FeatureDescription = "At 20th level, you embody the power of the wilds. Your Strength and Constitution scores increase by 4.Your maximum for those scores is now 24.",
					RequiredLevel = 20,
					FeatureForClass = barbarian
				};
				Feature frenzy = new Feature
				{
					FeatureName = "Frenzy",
					FeatureDescription = "You can go into a frenzy when you rage. If you do so, for the duration of your rage you can make a single melee weapon attack as a bonus action on each of your turns after this one. When your rage ends, you suffer one level of exhaustion(as described in appendix A).",
					RequiredLevel = 3,
					FeatureForClass = barbarian,
					FeatureForArchetype = berserker
				};
				Feature mindlessRage = new Feature
				{
					FeatureName = "Mindless Rage",
					FeatureDescription = "You can’t be charmed or frightened while raging.If you are charmed or frightened when you enter your rage, the effect is suspended for the duration of the rage.",
					RequiredLevel = 6,
					FeatureForClass = barbarian,
					FeatureForArchetype = berserker
				};
				Feature intimidatingPresence = new Feature
				{
					FeatureName = "Intimidating Presence",
					FeatureDescription = "you can use your action to frighten someone with your menacing presence. When you do so, choose one creature that you can see within 30 feet of you. If the creature can see or hear you, it must succeed on a Wisdom saving throw (DC equal to 8 + your proficiency bonus + your Charisma modifier) or be frightened of you until the end of your next turn. On subsequent turns, you can use your action to extend the duration o f this effect on the frightened creature until the end of your next turn. This effect ends if the creature ends its turn out of line of sight or more than 60 feet away from you. If the creature succeeds on its saving throw, you can't use this feature on that creature again for 24 hours.",
					RequiredLevel = 10,
					FeatureForClass = barbarian,
					FeatureForArchetype = berserker
				};
				Feature retaliation = new Feature
				{
					FeatureName = "Retaliation",
					FeatureDescription = "When you take damage from a creature that is within 5 feet of you, you can use your reaction to make a melee weapon attack against that creature.",
					RequiredLevel = 14,
					FeatureForClass = barbarian,
					FeatureForArchetype = berserker
				};
				Feature spiritSeeker = new Feature
				{
					FeatureName = "Spirit Seeker",
					FeatureDescription = "Yours is a path that seeks attunement with the natural world, giving you a kinship with beasts. You gain the ability to cast the beast sense and speak with animals spells, but only as rituals.",
					RequiredLevel = 3,
					FeatureForClass = barbarian,
					FeatureForArchetype = totemWarrior
				};
				Feature totemSpirit = new Feature
				{
					FeatureName = "Totem Spirit",
					FeatureDescription = "You choose a totem spirit and gain its feature. You must make or acquire a physical totem object - an amulet or similar adornment—that incorporates fur or feathers, claws, teeth, or bones of the totem animal. At your option, you also gain minor physical attributes that are reminiscent of your totem spirit.For example, if you have a bear totem spirit, you might be unusually hairy and thickskinned, or if your totem is the eagle, your eyes turn bright yellow. Your totem animal might be an animal related to those listed here but more appropriate to your homeland. For example, you could choose a hawk or vulture in place of an eagle.\r\nBear\r\nWhile raging, you have resistance to all damage except psychic damage. The spirit of the bear makes you tough enough to stand up to any punishment.\r\nEagle \r\nWhile you're raging and aren’t wearing heavy armor, other creatures have disadvantage on opportunity attack rolls against you, and you can use the Dash action as a bonus action on your turn.The spirit of the eagle makes you into a predator w ho can weave through the fray with ease. \r\nWolf, While you're raging, your friends have advantage on melee attack rolls against any creature within 5 feet o f you that is hostile to you. The spirit of the wolf makes you a leader of hunters.",
					RequiredLevel = 3,
					FeatureForClass = barbarian,
					FeatureForArchetype = totemWarrior
				};
				Feature aspectOfTheBeast = new Feature
				{
					FeatureName = "Aspect of the Beast",
					FeatureDescription = "At 6th level, you gain a magical benefit based on the totem animal o f your choice.You can choose the same animal you selected at 3rd level or a different one. \r\nBear\r\nYou gain the might of a bear.Your carrying capacity(including maximum load and maximum lift) is doubled, and you have advantage on Strength checks made to push, pull, lift, or break objects. \r\nEagle\r\nYou gain the eyesight of an eagle. You can see up to 1 mile away with no difficulty, able to discern even fine details as though looking at something no more than 100 feet away from you.Additionally, dim light doesn't impose disadvantage on your Wisdom (Perception) checks. \r\nWolf\r\nYou gain the hunting sensibilities of a wolf.You can track other creatures while traveling at a fast pace, and you can move stealthily while traveling at a normal pace (see chapter 8 for rules on travel pace).",
					RequiredLevel = 6,
					FeatureForClass = barbarian,
					FeatureForArchetype = totemWarrior
				};
				Feature spiritWalker = new Feature
				{
					FeatureName = "Spirit Walker",
					FeatureDescription = "You can cast the commune with nature spell, but only as a ritual. When you do so, a spiritual version of one of the animals you chose for Totem Spirit or Aspect of the Beast appears to you to convey the information you seek.",
					RequiredLevel = 10,
					FeatureForClass = barbarian,
					FeatureForArchetype = totemWarrior
				};
				Feature totemicAttunement = new Feature
				{
					FeatureName = "Totemic Attunement",
					FeatureDescription = "You gain a magical benefit based on a totem animal of your choice.You can choose the same animal you selected previously or a different one.\r\nBear\r\nWhile you’re raging, any creature within 5 feet of you that’s hostile to you has disadvantage on attack rolls against targets other than you or another character with this feature.An enemy is immune to this effect if it can’t see or hear you or if it can’t be frightened.\r\nEagle\r\nWhile raging, you have a flying speed equal to your current walking speed. This benefit works only in short bursts; you fall if you end your turn in the air and nothing else is holding you aloft.\r\nWolf\r\nWhile you’re raging, you can use a bonus action on your turn to knock a Large or smaller creature prone when you hit it with melee weapon attack.",
					RequiredLevel = 14,
					FeatureForClass = barbarian,
					FeatureForArchetype = totemWarrior
				};
				db.Features.Add(rage);
				db.Features.Add(unarmoredDefense);
				db.Features.Add(recklessAttack);
				db.Features.Add(dangerSense);
				db.Features.Add(extraAttack);
				db.Features.Add(fastMovement);
				db.Features.Add(feralInstinct);
				db.Features.Add(brutalCritical);
				db.Features.Add(relentlessRage);
				db.Features.Add(persistantRage);
				db.Features.Add(indomitableMight);
				db.Features.Add(primalChampion);
				db.Features.Add(frenzy);
				db.Features.Add(mindlessRage);
				db.Features.Add(intimidatingPresence);
				db.Features.Add(retaliation);
				db.Features.Add(spiritSeeker);
				db.Features.Add(totemSpirit);
				db.Features.Add(aspectOfTheBeast);
				db.Features.Add(spiritWalker);
				db.Features.Add(totemicAttunement);
				#endregion Barbarian Features
				#region Bard Features
				Feature bardicInspiration = new Feature
				{
					FeatureName = "Bardic Inspiration",
					FeatureDescription = "You can inspire others through stirring w ords or music. To do so, you use a bonus action on your turn to choose one creature other than yourself within 60 feet of you who can hear you. That creature gains one Bardic Inspiration die, a d6. Once within the next 10 minutes, the creature can roll the die and add the number rolled to one ability check, attack roll, or saving throw it makes.The creature can wait until after it rolls the d20 before deciding to use the Bardic Inspiration die, but must decide before the DM says whether the roll succeeds or fails. Once the Bardic Inspiration die is rolled, it is lost. A creature can have only one Bardic Inspiration die at a time.You can use this feature a number of times equal to your Charisma modifier(a minimum of once). You regain any expended uses when you finish a long rest. Your Bardic Inspiration die changes when you reach certain levels in this class. The die becomes a d8 at 5th level, a d 10 at 10th level, and a d l2 at 15th level.",
					RequiredLevel = 1,
					FeatureForClass = bard
				};
				Feature jackOfAllTrades = new Feature
				{
					FeatureName = "Jack of all Trades",
					FeatureDescription = "You can add half your proficiency bonus, rounded down, to any ability check you make that doesn’t already include your proficiency bonus.",
					RequiredLevel = 2,
					FeatureForClass = bard
				};
				Feature songOfRest = new Feature
				{
					FeatureName = "Song of Rest",
                    FeatureDescription = "Beginning at 2nd level, you can use soothing music or oration to help revitalize your wounded allies during a short rest.If you or any friendly creatures who can hear your performance regain hit points at the end of the short rest, each of those creatures regains an extra 1d6 hit points. The extra hit points increase when you reach certain levels in this class: to 1d8 at 9th level, to 1d 10 at 13th level, and to 1d 12 at 17th level.",
					RequiredLevel = 2,
					FeatureForClass = bard
				};
				Feature expertiseBard = new Feature
				{
					FeatureName = "Expertise",
					FeatureDescription = "Choose two of your skill proficiencies. Your proficiency bonus is doubled for any ability check you make that uses either o f the chosen proficiencies. At 10th level, you can choose another two skill proficiencies to gain this benefit.",
					RequiredLevel = 3,
					FeatureForClass = bard
				};
				Feature fontOfInspiration = new Feature
				{
					FeatureName = "Font of Inspiration",
					FeatureDescription = "Beginning when you reach 5th level, you regain all of your expended uses o f Bardic Inspiration when you finish a short or long rest.",
					RequiredLevel = 5,
					FeatureForClass = bard
				};
				Feature countercharm = new Feature
				{
					FeatureName = "Countercharm",
					FeatureDescription = "You gain the ability to use musical notes or words of power to disrupt mind-influencing effects. As an action, you can start a performance that lasts until the end of your next turn. During that time, you and any friendly creatures within 30 feet of you have advantage on saving throws against being frightened or charmed. A creature must be able to hear you to gain this benefit. The performance ends early if you are incapacitated or silenced or if you voluntarily end it(no action required).",
					RequiredLevel = 6,
					FeatureForClass = bard
				};
				Feature magicalSecrets = new Feature
				{
					FeatureName = "Magical Secrets",
					FeatureDescription = "you have plundered magical knowledge from a wide spectrum of disciplines.Choose two spells from any class, including this one.A spell you choose must be of a level you can cast, as shown on the Bard table, or a cantrip. The chosen spells count as bard spells for you and are included in the number in the Spells Known column of the Bard table. You learn two additional spells from any class at 14th level and again at 18th level.",
					RequiredLevel = 10,
					FeatureForClass = bard
				};
				Feature superiorInspiration = new Feature
				{
					FeatureName = "Superior Inspiration",
					FeatureDescription = "When you roll initiative and have no uses of Bardic Inspiration left, you regain one use.",
					RequiredLevel = 20,
					FeatureForClass = bard
				};
				Feature bonusProficienciesLore = new Feature
				{
					FeatureName = "Bonus Proficiencies",
					FeatureDescription = "You gain proficiency with three skills of your choice.",
					RequiredLevel = 3,
					FeatureForClass = bard,
					FeatureForArchetype = collegeOfLore
				};
				Feature cuttingWords = new Feature
				{
					FeatureName = "Cutting Words",
					FeatureDescription = "You learn how to use your wit to distract, confuse, and otherwise sap the confidence and competence of others.When a creature that you can see within 60 feet of you makes an attack roll, an ability check, or a damage roll, you can use your reaction to expend one of your uses of Bardic Inspiration, rolling a Bardic Inspiration die and subtracting the number rolled from the creature’s roll.You can choose to use this feature after the creature makes its roll, but before the DM determines whether the attack roll or ability check succeeds or fails, or before the creature deals its damage.The creature is immune if it can’t hear you or if it’s immune to being charmed.",
					RequiredLevel = 3,
					FeatureForClass = bard,
					FeatureForArchetype = collegeOfLore
				};
				Feature additionalMagicalSecrets = new Feature
				{
					FeatureName = "Additional Magical Secrets",
					FeatureDescription = "You learn two spells of your choice from any class. A spell you choose must be of a level you can cast, as shown on the Bard table, or a cantrip.The chosen spells count as bard spells for you but don’t count against the number o f bard spells you know.",
					RequiredLevel = 6,
					FeatureForClass = bard,
					FeatureForArchetype = collegeOfLore
				};
				Feature peerlessSkill = new Feature
				{
					FeatureName = "Peerless Skill",
					FeatureDescription = "When you make an ability check, you can expend one use of Bardic Inspiration.Roll a Bardic Inspiration die and add the number rolled to your ability check.You can choose to do so after you roil the die for the ability check, but before the DM tells you whether you succeed or fail.",
					RequiredLevel = 14,
					FeatureForClass = bard,
					FeatureForArchetype = collegeOfLore
				};
				Feature bonusProficienciesValor = new Feature
				{
					FeatureName = "Bonus Proficiencies",
					FeatureDescription = "You gain proficiency with medium armor, shields, and martial weapons.",
					RequiredLevel = 3,
					FeatureForClass = bard,
					FeatureForArchetype = collegeOfValor
				};
				Feature combatInspiration = new Feature
				{
					FeatureName = "Combat Inspiration",
					FeatureDescription = "You learn to inspire others in battle. A creature that has a Bardic Inspiration die from you can roll that die and add the number rolled to a weapon damage roll it just made.Alternatively, when an attack roll is made against the creature, it can use its reaction to roll the Bardic Inspiration die and add the number rolled to its AC against that attack, after seeing the roll but before knowing whether it hits or misses.",
					RequiredLevel = 3,
					FeatureForClass = bard,
					FeatureForArchetype = collegeOfValor
				};
				Feature extraAttackBard = new Feature
				{
					FeatureName = "Extra Attack",
					FeatureDescription = "You can attack twice, instead of once, whenever you take the Attack action on your turn.",
					RequiredLevel = 6,
					FeatureForClass = bard,
					FeatureForArchetype = collegeOfValor
				};
				Feature battleMagic = new Feature
				{
					FeatureName = "Battle Magic",
					FeatureDescription = "You have mastered the art of weaving spellcasting and weapon use into a single harmonious act.When you use your action to cast a bard spell, you can make one weapon attack as a bonus action.",
					RequiredLevel = 14,
					FeatureForClass = bard,
					FeatureForArchetype = collegeOfValor
				};
				db.Features.Add(bardicInspiration);
				db.Features.Add(jackOfAllTrades);
				db.Features.Add(songOfRest);
				db.Features.Add(expertiseBard);
				db.Features.Add(bardicInspiration);
				db.Features.Add(fontOfInspiration);
				db.Features.Add(countercharm);
				db.Features.Add(magicalSecrets);
				db.Features.Add(superiorInspiration);
				db.Features.Add(bonusProficienciesLore);
				db.Features.Add(cuttingWords);
				db.Features.Add(additionalMagicalSecrets);
				db.Features.Add(peerlessSkill);
				db.Features.Add(bonusProficienciesValor);
				db.Features.Add(combatInspiration);
				db.Features.Add(extraAttackBard);
				db.Features.Add(battleMagic);
				#endregion Bard Features
				#region Cleric Features
				Feature channelDivinityCleric = new Feature
				{
					FeatureName = "Channel Divinity",
					FeatureDescription = "You gain the ability to channel divine energy directly from your deity, using that energy to fuel magical effects. You start with two such effects: Turn Undead and an effect determined by your domain. Some domains grant you additional effects as you advance in levels, as noted in the domain description. When you use your Channel Divinity, you choose which effect to create. You must then finish a short or long rest to use your Channel Divinity again. Some Channel Divinity effects require saving throws. When you use such an effect from this class, the DC equals your cleric spell save DC. Beginning at 6th level, you can use your Channel Divinity twice between rests, and beginning at 18th level. You can use it three times between rests. When you finish a short or long rest, you regain your expended uses.",
                    RequiredLevel = 2,
					FeatureForClass = cleric
				};
				Feature turnUndead = new Feature
				{
					FeatureName = "Channel Divinity: Turn Undead",
					FeatureDescription = "As an action, you present your holy symbol and speak a prayer censuring the undead. Each undead that can see or hear you within 30 feet of you must make a Wisdom saving throw. If the creature fails its saving throw, it is turned for 1 minute or until it takes any damage. A turned creature must spend its turns trying to move as far away from you as it can, and it can’t willingly move to a space within 30 feet of you. It also can’t take reactions. For its action, it can use only the Dash action or try to escape from an effect that prevents it from moving. If there’s nowhere to move, the creature can use the Dodge action.",
					RequiredLevel = 2,
					FeatureForClass = cleric
				};
				Feature destroyUndead = new Feature
				{
					FeatureName = "Destroy Undead",
					FeatureDescription = "Starting at 5th level, when an undead fails its saving throw against your Turn Undead feature, the creature is instantly destroyed if its challenge rating is at or below a certain threshold.",
					RequiredLevel = 5,
					FeatureForClass = cleric,
				};
				Feature divineIntervention = new Feature
				{
					FeatureName = "Divine Intervention",
					FeatureDescription = "You can call on your deity to intervene on your behalf when your need is great. Imploring your deity’s aid requires you to use your action.Describe the assistance you seek, and roll percentile dice.If you roll a number equal to or lower than your cleric level, your deity intervenes.The DM chooses the nature o f the intervention; the effect of any cleric spell or cleric domain spell would be appropriate. If your deity intervenes, you can’t use this feature again for 7 days.Otherwise, you can use it again after you finish a long rest. At 20th level, your call for intervention succeeds automatically, no roll required.",
					RequiredLevel = 10,
					FeatureForClass = cleric,
				};
				Feature blessingsOfKnowledge = new Feature
				{
					FeatureName = "Blessings of Knowledge",
					FeatureDescription = "You learn two languages of your choice. You also become proficient in your choice of two of the following skills: Arcana, History, Nature, or Religion. Your proficiency bonus is doubled for any ability check you make that uses either o f those skills.",
					RequiredLevel = 1,
					FeatureForClass = cleric,
					FeatureForArchetype = knowledgeDomain
				};
				Feature knowledgeOfTheAges = new Feature
				{
					FeatureName = "Channel Divinity: Knowledge of the Ages",
					FeatureDescription = "You can use your Channel Divinity to tap into a divine well o f knowledge.As an action, you choose one skill or tool.For 10 minutes, you have proficiency with the chosen skill or tool.",
					RequiredLevel = 2,
					FeatureForClass = cleric,
					FeatureForArchetype = knowledgeDomain
				};
				Feature readThoughts = new Feature
				{
					FeatureName = "Channel Divinity: Read Thoughts",
					FeatureDescription = "You can use your Channel Divinity to read a creature’s thoughts.You can then use your access to the creature’s mind to command it. As an action, choose one creature that you can see within 60 feet of you.That creature must make a Wisdom saving throw.If the creature succeeds on the saving throw, you can’t use this feature on it again until you finish a long rest. If the creature fails its save, you can read its surface thoughts(those foremost in its mind, reflecting its current emotions and what it is actively thinking about) when it is within 60 feet of you. This effect lasts for 1 minute. During that time, you can use your action to end this effect and cast the suggestion spell on the creature without expending a spell slot.The target automatically fails its saving throw against the spell.",
					RequiredLevel = 6,
					FeatureForClass = cleric,
					FeatureForArchetype = knowledgeDomain
				};
				Feature potentSpellcastingKnowledge = new Feature
				{
					FeatureName = "Potent Spellcasting",
					FeatureDescription = "You add your Wisdom modifier to the damage you deal with any cleric cantrip.",
					RequiredLevel = 8,
					FeatureForClass = cleric,
					FeatureForArchetype = knowledgeDomain
				};
				Feature visionsOfThePast = new Feature
				{
					FeatureName = "Visions of the Past",
					FeatureDescription = "Starting at 17th level, you can call up visions of the past that relate to an object you hold or your immediate surroundings.You spend at least 1 minute in meditation and prayer, then receive dreamlike, shadowy glimpses of recent events. You can meditate in this way for a number of minutes equal to your Wisdom score and must maintain concentration during that time, as if you were casting a spell. Once you use this feature, you can’t use it again until you finish a short or long rest. Object Reading. Holding an object as you meditate, you can see visions of the object’s previous owner. After meditating for 1 minute, you learn how the owner acquired and lost the object, as well as the most recent significant event involving the object and that owner. If the object was owned by another creature in the recent past (within a number of days equal to your Wisdom score), you can spend 1 additional minute for each owner to learn the same information about that creature. Area Reading.As you meditate, you see visions of recent events in your immediate vicinity (a room, street, tunnel, clearing, or the like, up to a 50-foot cube), going back a number of days equal to your Wisdom score. For each minute you meditate, you learn about one significant event, beginning with the most recent. Significant events typically involve powerful emotions, such as battles and betrayals, marriages and murders, births and funerals. However, they might also include more mundane events that are nevertheless important in your current situation.",
					RequiredLevel = 17,
					FeatureForClass = cleric,
					FeatureForArchetype = knowledgeDomain
				};
				Feature bonusProficiencyLife = new Feature
				{
					FeatureName = "Bonus Proficiency",
					FeatureDescription = "You gain proficiency with heavy armor.",
					RequiredLevel = 1,
					FeatureForClass = cleric,
					FeatureForArchetype = lifeDomain
				};
				Feature discipleOfLife = new Feature
				{
					FeatureName = "Disciple of Life",
					FeatureDescription = "Your healing spells are more effective. Whenever you use a spell of 1st level or higher to restore hit points to a creature, the creature regains additional hit points equal to 2 + the spell’s level.",
					RequiredLevel = 1,
					FeatureForClass = cleric,
					FeatureForArchetype = lifeDomain
				};
				Feature preserveLife = new Feature
				{
					FeatureName = "Channel Divinity: Preserve Life",
					FeatureDescription = "You can use your Channel Divinity to heal the badly injured. As an action, you present your holy symbol and evoke healing energy that can restore a number of hit points equal to five times your cleric level. Choose any creatures within 30 feet of you, and divide those hit points among them.This feature can restore a creature to no more than half o f its hit point maximum. You can’t use this feature on an undead or a construct.",
					RequiredLevel = 2,
					FeatureForClass = cleric,
					FeatureForArchetype = lifeDomain
				};
				Feature blessedHealer = new Feature
				{
					FeatureName = "Blessed Healer",
					FeatureDescription = "The healing spells you cast on others heal you as well. When you cast a spell of 1st level or higher that restores hit points to a creature other than you, you regain hit points equal to 2 + the spell’s level.",
					RequiredLevel = 6,
					FeatureForClass = cleric,
					FeatureForArchetype = lifeDomain
				};
				Feature divineStrikeLife = new Feature
				{
					FeatureName = "Divine Strike",
					FeatureDescription = "You gain the ability to infuse your weapon strikes with divine energy.Once on each of your turns when you hit a creature with a weapon attack, you can cause the attack to deal an extra 1d8 radiant damage to the target.When you reach 14th level, the extra damage increases to 2d8.",
					RequiredLevel = 8,
					FeatureForClass = cleric,
					FeatureForArchetype = lifeDomain
				};
				Feature supremeHealing = new Feature
				{
					FeatureName = "Supreme Healing",
					FeatureDescription = "When you would normally roll one or more dice to restore hit points with a spell, you instead use the highest number possible for each die. For example, instead of restoring 2d6 hit points to a creature, you restore 12.",
					RequiredLevel = 17,
					FeatureForClass = cleric,
					FeatureForArchetype = lifeDomain
				};
				Feature bonusCantripLight = new Feature
				{
					FeatureName = "Bonus Cantrip",
					FeatureDescription = "You learn the light cantrip.",
					RequiredLevel = 1,
					FeatureForClass = cleric,
					FeatureForArchetype = lightDomain
				};
				Feature wardingFlare = new Feature
				{
					FeatureName = "Warding Flare",
					FeatureDescription = "You can interpose divine light between yourself and an attacking enemy.When you are attacked by a creature within 30 feet of you that you can see, you can use your reaction to impose disadvantage on the attack roll, causing light to flare before the attacker before it hits or misses.An attacker that can’t be blinded is immune to this feature. You can use this feature a number of times equal to your Wisdom modifier (a minimum of once).You regain all expended uses when you finish a long rest.",
					RequiredLevel = 1,
					FeatureForClass = cleric,
					FeatureForArchetype = lightDomain
				};
				Feature radianceOfTheDawn = new Feature
				{
					FeatureName = "Channel Divinity: Radiance of the Dawn",
					FeatureDescription = "You can use your Channel Divinity to harness sunlight, banishing darkness and dealing radiant damage to your foes. As an action, you present your holy symbol, and any magical darkness within 30 feet of you is dispelled. Additionally, each hostile creature within 30 feet of you must make a Constitution saving throw.A creature takes radiant damage equal to 2d10 + your cleric level on a failed saving throw, and half as much damage on a successful one.A creature that has total cover from you is not affected.",
					RequiredLevel = 2,
					FeatureForClass = cleric,
					FeatureForArchetype = lightDomain
				};
				Feature improvedFlare = new Feature
				{
					FeatureName = "Improved Flare",
					FeatureDescription = "You can also use your Warding Flare feature when a creature that you can see within 30 feet of you attacks a creature other than you.",
					RequiredLevel = 6,
					FeatureForClass = cleric,
					FeatureForArchetype = lightDomain
				};
				Feature potentSpellcastingLight = new Feature
				{
					FeatureName = "Potent Spellcasting",
					FeatureDescription = "You add your Wisdom modifier to the damage you deal with any cleric cantrip.",
					RequiredLevel = 8,
					FeatureForClass = cleric,
					FeatureForArchetype = lightDomain
				};
				Feature coronaOfLight = new Feature
				{
					FeatureName = "Corona of Light",
					FeatureDescription = "You can use your action to activate an aura of sunlight that lasts for 1 minute or until you dismiss it using another action. You emit bright light in a 60-foot radius and dim light 30 feet beyond that. Your enemies in the bright light have disadvantage on saving throws against any spell that deals fire or radiant damage.",
					RequiredLevel = 17,
					FeatureForClass = cleric,
					FeatureForArchetype = lightDomain
				};
				Feature acolyteOfNature = new Feature
				{
					FeatureName = "Acolyte of Nature",
					FeatureDescription = "You learn one druid cantrip of your choice. You also gain proficiency in one of the following skills of your choice: Animal Handling, Nature, or Survival.",
					RequiredLevel = 1,
					FeatureForClass = cleric,
					FeatureForArchetype = natureDomain
				};
				Feature bonusProficiencyNature = new Feature
				{
					FeatureName = "Bonus Proficiency",
					FeatureDescription = "You gain proficiency with heavy armor.",
					RequiredLevel = 1,
					FeatureForClass = cleric,
					FeatureForArchetype = natureDomain
				};
				Feature charmAnimalsAndPlants = new Feature
				{
					FeatureName = "Channel Divinity: Charm Animals and Plants.",
					FeatureDescription = "You can use your Channel Divinity to charm animals and plants. As an action, you present your holy symbol and invoke the name of your deity. Each beast or plant creature that can see you within 30 feet of you must make a Wisdom saving throw. If the creature fails its saving throw, it is charmed by you for 1 minute or until it takes damage. While it is charmed by you, it is friendly to you and other creatures you designate.",
					RequiredLevel = 2,
					FeatureForClass = cleric,
					FeatureForArchetype = natureDomain
				};
				Feature dampenElements = new Feature
				{
					FeatureName = "Dampen Elements",
					FeatureDescription = "When you or a creature within 30 feet of you takes acid, cold, fire, lightning, or thunder damage, you can use your reaction to grant resistance to the creature against that instance of the damage.",
					RequiredLevel = 6,
					FeatureForClass = cleric,
					FeatureForArchetype = natureDomain
				};
				Feature divineStrikeNature = new Feature
				{
					FeatureName = "Divine Strike",
					FeatureDescription = "you gain the ability to infuse your weapon strikes with divine energy. Once on each of your turns when you hit a creature with a weapon attack, you can cause the attack to deal an extra 1d8 cold, fire, or lightning damage(your choice) to the target. When you reach 14th level, the extra damage increases to 2d8.",
					RequiredLevel = 8,
					FeatureForClass = cleric,
					FeatureForArchetype = natureDomain
				};
				Feature masterOfNature = new Feature
				{
					FeatureName = "Master of Nature",
					FeatureDescription = "You gain the ability to command animals and plant creatures.While creatures are charmed by your Charm Animals and Plants feature, you can take a bonus action on your turn to verbally command what each of those creatures will do on its next turn.",
					RequiredLevel = 17,
					FeatureForClass = cleric,
					FeatureForArchetype = natureDomain
				};
				Feature bonusProficienciesTempest = new Feature
				{
					FeatureName = "Bonus Proficiencies",
					FeatureDescription = "You gain proficiency with martial weapons and heavy armor.",
					RequiredLevel = 1,
					FeatureForClass = cleric,
					FeatureForArchetype = tempestDomain
				};
				Feature wrathOfTheStorm = new Feature
				{
					FeatureName = "Wrath of the Storm",
					FeatureDescription = "You can thunderously rebuke attackers. When a creature within 5 feet of you that you can see hits you with an attack, you can use your reaction to cause the creature to make a Dexterity saving throw. The creature takes 2d8 lightning or thunder damage (your choice) on a failed saving throw, and half as much damage on a successful one. You can use this feature a number of times equal to your Wisdom modifier(a minimum of once). You regain all expended uses when you finish a long rest.",
					RequiredLevel = 1,
					FeatureForClass = cleric,
					FeatureForArchetype = tempestDomain
				};
				Feature destructiveWrath = new Feature
				{
					FeatureName = "Channel Divinity: Destructive Wrath",
					FeatureDescription = "You can use your Channel Divinity to wield the power o f the storm with unchecked ferocity. When you roll lightning or thunder damage, you can use your Channel Divinity to deal maximum damage, instead of rolling.",
					RequiredLevel = 2,
					FeatureForClass = cleric,
					FeatureForArchetype = tempestDomain
				};
				Feature thunderboltStrike = new Feature
				{
					FeatureName = "Thunderbolt Strike",
					FeatureDescription = "When you deal lightning damage to a Large or smaller creature, you can also push it up to 10 feet away from you.",
					RequiredLevel = 6,
					FeatureForClass = cleric,
					FeatureForArchetype = tempestDomain
				};
				Feature divineStrikeTempest = new Feature
				{
					FeatureName = "Divine Strike",
					FeatureDescription = "At 8th level, you gain the ability to infuse your weapon strikes with divine energy. Once on each of your turns when you hit a creature with a weapon attack, you can cause the attack to deal an extra 1d8 thunder damage to the target.When you reach 14th level, the extra damage increases to 2d8.",
					RequiredLevel = 8,
					FeatureForClass = cleric,
					FeatureForArchetype = tempestDomain
				};
				Feature stormBorn = new Feature
				{
					FeatureName = "Stormborn",
					FeatureDescription = "You have a flying speed equal to your current walking speed whenever you are not underground or indoors.",
					RequiredLevel = 17,
					FeatureForClass = cleric,
					FeatureForArchetype = tempestDomain
				};
				Feature blessingOfTheTrickster = new Feature
				{
					FeatureName = "Blessing of the Trickster",
					FeatureDescription = "You can use your action to touch a willing creature other than yourself to give it advantage on Dexterity(Stealth) checks. This blessing lasts for 1 hour or until you use this feature again.",
					RequiredLevel = 1,
					FeatureForClass = cleric,
					FeatureForArchetype = trickeryDomain
				};
				Feature invokeDuplicity = new Feature
				{
					FeatureName = "Channel Divinity: Invoke Duplicity",
					FeatureDescription = "You can use your Channel Divinity to create an illusory duplicate of yourself. As an action, you create a perfect illusion of yourself that lasts for 1 minute, or until you lose your concentration(as if you were concentrating on a spell). The illusion appears in an unoccupied space that you can see within 30 feet of you. As a bonus action on your turn, you can move the illusion up to 30 feet to a space you can see, but it must remain within 120 feet of you. For the duration, you can cast spells as though you were in the illusion’s space, but you must use your own senses.Additionally, when both you and your illusion are within 5 feet o f a creature that can see the illusion, you have advantage on attack rolls against that creature, given how distracting the illusion is to the target.",
					RequiredLevel = 2,
					FeatureForClass = cleric,
					FeatureForArchetype = trickeryDomain
				};
				Feature cloakOfShadowsTrickery = new Feature
				{
					FeatureName = "Channel Divinity: Cloak of Shadows",
					FeatureDescription = "You can use your Channel Divinity to vanish. As an action, you become invisible until the end of your next turn.You become visible if you attack or cast a spell.",
					RequiredLevel = 6,
					FeatureForClass = cleric,
					FeatureForArchetype = trickeryDomain
				};
				Feature divineStrikeTrickery = new Feature
				{
					FeatureName = "Divine Strike",
					FeatureDescription = "You gain the ability to infuse your weapon strikes with poison—a gift from your deity. Once on each of your turns when you hit a creature with a weapon attack, you can cause the attack to deal an extra 1d8 poison damage to the target. When you reach 14th level, the extra damage increases to 2d8.",
					RequiredLevel = 8,
					FeatureForClass = cleric,
					FeatureForArchetype = trickeryDomain
				};
				Feature improvedDuplicity = new Feature
				{
					FeatureName = "Improved Duplicity",
					FeatureDescription = "You can create up to four duplicates of yourself, instead of one, when you use Invoke Duplicity. As a bonus action on your turn, you can move any number of them up to 30 feet, to a maximum range o f 120 feet.",
					RequiredLevel = 17,
					FeatureForClass = cleric,
					FeatureForArchetype = trickeryDomain
				};
				Feature bonusProficienciesWar = new Feature
				{
					FeatureName = "Bonus Proficiencies",
					FeatureDescription = "You gain proficiency with martial weapons and heavy armor.",
					RequiredLevel = 1,
					FeatureForClass = cleric,
					FeatureForArchetype = warDomain
				};
				Feature warPriest = new Feature
				{
					FeatureName = "War Priest",
					FeatureDescription = "Your god delivers bolts of inspiration to you while you are engaged in battle. When you use the Attack action, you can make one weapon attack as a bonus action. You can use this feature a number of times equal to your Wisdom modifier (a minimum of once). You regain all expended uses when you finish a long rest.",
					RequiredLevel = 1,
					FeatureForClass = cleric,
					FeatureForArchetype = warDomain
				};
				Feature guidedStrike = new Feature
				{
					FeatureName = "Channel Divinity: Guided Strike",
					FeatureDescription = "You can use your Channel Divinity to strike with supernatural accuracy. When you make an attack roll, you can use your Channel Divinity to gain a + 10 bonus to the roll.You make this choice after you see the roll, but before the DM says whether the attack hits or misses.",
					RequiredLevel = 2,
					FeatureForClass = cleric,
					FeatureForArchetype = warDomain
				};
				Feature warGodsBlessing = new Feature
				{
					FeatureName = "Channel Divinity: War God's Blessing",
					FeatureDescription = "When a creature within 30 feet of you makes an attack roll, you can use your reaction to grant that creature a +10 bonus to the roll, using your Channel Divinity. You make this choice after you see the roll, but before the DM says whether the attack hits or misses.",
					RequiredLevel = 6,
					FeatureForClass = cleric,
					FeatureForArchetype = warDomain
				};
				Feature divineStrikeWar = new Feature
				{
					FeatureName = "Divine Strike",
					FeatureDescription = "You gain the ability to infuse your weapon strikes with divine energy. Once on each of your turns when you hit a creature with a weapon attack, you can cause the attack to deal an extra 1d8 damage of the same type dealt by the weapon to the target. When you reach 14th level, the extra damage increases to 2d8.",
					RequiredLevel = 8,
					FeatureForClass = cleric,
					FeatureForArchetype = warDomain
				};
				Feature avatarOfBattle = new Feature
				{
					FeatureName = "Avatar of Battle",
					FeatureDescription = "You gain resistance to bludgeoning, piercing and slashing damage from nonmagical weapons.",
					RequiredLevel = 17,
					FeatureForClass = cleric,
					FeatureForArchetype = warDomain
				};
				db.Features.Add(channelDivinityCleric);
				db.Features.Add(turnUndead);
				db.Features.Add(destroyUndead);
				db.Features.Add(divineIntervention);
				db.Features.Add(blessingsOfKnowledge);
				db.Features.Add(knowledgeOfTheAges);
				db.Features.Add(readThoughts);
				db.Features.Add(potentSpellcastingKnowledge);
				db.Features.Add(visionsOfThePast);
				db.Features.Add(bonusProficiencyLife);
				db.Features.Add(discipleOfLife);
				db.Features.Add(preserveLife);
				db.Features.Add(blessedHealer);
				db.Features.Add(divineStrikeLife);
				db.Features.Add(supremeHealing);
				db.Features.Add(bonusCantripLight);
				db.Features.Add(wardingFlare);
				db.Features.Add(radianceOfTheDawn);
				db.Features.Add(improvedFlare);
				db.Features.Add(potentSpellcastingLight);
				db.Features.Add(coronaOfLight);
				db.Features.Add(acolyteOfNature);
				db.Features.Add(bonusProficiencyNature);
				db.Features.Add(charmAnimalsAndPlants);
				db.Features.Add(dampenElements);
				db.Features.Add(divineStrikeNature);
				db.Features.Add(masterOfNature);
				db.Features.Add(bonusProficienciesTempest);
				db.Features.Add(wrathOfTheStorm);
				db.Features.Add(destructiveWrath);
				db.Features.Add(thunderboltStrike);
				db.Features.Add(divineStrikeTempest);
				db.Features.Add(stormBorn);
				db.Features.Add(blessingOfTheTrickster);
				db.Features.Add(invokeDuplicity);
				db.Features.Add(cloakOfShadowsTrickery);
				db.Features.Add(divineStrikeTrickery);
				db.Features.Add(improvedDuplicity);
				db.Features.Add(bonusProficienciesWar);
				db.Features.Add(warPriest);
				db.Features.Add(guidedStrike);
				db.Features.Add(warGodsBlessing);
				db.Features.Add(divineStrikeWar);
				db.Features.Add(avatarOfBattle);
				#endregion Cleric Features
				#region Druid Features
				Feature druidicFeature = new Feature
				{
					FeatureName = "Druidic",
					FeatureDescription = "You know Druidic, the secret language of druids. You can speak the language and use it to leave hidden messages. You and others who know this language automatically spot such a message. Others spot the message’s presence with a successful DC 15 Wisdom(Perception) check but can’t decipher it without magic.",
					RequiredLevel = 1,
					FeatureForClass = druid,
				};
				Feature wildShape = new Feature
				{
					FeatureName = "Wild Shape",
					FeatureDescription = "Starting at 2nd level, you can use your action to magically assume the shape of a beast that you have seen before. You can use this feature twice. You regain expended uses when you finish a short or long rest. Your druid level determines the beasts you can transform into, as shown in the Beast Shapes table. At 2nd level, for example, you can transform into any beast that has a challenge rating of 1/4 or lower that doesn’t have a flying or swimming speed.\r\nBeast Shapes\r\nLevel Max.CR Limitations Example \r\n2nd 1 / 4 No flying or swimming speed Wolf \r\n4th 1 / 2 No flying speed Crocodile \r\n8th 1 — Giant eagle\r\nYou can stay in a beast shape for a number of hours equal to half your druid level(rounded down).You then revert to your normal form unless you expend another use of this feature.You can revert to your normal form earlier by using a bonus action on your turn. You automatically revert if you fall unconscious, drop to 0 hit points, or die. While you are transformed, the following rules apply:\r\nYour game statistics are replaced by the statistics of the beast, but you retain your alignment, personality, and Intelligence, Wisdom, and Charisma scores. You also retain all of your skill and saving throw proficiencies, in addition to gaining those o f the creature. If the creature has the same proficiency as you and the bonus in its stat block is higher than yours, use the creature’s bonus instead of yours.If the creature has any legendary or lair actions, you can't use them.\r\nWhen you transform, you assume the beast’s hit points and Hit Dice. When you revert to your normal form, you return to the number of hit points you had before you transformed.However, if you revert as a result of dropping to 0 hit points, any excess damage carries over to your normal form. For example, if you take 10 damage in animal form and have only 1 hit point left, you revert and take 9 damage.As long as the excess damage doesn’t reduce your normal form to 0 hit points, you aren’t knocked unconscious.\r\nYou can’t cast spells, and your ability to speak or take any action that requires hands is limited to the capabilities of your beast form. Transforming doesn’t break your concentration on a spell you’ve already cast, however, or prevent you from taking actions that are part of a spell, such as call lightning, that you’ve already cast.\r\nYou retain the benefit of any features from your class, race, or other source and can use them if the new form is physically capable of doing so. However, you can’t use any of your special senses, such as darkvision, unless your new form also has that sense.\r\nYou choose whether your equipment falls to the ground in your space, merges into your new form, or is worn by it. Worn equipment functions as normal, but the DM decides whether it is practical for the new form to wear a piece o f equipment, based on the creature’s shape and size.Your equipment doesn’t change size or shape to match the new form, and any equipment that the new form can’t wear must either fall to the ground or merge with it. Equipment that merges with the form has no effect until you leave the form.",
					RequiredLevel = 2,
					FeatureForClass = druid,
				};
				Feature timelessBodyDruid = new Feature
				{
					FeatureName = "Timeless Body",
					FeatureDescription = "The primal magic that you wield causes you to age more slowly. For every 10 years that pass, your body ages only 1 year.",
					RequiredLevel = 18,
					FeatureForClass = druid,
				};
				Feature beastSpells = new Feature
				{
					FeatureName = "Beast Spells",
					FeatureDescription = "You can cast many of your druid spells in any shape you assume using Wild Shape. You can perform the somatic and verbal components of a druid spell while in a beast shape, but you aren’t able to provide material components.",
					RequiredLevel = 18,
					FeatureForClass = druid,
				};
				Feature archdruid = new Feature
				{
					FeatureName = "Archdruid",
					FeatureDescription = "You can use your wild shape an unlimited number of times.",
					RequiredLevel = 20,
					FeatureForClass = druid,
				};
				Feature bonusCantripLand = new Feature
				{
					FeatureName = "Bonus Cantrip",
					FeatureDescription = "You learn one additional druid cantrip of your choice.",
					RequiredLevel = 2,
					FeatureForClass = druid,
					FeatureForArchetype = circleOfTheLand
				};
				Feature naturalRecovery = new Feature
				{
					FeatureName = "Natural Recovery",
					FeatureDescription = "You can regain some of your magical energy by sitting in meditation and communing with nature.During a short rest, you choose expended spell slots to recover. The spell slots can have a combined level that is equal to or less than half your druid level(rounded up), and none of the slots can be 6th level or higher. You can’t use this feature again until you finish a long rest. For example, when you are a 4th - level druid, you can recover up to two levels w orth of spell slots.You can recover either a 2nd - level slot or two 1st - level slots.",
					RequiredLevel = 2,
					FeatureForClass = druid,
					FeatureForArchetype = circleOfTheLand
				};
				Feature circleSpells = new Feature
				{
					FeatureName = "Circle Spells",
					FeatureDescription = "Your mystical connection to the land infuses you with the ability to cast certain spells. At 3rd, 5th, 7th, and 9th level you gain access to circle spells connected to the land where you became a druid. Choose that land—arctic, coast, desert, forest, grassland, mountain, swamp, or Underdark—and consult the associated list of spells. Once you gain access to a circle spell, you always have it prepared, and it doesn’t count against the number of spells you can prepare each day.If you gain access to a spell that doesn’t appear on the druid spell list, the spell is nonetheless a druid spell for you.",
					RequiredLevel = 3,
					FeatureForClass = druid,
					FeatureForArchetype = circleOfTheLand
				};
				Feature landStrideDruid = new Feature
				{
					FeatureName = "Land's Stride",
					FeatureDescription = "Moving through nonmagical difficult terrain costs you no extra movement. You can also pass through nonmagical plants without being slowed by them and without taking damage from them if they have thorns, spines, or a similar hazard. In addition, you have advantage on saving throws against plants that are magically created or manipulated to impede movement, such those created by the entangle spell.",
					RequiredLevel = 6,
					FeatureForClass = druid,
					FeatureForArchetype = circleOfTheLand
				};
				Feature natureWard = new Feature
				{
					FeatureName = "Nature's Ward",
					FeatureDescription = "You can't be charmed or frightened by elementals or fey, and you are immune to poison and disease.",
					RequiredLevel = 10,
					FeatureForClass = druid,
					FeatureForArchetype = circleOfTheLand
				};
				Feature natureSanctuary = new Feature
				{
					FeatureName = "Nature's Sanctuary",
					FeatureDescription = "Creatures of the natural world sense your connection to nature and become hesitant to attack you.When a beast or plant creature attacks you, that creature must make a Wisdom saving throw against your druid spell save DC. On a failed save, the creature must choose a different target, or the attack automatically misses. On a successful save, the creature is immune to this effect for 24 hours. The creature is aware of this effect before it makes its attack against you.",
					RequiredLevel = 14,
					FeatureForClass = druid,
					FeatureForArchetype = circleOfTheLand
				};
				Feature combatWildShape = new Feature
				{
					FeatureName = "Combat Wild Shape",
					FeatureDescription = "You gain the ability to use Wild Shape on your turn as a bonus action, rather than as an action. Additionally, while you are transformed by Wild Shape, you can use a bonus action to expend one spell slot to regain 1d8 hit points per level of the spell slot expended.",
					RequiredLevel = 2,
					FeatureForClass = druid,
					FeatureForArchetype = circleOfTheMoon
				};
				Feature circleForms = new Feature
				{
					FeatureName = "Circle Forms",
					FeatureDescription = "The rites of your circle grant you the ability to transform into more dangerous animal forms. You can use your Wild Shape to transform into a beast with a challenge rating as high as 1 (you ignore the Max CR column of the Beast Shapes table, but must abide by the other limitations there). Starting at 6th level, you can transform into a beast with a challenge rating as high as your druid level divided by 3, rounded down.",
					RequiredLevel = 2,
					FeatureForClass = druid,
					FeatureForArchetype = circleOfTheMoon
				};
				Feature primalStrike = new Feature
				{
					FeatureName = "Primal Strike",
					FeatureDescription = "Your attacks in beast form count as magical for the purpose of overcoming resistance and immunity to nonmagical attacks and damage.",
					RequiredLevel = 6,
					FeatureForClass = druid,
					FeatureForArchetype = circleOfTheMoon
				};
				Feature elementalWildShape = new Feature
				{
					FeatureName = "Elemental Wild Shape",
					FeatureDescription = "You can expend two uses of Wild Shape at the same time to transform into an air elemental, an earth elemental, a fire elemental, or a water elemental.",
					RequiredLevel = 10,
					FeatureForClass = druid,
					FeatureForArchetype = circleOfTheMoon
				};
				Feature thousandForms = new Feature
				{
					FeatureName = "Thousand Forms",
					FeatureDescription = "You have learned to use magic to alter your physical form in more subtle ways. You can cast the alter self spell at will.",
					RequiredLevel = 14,
					FeatureForClass = druid,
					FeatureForArchetype = circleOfTheMoon
				};
                db.Features.Add(druidicFeature);
                db.Features.Add(wildShape);
                db.Features.Add(timelessBodyDruid);
                db.Features.Add(beastSpells);
                db.Features.Add(archdruid);
                db.Features.Add(bonusCantripLand);
                db.Features.Add(naturalRecovery);
                db.Features.Add(circleSpells);
                db.Features.Add(landStrideDruid);
                db.Features.Add(natureWard);
                db.Features.Add(natureSanctuary);
                db.Features.Add(combatWildShape);
                db.Features.Add(circleForms);
                db.Features.Add(primalStrike);
                db.Features.Add(elementalWildShape);
                db.Features.Add(thousandForms);
                db.SaveChanges();
				#endregion Druid Features
				#region Fighter Features
				Feature fightingStyleFighter = new Feature
				{
					FeatureName = "Fighting Style",
					FeatureDescription = "You adopt a particular style o f fighting as your specialty. Choose one of the following options. You can’t take a Fighting Style option more than once, even if you later get to choose again.\r\nArchery\r\nYou gain a +2 bonus to attack rolls you make with ranged weapons.\r\nDefense\r\nWhile you are wearing armor, you gain a +1 bonus to AC. \r\nDueling\r\nWhen you are wielding a melee w eapon in one hand and no other w eapons, you gain a +2 bonus to damage rolls with that weapon.\r\nGreat Weapon Fighting\r\nWhen you roll a 1 or 2 on a damage die for an attack you make with a melee weapon that you are wielding with two hands, you can reroll the die and must use the new roll, even if the new roll is a 1 or a 2. The weapon must have the two-handed or versatile property for you to gain this benefit.\r\nProtection\r\nWhen a creature you can see attacks a target other than you that is within 5 feet of you, you can use your reaction to impose disadvantage on the attack roll. You must be wielding a shield.\r\nTwo-weapon Fighting\r\nWhen you engage in two-weapon fighting, you can add your ability modifier to the damage o f the second attack.",
					RequiredLevel = 1,
					FeatureForClass = fighter,
				};
				Feature secondWind = new Feature
				{
					FeatureName = "Second Wind",
					FeatureDescription = "You have a limited well o f stamina that you can draw on to protect yourself from harm.On your turn, you can use a bonus action to regain hit points equal to 1d10 + your fighter level. Once you use this feature, you must finish a short or long rest before you can use it again.",
					RequiredLevel = 1,
					FeatureForClass = fighter,
				};
				Feature actionSurge = new Feature
				{
					FeatureName = "Action Surge",
					FeatureDescription = "You can push yourself beyond your normal limits for a moment. On your turn, you can take one additional action on top of your regular action and a possible bonus action. Once you use this feature, you must finish a short or long rest before you can use it again.Starting at 17th level, you can use it twice before a rest, but only once on the same turn.",
					RequiredLevel = 1,
					FeatureForClass = fighter,
				};
				Feature extraAttackFighter = new Feature
				{
					FeatureName = "Extra Attack",
					FeatureDescription = "Beginning at 5th level, you can attack twice, instead of once, whenever you take the Attack action on your turn. The number o f attacks increases to three when you reach 11th level in this class and to four when you reach 20th level in this class.",
					RequiredLevel = 5,
					FeatureForClass = fighter,
				};
				Feature indomitable = new Feature
				{
					FeatureName = "Indomitable",
					FeatureDescription = "You can reroll a saving throw that you fail. If you do so, you must use the new roll, and you can’t use this feature again until you finish a long rest. You can use this feature twice between long rests starting at 13th level and three times between long rests starting at 17th level.",
					RequiredLevel = 9,
					FeatureForClass = fighter,
				};
				Feature improvedCritical = new Feature
				{
					FeatureName = "Improved Critical",
					FeatureDescription = "Your weapon attacks score a critical hit on a roll of 19 or 20",
					RequiredLevel = 3,
					FeatureForClass = fighter,
					FeatureForArchetype = champion
				};
				Feature remarkableAthelete = new Feature
				{
					FeatureName = "Remarkable Athlete",
					FeatureDescription = "You can add half your proficiency bonus(round up) to any Strength, Dexterity, or Constitution check you make that doesn’t already use your proficiency bonus. In addition, when you make a running long jump, the distance you can cover increases by a number of feet equal to your Strength modifier.",
					RequiredLevel = 7,
					FeatureForClass = fighter,
					FeatureForArchetype = champion
				};
				Feature additionalFightingStyle = new Feature
				{
					FeatureName = "Additional Fighting Style",
					FeatureDescription = "YOu can choose a second option from the Fighting Style class feature.",
					RequiredLevel = 10,
					FeatureForClass = fighter,
					FeatureForArchetype = champion
				};
				Feature superiorCritical = new Feature
				{
					FeatureName = "Superior Critical",
					FeatureDescription = "Your weapon attacks score a critical hit on a roll of 18-20",
					RequiredLevel = 15,
					FeatureForClass = fighter,
					FeatureForArchetype = champion
				};
				Feature survivor = new Feature
				{
					FeatureName = "Survivor",
					FeatureDescription = "you attain the pinnacle o f resilience in battle.At the start of each of your turns, you regain hit points equal to 5+ your Constitution modifier if you have no more than half of your hit points left. You don’t gain this benefit if you have 0 hit points.",
					RequiredLevel = 18,
					FeatureForClass = fighter,
					FeatureForArchetype = champion
				};
				Feature combatSuperiority = new Feature
				{
					FeatureName = "Combat Superiority",
					FeatureDescription = "When you choose this archetype at 3rd level, you learn maneuvers that are fueled by special dice called superiority dice.\r\nManeuvers\r\nYou learn three maneuvers of your choice, which are detailed under “Maneuvers” below. Many maneuvers enhance an attack in some way. You can use only one maneuver per attack. You learn two additional maneuvers of your choice at 7th, 10th, and 15th level. Each time you learn new maneuvers, you can also replace one maneuver you know with a different one.\r\nSuperiority Dice\r\nYou have four superiority dice, which are d8s.A superiority die is expended when you use it.You regain all o f your expended superiority dice when you finish a short or long rest. You gain another superiority die at 7th level and one more at 15th level.\r\nSaving Throws\r\nSome of your maneuvers require your target to make a saving throw to resist the maneuver’s effects. The saving throw DC is calculated as follows:\r\nManeuver save DC = 8 + your proficiency bonus + your Strength or Dexterity modifier (your choice)",
					RequiredLevel = 3,
					FeatureForClass = fighter,
					FeatureForArchetype = battleMaster
				};
				Feature studentOfWar = new Feature
				{
					FeatureName = "Student of War",
					FeatureDescription = "You gain proficiency with one type of artisan's tool of your choice.",
					RequiredLevel = 3,
					FeatureForClass = fighter,
					FeatureForArchetype = battleMaster
				};
				Feature knowYourEnemy = new Feature
				{
					FeatureName = "Know Your Enemy",
					FeatureDescription = "If you spend at least 1 minute observing or interacting with another creature outside combat, you can learn certain information about its capabilities compared to your own.The DM tells you if the creature is your equal, superior, or inferior in regard to two of the following characteristics of your choice:\r\nStrength score\r\nDexterity score\r\nConstitution score\r\nArmor Class\r\nCurrent hit points\r\nTotal class levels (if any)\r\nFighter class levels (if any)",
					RequiredLevel = 7,
					FeatureForClass = fighter,
					FeatureForArchetype = battleMaster
				};
				Feature improvedCombatSuperiority = new Feature
				{
					FeatureName = "Improved Combat Superiority",
					FeatureDescription = "At 10th level, your superiority dice turn into d10s. At 18th level, they turn into dl2s.",
					RequiredLevel = 10,
					FeatureForClass = fighter,
					FeatureForArchetype = battleMaster
				};
				Feature relentless = new Feature
				{
					FeatureName = "Relentless",
					FeatureDescription = "When you roll initiative and have no superiority dice remaining, you regain 1 superiority die.",
					RequiredLevel = 15,
					FeatureForClass = fighter,
					FeatureForArchetype = battleMaster
				};
				Feature weaponBond = new Feature
				{
					FeatureName = "Weapon Bond",
					FeatureDescription = "You learn a ritual that creates a magical bond between yourself and one weapon. You perform the ritual over the course of 1 hour, which can be done during a short rest. The weapon must be within your reach throughout the ritual, at the conclusion of which you touch the weapon and forge the bond. Once you have bonded a weapon to yourself, you can’t be disarmed of that w eapon unless you are incapacitated. If it is on the same plane of existence, you can summon that weapon as a bonus action on your turn, causing it to teleport instantly to your hand. You can have up to two bonded weapons, but can summon only one at a time with your bonus action. If you attempt to bond with a third weapon, you must break the bond with one of the other two.",
					RequiredLevel = 1,
					FeatureForClass = fighter,
					FeatureForArchetype = eldritchKnight
				};
				Feature warMagic = new Feature
				{
					FeatureName = "War Magic",
					FeatureDescription = "When you use your action to cast a cantrip, you can make one weapon attack as a bonus action.",
					RequiredLevel = 1,
					FeatureForClass = fighter,
					FeatureForArchetype = eldritchKnight
				};
				Feature eldritchStrike = new Feature
				{
					FeatureName = "Eldritch Strike",
					FeatureDescription = " You learn how to make your weapon strikes undercut a creature’s resistance to your spells. When you hit a creature with a weapon attack, that creature has disadvantage on the next saving throw it makes against a spell you cast before the end of your next turn.",
					RequiredLevel = 10,
					FeatureForClass = fighter,
					FeatureForArchetype = eldritchKnight
				};
				Feature arcaneCharge = new Feature
				{
					FeatureName = "Arcane Charge",
					FeatureDescription = "You gain the ability to teleport up to 30 feet to an unoccupied space you can see when you use your Action Surge. You can teleport before or after the additional action.",
					RequiredLevel = 15,
					FeatureForClass = fighter,
					FeatureForArchetype = eldritchKnight
				};
				Feature improvedWarMagic = new Feature
				{
					FeatureName = "Improved War Magic",
					FeatureDescription = "When you use your action to cast a spell, you can make one w eapon attack as a bonus action.",
					RequiredLevel = 18,
					FeatureForClass = fighter,
					FeatureForArchetype = eldritchKnight
				};
				db.Features.Add(fightingStyleFighter);
				db.Features.Add(secondWind);
				db.Features.Add(actionSurge);
				db.Features.Add(extraAttackFighter);
				db.Features.Add(indomitable);
				db.Features.Add(improvedCritical);
				db.Features.Add(remarkableAthelete);
				db.Features.Add(additionalFightingStyle);
				db.Features.Add(superiorCritical);
				db.Features.Add(survivor);
				db.Features.Add(combatSuperiority);
				db.Features.Add(studentOfWar);
				db.Features.Add(knowYourEnemy);
				db.Features.Add(relentless);
				db.Features.Add(weaponBond);
				db.Features.Add(warMagic);
				db.Features.Add(eldritchStrike);
				db.Features.Add(arcaneCharge);
				db.Features.Add(improvedWarMagic);
				#endregion Fighter Features
				#region Monk Features
				Feature unarmoredDefenseMonk = new Feature
				{
					FeatureName = "Unarmored Defense",
					FeatureDescription = "While you are wearing no armor and not wielding a shield, your AC equals 10 + your Dexterity modifier + your Wisdom modifier.",
					RequiredLevel = 1,
					FeatureForClass = monk,
				};
				Feature martialArts = new Feature
				{
					FeatureName = "Martial Arts",
					FeatureDescription = "At 1st level, your practice of martial arts gives you mastery of combat styles that use unarmed strikes and monk weapons, which are shortswords and any simple melee weapons that don’t have the two-handed or heavy property. You gain the following benefits while you are unarmed or wielding only monk weapons and you aren’t wearing armor or wielding a shield:\r\nYou can use Dexterity instead of Strength for the attack and damage rolls of your unarmed strikes and monk weapons.\r\nYou can roll a d4 in place of the normal damage of your unarmed strike or monk weapon. This die changes as you gain monk levels, as shown in the Martial Arts column of the Monk table.\r\nWhen you use the Attack action with an unarmed strike or a monk weapon on your turn, you can make one unarmed strike as a bonus action. For example, if you take the Attack action and attack with a quarterstaff, you can also make an unarmed strike as a bonus action, assuming you haven't already taken a bonus action this turn.",
					RequiredLevel = 1,
					FeatureForClass = monk,
				};
				Feature ki = new Feature
				{
					FeatureName = "Ki",
					FeatureDescription = "Your training allows you to harness the mystic energy of ki. Your access to this energy is represented by a number of ki points. Your monk level determines the number of points you have, as shown in the Ki Points column of the Monk table. You can spend these points to fuel various ki features. You start knowing three such features: Flurry of Blows, Patient Defense, and Step of the Wind. You learn more ki features as you gain levels in this class. When you spend a ki point, it is unavailable until you finish a short or long rest, at the end of which you draw all of your expended ki back into yourself. You must spend at least 30 minutes of the rest meditating to regain your ki points. Some of your ki features require your target to make a saving throw to resist the feature’s effects. The saving throw DC is calculated as follows:\r\nKi save DC = 8 + your proficiency bonus + your Wisdom modifier",
                    RequiredLevel = 2,
					FeatureForClass = monk,
				};
				Feature flurryOfBlows = new Feature
				{
					FeatureName = "Flurry of Blows",
					FeatureDescription = "Immediately after you take the Attack action on your turn, you can spend 1 ki point to make two unarmed strikes as a bonus action.",
					RequiredLevel = 2,
					FeatureForClass = monk,
				};
				Feature patientDefense = new Feature
				{
					FeatureName = "Patient Defense",
					FeatureDescription = "You can spend 1 ki point to take the Dodge action as a bonus action on your turn.",
					RequiredLevel = 2,
					FeatureForClass = monk,
				};
				Feature stepOfTheWind = new Feature
				{
					FeatureName = "Step of the Wind",
					FeatureDescription = "You can spend 1 ki point to take tht Disengage or Dash action as a bonus action on your turn, and your jump distance is doubled for the turn.",
					RequiredLevel = 2,
					FeatureForClass = monk,
				};
				Feature unarmoredMovement = new Feature
				{
					FeatureName = "Unarmored Movement",
					FeatureDescription = "Your speed increases by 10 feet while you are not wearing armor or wielding a shield. This bonus increases when you reach certain monk levels, as shown in the Monk table. At 9th level, you gain the ability to move along vertical surfaces and across liquids on your turn without falling during the move.",
					RequiredLevel = 2,
					FeatureForClass = monk,
				};
				Feature deflectMissiles = new Feature
				{ 
					FeatureName = "Deflect Missiles",
					FeatureDescription = "You can use your reaction to deflect or catch the missile when you are hit by a ranged weapon attack.When you do so, the damage you take from the attack is reduced by 1d10 + your Dexterity modifier + your monk level. If you reduce the damage to 0, you can catch the missile if it is small enough for you to hold in one hand and you have at least one hand free. If you catch a missile in this way, you can spend 1 ki point to make a ranged attack with the weapon or piece of ammunition you just caught, as part of the same reaction. You make this attack with proficiency, regardless of your weapon proficiencies, and the missile counts as a monk weapon for the attack.",
					RequiredLevel = 3,
					FeatureForClass = monk,
				};
				Feature slowFall = new Feature
				{
					FeatureName = "Slow Fall",
					FeatureDescription = "You can use your reaction when you fall to reduce any falling damage you take by an amount equal to five times your monk level.",
					RequiredLevel = 4,
					FeatureForClass = monk
				};
				Feature extraAttackMonk = new Feature
				{
					FeatureName = "Extra Attack",
					FeatureDescription = "You can attack twice, instead of once, whenever you take the Attack action on your turn.",
					RequiredLevel = 5,
					FeatureForClass = monk,
				};
				Feature stunningStrike = new Feature
				{
					FeatureName = "Stunning Strike",
					FeatureDescription = "You can interfere with the flow of ki in an opponent’s body.When you hit another creature with a melee w eapon attack, you can spend 1 ki point to attempt a stunning strike.The target must succeed on a Constitution saving throw or be stunned until the end of your next turn.",
					RequiredLevel = 5,
					FeatureForClass = monk,
				};
				Feature kiEmpoweredStrikes = new Feature
				{
					FeatureName = "Ki-Empowered Strikes",
					FeatureDescription = "Your unarmed strikes count as magical for the purpose of overcoming resistance and immunity to nonmagical attacks and damage.",
					RequiredLevel = 6,
					FeatureForClass = monk,
				};
				Feature evasionMonk = new Feature
				{
					FeatureName = "Evasion",
					FeatureDescription = "Your instinctive agility lets you dodge out of the way of certain area effects, such as a blue dragon’s lightning breath or a fireball spell. When you are subjected to an effect that allows you to make a Dexterity saving throw to take only half damage, you instead take no damage if you succeed on the saving throw, and only half damage if you fail.",
					RequiredLevel = 7,
					FeatureForClass = monk,
				};
				Feature stillnessOfMind = new Feature
				{
					FeatureName = "Stillness of Mind",
					FeatureDescription = "You can use your action to end one effect on yourself that is causing you to be charmed or frightened.",
					RequiredLevel = 7,
					FeatureForClass = monk,
				};
				Feature purityOfBody = new Feature
				{
					FeatureName = "Purity of Body",
					FeatureDescription = "Your mastery of the ki flowing through you makes you immune to disease and poison.",
					RequiredLevel = 10,
					FeatureForClass = monk,
				};
				Feature tongueSunMoon = new Feature
				{
					FeatureName = "Tongue of the Sun and Moon",
					FeatureDescription = "You learn to touch the ki of other minds so that you understand all spoken languages. Moreover, any creature that can understand a language can understand what you say.",
					RequiredLevel = 13,
					FeatureForClass = monk,
				};
				Feature diamondSoul = new Feature
				{
					FeatureName = "Diamond Soul",
					FeatureDescription = "Your mastery of ki grants you proficiency in all saving throws. Additionally, whenever you make a saving throw and fail, you can spend 1 ki point to reroll it and take the second result.",
					RequiredLevel = 14,
					FeatureForClass = monk,
				};
				Feature timelessBodyMonk = new Feature
				{
					FeatureName = "Timeless Body",
					FeatureDescription = "Your ki sustains you so that you suffer none of the frailty of old age, and you can't be aged magically. You can still die of old age, however. In addition, you no longer need food or water.",
					RequiredLevel = 15,
					FeatureForClass = monk,
				};
				Feature emptyBody = new Feature
				{
					FeatureName = "Empty Body",
					FeatureDescription = "You can use your action to spend 4 ki points to become invisible for 1 minute. During that time, you also have resistance to all damage but force damage. Additionally, you can spend 8 ki points to cast the astral projection spell, without needing material components.When you do so, you can’t take any other creatures with you.",
					RequiredLevel = 18,
					FeatureForClass = monk,
				};
				Feature perfectSelf = new Feature
				{
					FeatureName = "Perfect Self",
					FeatureDescription = "When you roll for initiative and have no ki points remaining, you regain 4 ki points.",
					RequiredLevel = 20,
					FeatureForClass = monk,
				};
				Feature openHandTechnique = new Feature
				{
					FeatureName = "Open Hand Technique",
					FeatureDescription = "You can manipulate your enemy’s ki when you harness your own.Whenever you hit a creature with one of the attacks granted by your Flurry of Blows, you can impose one of the following effects on that target:\r\nIt must succeed on a Dexterity saving throw or be knocked prone.\r\nIt must make a Strength saving throw. If it fails, you can push it up to 15 feet away from you.\r\nIt can’t take reactions until the end of your next turn.",
					RequiredLevel = 3,
					FeatureForClass = monk,
					FeatureForArchetype = wayOpenHand
				};
				Feature wholenessOfBody = new Feature
				{
					FeatureName = "Wholeness of Body",
					FeatureDescription = "You gain the ability to heal yourself. As an action, you can regain hit points equal to three times your monk level. You must finish a long rest before you can use this feature again.",
					RequiredLevel = 6,
					FeatureForClass = monk,
					FeatureForArchetype = wayOpenHand
				};
				Feature tranquility = new Feature
				{
					FeatureName = "Tranquility",
					FeatureDescription = "You can enter a special meditation that surrounds you with an aura of peace. At the end of a long rest, you gain the effect of a sanctuary spell that lasts until the start of your next long rest (the spell can end early as normal).The saving throw DC for the spell equals 8+ your Wisdom modifier + your proficiency bonus.",
					RequiredLevel = 11,
					FeatureForClass = monk,
					FeatureForArchetype = wayOpenHand
				};
				Feature quiveringPalm = new Feature
				{
					FeatureName = "Quivering Palm",
					FeatureDescription = "You gain the ability to set up lethal vibrations in someone’s body. When you hit a creature with an unarmed strike, you can spend 3 ki points to start these imperceptible vibrations, which last for a number of days equal to your monk level. The vibrations are harmless unless you use your action to end them. To do so, you and the target must be on the same plane of existence. When you use this action, the creature must make a Constitution saving throw. If it fails, it is reduced to 0 hit points. If it succeeds, it takes  10d10 necrotic damage. You can have only one creature under the effect of this feature at a time. You can choose to end the vibrations harmlessly without using an action.",
					RequiredLevel = 17,
					FeatureForClass = monk,
					FeatureForArchetype = wayOpenHand
				};
				Feature shadowArts = new Feature
				{
					FeatureName = "Shadow Arts",
					FeatureDescription = "You can use your ki to duplicate the effects o f certain spells. As an action, you can spend 2 ki points to cast darkness, darkvision, pass without trace, or silence, without providing material components. Additionally, you gain the minor illusion cantrip if you don’t already know it.",
					RequiredLevel = 3,
					FeatureForClass = monk,
					FeatureForArchetype = wayShadow
				};
				Feature shadowStep = new Feature
				{
					FeatureName = "Shadow Step",
					FeatureDescription = "You gain the ability to step from one shadow into another. When you are in dim light or darkness, as a bonus action you can teleport up to 60 feet to an unoccupied space you can see that is also in dim light or darkness.You then have advantage on the first melee attack you make before the end o f the turn.",
					RequiredLevel = 6,
					FeatureForClass = monk,
					FeatureForArchetype = wayShadow
				};
				Feature cloakOfShadowsMonk = new Feature
				{
					FeatureName = "Cloak of Shadows",
					FeatureDescription = "You have learned to become one with the shadows.When you are in an area o f dim light or darkness, you can use your action to become invisible. You remain invisible until you make an attack, cast a spell, or are in an area o f bright light.",
					RequiredLevel = 11,
					FeatureForClass = monk,
					FeatureForArchetype = wayShadow
				};
				Feature opportunist = new Feature
				{
					FeatureName = "Opportunist",
					FeatureDescription = "You can exploit a creature's momentary distraction when it is hit by an attack. Whenever a creature within 5 feet of you is hit by an attack made by a creature other than you, you can use your reaction to make a melee attack against that creature.",
					RequiredLevel = 17,
					FeatureForClass = monk,
					FeatureForArchetype = wayShadow
				};
				//TODO: Will need to get it's own subtable. Leaving it for now
				Feature discipleOfTheElements = new Feature
				{
					FeatureName = "Disciple of the Elements",
					FeatureDescription = "When you choose this tradition at 3rd level, you learn magical disciplines that harness the power of the four elements. A discipline requires you to spend ki points each time you use it. You know the Elemental Attunement discipline and one other elemental discipline of your choice, which are detailed in the “Elemental Disciplines” section below. You learn one additional elemental discipline of your choice at 6th, 11th, and 17th level. Whenever you learn a new elemental discipline, you can also replace one elemental discipline that you already know with a different discipline.\r\nCasting Elemental Spells\r\nSome elemental disciplines allow you to cast spells. See chapter 10 for the general rules o f spellcasting. To cast one of these spells, you use its casting time and other rules, but you don’t need to provide material components for it. Once you reach 5th level in this class, you can spend additional ki points to increase the level of an elemental discipline spell that you cast, provided that the spell has an enhanced effect at a higher level, as burning hands does. The spell's level increases by 1 for each additional ki point you spend.For example, if you are a 5th-level monk and use Sweeping Cinder Strike to cast burning hands, you can spend 3 ki points to cast it as a 2nd-level spell (the discipline’s base cost o f 2 ki points plus 1). The maximum number o f ki points you can spend to cast a spell in this way (including its base ki point cost and any additional ki points you spend to increase its level) is determined by your monk level, as shown in the Spells and Ki Points table.\r\nSpells and Ki Points\r\nMonk Levels Maximum Ki Points for a Spell\r\n5th—8th 3\r\n9th—12th 4\r\n13th—16th 5\r\n17th—20th 6\r\nElemental Disciplines\r\nThe elemental disciplines are presented in alphabetical order. If a discipline requires a level, you must be that level in this class to learn the discipline.\r\nBreath of Winter(17th Level Required)\r\nYou can spend 6 ki points to cast cone of cold.\r\nClench of the North Wind (6th Level Required)\r\nYou can spend 3 ki points to cast hold person. \r\nElemental Attunement\r\nYou can use your action to briefly control elemental forces nearby, causing one of the following effects of your choice:\r\n • Create a harmless, instantaneous sensory effect related to air, earth, fire, or water, such as a shower of sparks, a puff of wind, a spray of light mist, or a gentle rumbling of stone.\r\n • Instantaneously light or snuff out a candle, a torch, or a small campfire.\r\n • Chill or warm up to 1 pound o f nonliving material for up to 1 hour.\r\n • Cause earth, fire, water, or mist that can fit within a 1-foot cube to shape itself into a crude form you designate for 1 minute.\r\nEternal Mountain Defense (11th Level Required)\r\nYou can spend 5 ki points to cast stoneskin, targeting yourself.\r\nFangs of the Fire Snake\r\nWhen you use the Attack action on your turn, you can spend 1 ki point to cause tendrils of flame to stretch out from your fists and feet. Your reach with your unarmed strikes increases by 10 feet for that action, as well as the rest of the turn. A hit with such an attack deals fire damage instead of bludgeoning damage, and if you spend 1 ki point when the attack hits, it also deals an extra 1d 10 fire damage.\r\nFist of Four Thunders\r\nYou can spend 2 ki points to cast thunderwave.\r\nFist of Unbroken Air\r\nYou can create a blast of compressed air that strikes like a mighty fist. As an action, you can spend 2 ki points and choose a creature within 30 feet o f you. That creature must make a Strength saving throw. On a failed save, the creature takes 3d10 bludgeoning damage, plus an extra 1d10 bludgeoning damage for each additional ki point you spend, and you can push the creature up to 20 feet away from you and knock it prone. On a successful save, the creature takes half as much damage, and you don’t push it or knock it prone.\r\nFlames of the Phoenix (11th Level Required)\r\nYou can spend 4 ki points to cast fireball.\r\nGong of the Summit(6th Level Required)\r\nYou can spend 3 ki points to cast shatter.\r\nMist Stance(11th Level Required)\r\nYou can spend 4 ki points to cast gaseous form, targeting yourself.\r\nRide the Wind(11th Level Required)\r\nYou can spend 4 ki points to cast fly, targeting yourself.\r\nRiver of Hungry Flame (17th Level Required)\r\nYou can spend 5 ki points to cast wall of fire.\r\nRush of the Gale Spirits\r\nYou can spend 2 ki points to cast gust of wind.\r\nShape the Flowing River\r\nAs an action, you can spend 1 ki point to choose an area of ice or water no larger than 30 feet on a side within 120 feet of you. You can change water to ice within the area and vice versa, and you can reshape ice in the area in any manner you choose.You can raise or lower the ice’s elevation, create or fill in a trench, erect or flatten a wall, or form a pillar. The extent of any such changes can’t exceed half the area’s largest dimension. For example, if you affect a 30-foot square, you can create a pillar up to 15 feet high, raise or lower the square’s elevation by up to 15 feet, dig a trench up to 15 feet deep, and so on. You can’t shape the ice to trap or injure a creature in the area.\r\nSweeping Cinder Strike\r\nYou can spend 2 ki points to cast burning hands.\r\nWater Whip\r\nYou can spend 2 ki points as a bonus action to create a whip of water that shoves and pulls a creature to unbalance it. A creature that you can see that is within 30 feet of you must make a Dexterity saving throw. On a failed save, the creature takes 3d10 bludgeoning damage, plus an extra 1d10 bludgeoning damage for each additional ki point you spend, and you can either knock it prone or pull it up to 25 feet closer to you. On a successful save, the creature takes half as much damage, and you don’t pull it or knock it prone.\r\nWave of Rolling Earth(17th Level Required)\r\nYou can spend 6 ki points to cast wall of stone.",
					RequiredLevel = 3,
					FeatureForClass = monk,
					FeatureForArchetype = wayElements
				};
				db.Features.Add(unarmoredDefenseMonk);
				db.Features.Add(martialArts);
				db.Features.Add(ki);
				db.Features.Add(unarmoredMovement);
				db.Features.Add(deflectMissiles);
				db.Features.Add(slowFall);
				db.Features.Add(extraAttackMonk);
				db.Features.Add(stunningStrike);
				db.Features.Add(kiEmpoweredStrikes);
				db.Features.Add(evasionMonk);
				db.Features.Add(stillnessOfMind);
				db.Features.Add(purityOfBody);
				db.Features.Add(tongueSunMoon);
				db.Features.Add(diamondSoul);
				db.Features.Add(timelessBodyMonk);
				db.Features.Add(emptyBody);
				db.Features.Add(perfectSelf);
				db.Features.Add(openHandTechnique);
				db.Features.Add(wholenessOfBody);
				db.Features.Add(tranquility);
				db.Features.Add(quiveringPalm);
				db.Features.Add(shadowArts);
				db.Features.Add(shadowStep);
				db.Features.Add(cloakOfShadowsMonk);
				db.Features.Add(discipleOfTheElements);
				#endregion Monk Features
				#region Paladin Features
				Feature divineSense = new Feature
				{
					FeatureName = "Divine Sense",
					FeatureDescription = "The presence o f strong evil registers on your senses like a noxious odor, and powerful good rings like heavenly music in your ears. As an action, you can open your awareness to detect such forces. Until the end of your next turn, you know the location of any celestial, fiend, or undead within 60 feet o f you that is not behind total cover. You know the type(celestial, fiend, or undead) of any being whose presence you sense, but not its identity (the vampire Count Strahd von Zarovich, for instance). Within the same radius, you also detect the presence of any place or object that has been consecrated or desecrated, as with the hallow spell. You can use this feature a number of times equal to 1+ your Charisma modifier. When you finish a long rest, you regain all expended uses.",
					RequiredLevel = 1,
					FeatureForClass = paladin,
				};
				Feature layOnHands = new Feature
				{
					FeatureName = "Lay on Hands",
					FeatureDescription = "Your blessed touch can heal wounds. You have a pool of healing power that replenishes when you take a long rest. With that pool, you can restore a total number of hit points equal to your paladin level x5. As an action, you can touch a creature and draw power from the pool to restore a number of hit points to that creature, up to the maximum amount remaining in your pool. Alternatively, you can expend 5 hit points from your pool of healing to cure the target of one disease or neutralize one poison affecting it. You can cure multiple diseases and neutralize multiple poisons with a single use of Lay on Hands, expending hit points separately for each one. This feature has no effect on undead and constructs.",
					RequiredLevel = 1,
					FeatureForClass = paladin,
				};
				Feature fightingStylePaladin = new Feature
				{
					FeatureName = "Fighting Style",
					FeatureDescription = "You adopt a style of fighting as your specialty. Choose one of the following options. You can’t take a Fighting Style option more than once, even if you later get to choose again.\r\nDefense\r\nWhile you are w earing armor, you gain a + 1 bonus to AC.\r\nDueling\r\nWhen you are wielding a melee weapon in one hand and no other weapons, you gain a +2 bonus to damage rolls with that weapon.\r\nGreat Weapon Fighting\r\nWhen you roll a 1 or 2 on a damage die for an attack you make with a melee weapon that you are wielding with two hands, you can reroll the die and must use the new roll. The weapon must have the two-handed or versatile property for you to gain this benefit.\r\nProtection\r\nWhen a creature you can see attacks a target other than you that is within 5 feet of you, you can use your reaction to impose disadvantage on the attack roll. You must be wielding a shield.",
					RequiredLevel = 1,
					FeatureForClass = paladin,
				};
				Feature divineSmite = new Feature
				{
					FeatureName = "Divine Smite",
					FeatureDescription = "When you hit a creature with a melee weapon attack, you can expend one paladin spell slot to deal radiant damage to the target, in addition to the weapon’s damage.The extra damage is 2d8 for a 1st-level spell slot, plus 1d8 for each spell level higher than 1st, to a maximum o f 5d8.The damage increases by 1d8 if the target is an undead or a fiend.",
					RequiredLevel = 2,
					FeatureForClass = paladin,
				};
				Feature divineHealth = new Feature
				{
					FeatureName = "Divine Health",
					FeatureDescription = "The divine magic flowing through you makes you immune to disease.",
					RequiredLevel = 3,
					FeatureForClass = paladin,
				};
				Feature channelDivinityPaladin = new Feature
				{
					FeatureName = "Channel Divinity",
					FeatureDescription = "Your oath allows you to channel divine energy to fuel magical effects. Each Channel Divinity option provided by your oath explains how to use it. When you use your Channel Divinity, you choose which option to use. You must then finish a short or long rest to use your Channel Divinity again. Some Channel Divinity effects require saving throws. When you use such an effect from this class, the DC equals your paladin spell save DC.",
					RequiredLevel = 3,
					FeatureForClass = paladin,
				};
				Feature extraAttackPaladin = new Feature
				{
					FeatureName = "Extra Attack",
					FeatureDescription = "You can attack twice, instead of once, whenever you take the Attack action on your turn.",
					RequiredLevel = 5,
					FeatureForClass = paladin,
				};
				Feature auraProtection = new Feature
				{
					FeatureName = "Auro of Protection",
					FeatureDescription = "Whenever you or a friendly creature within 10 feet o f you must make a saving throw, the creature gains a bonus to the saving throw equal to your Charisma modifier (with a minimum bonus o f + 1). You must be conscious to grant this bonus. At 18th level, the range of this aura increases to 30 feet.",
					RequiredLevel = 6,
					FeatureForClass = paladin,
				};
				Feature auraCourage = new Feature
				{
					FeatureName = "Aura of Courage",
					FeatureDescription = "You and friendly creatures within 10 feet of you can’t be frightened while you are conscious. At 18th level, the range of this aura increases to 30 feet.",
					RequiredLevel = 10,
					FeatureForClass = paladin,
				};
				Feature improvedDivineSmite = new Feature
				{
					FeatureName = "Improved Divine Smite",
					FeatureDescription = "You are so suffused with righteous might that all your melee weapon strikes carry divine power with them. Whenever you hit a creature with a melee weapon, the creature takes an extra 1d8 radiant damage. If you also use your Divine Smite with an attack, you add this damage to the extra damage of your Divine Smite.",
					RequiredLevel = 11,
					FeatureForClass = paladin,
				};
				Feature cleansingTouch = new Feature
				{
					FeatureName = "Cleansing Touch",
					FeatureDescription = "You can use your action to end one spell on yourself or on one willing creature that you touch. You can use this feature a number of times equal to your Charisma modifier (a minimum of once).You regain expended uses when you finish a long rest.",
					RequiredLevel = 14,
					FeatureForClass = paladin,
				};
				Feature tenetsDevotion = new Feature
				{
					FeatureName = "Tenets of Devotion",
					FeatureDescription = "Though the exact words and strictures of the Oath of Devotion vary, paladins of this oath share these tenets.\r\nHonesty\r\nDon’t lie or cheat. Let your word be your promise.\r\nCourage\r\nNever fear to act, though caution is wise.\r\nCompassion\r\nAid others, protect the weak, and punish those who threaten them. Show mercy to your foes, but temper it with wisdom.\r\nHonor\r\nTreat others with fairness, and let your honorable deeds be an example to them. Do as much good as possible while causing the least amount of harm.\r\nDuty\r\nBe responsible for your actions and their consequences, protect those entrusted to your care, and obey those who have just authority over you.",
					RequiredLevel = 3,
					FeatureForClass = paladin,
					FeatureForArchetype = oathDevotion
				};
				Feature oathSpellsDevotion = new Feature
				{
					FeatureName = "Oath Spells",
					FeatureDescription = "Your oath has a list of associated spells. You gain access to these spells at the levels specified in the oath description. Once you gain access to an oath spell, you always have it prepared. Oath spells don’t count against the number of spells you can prepare each day. If you gain an oath spell that doesn’t appear on the paladin spell list, the spell is nonetheless a paladin spell for you.\r\nPaladin Level Spells\r\n3rd protection from evil and good, sanctuary\r\n5th lesser restoration, zone of truth\r\n9th beacon of hope, dispel magic\r\n13th freedom of movement, guardian of faith\r\n17th commune, flame strike",
					RequiredLevel = 3,
					FeatureForClass = paladin,
					FeatureForArchetype = oathDevotion
				};
				Feature sacredWeapon = new Feature
				{
					FeatureName = "Channel Divinity: Sacred Weapon",
					FeatureDescription = "As an action, you can imbue one weapon that you are holding with positive energy, using your Channel Divinity. For 1 minute, you add your Charisma modifier to attack rolls made with that weapon (with a minimum bonus of + 1). The weapon also emits bright light in a 20-foot radius and dim light 20 feet beyond that. If the weapon is not already magical, it becomes magical for the duration. You can end this effect on your turn as part of any other action. If you are no longer holding or carrying this weapon, or if you fall unconscious, this effect ends.",
					RequiredLevel = 3,
					FeatureForClass = paladin,
					FeatureForArchetype = oathDevotion
				};
				Feature turnUnholy = new Feature
				{
					FeatureName = "Channel Divinity: Turn the Unholy",
					FeatureDescription = "As an action, you present your holy symbol and speak a prayer censuring fiends and undead, using your Channel Divinity. Each fiend or undead that can see or hear you within 30 feet of you must make a Wisdom saving throw. If the creature fails its saving throw, it is turned for 1 minute or until it takes damage. A turned creature must spend its turns trying to move as far away from you as it can, and it can’t willingly move to a space within 30 feet of you. It also can’t take reactions. For its action, it can use only the Dash action or try to escape from an effect that prevents it from moving. If there’s nowhere to move, the creature can use the Dodge action.",
					RequiredLevel = 3,
					FeatureForClass = paladin,
					FeatureForArchetype = oathDevotion
				};
				Feature auraDevotion = new Feature
				{
					FeatureName = "Aura of Devotion",
					FeatureDescription = "You and friendly creatures within 10 feet o f you can’t be charmed while you are conscious. At 18th level, the range of this aura increases to 30 feet.",
					RequiredLevel = 7,
					FeatureForClass = paladin,
					FeatureForArchetype = oathDevotion
				};
				Feature puritySpirit = new Feature
				{
					FeatureName = "Purity of Spirit",
					FeatureDescription = "You are always under the effects of a protection from evil and good spell.",
					RequiredLevel = 15,
					FeatureForClass = paladin,
					FeatureForArchetype = oathDevotion
				};
				Feature holyNimbus = new Feature
				{
					FeatureName = "Holy Nimbus",
					FeatureDescription = "You can use an action to emanate an aura of sunlight. For 1 minute, bright light shines from you in a 30-foot radius, and dim light shines 30 feet beyond that. Whenever an enemy creature starts its turn in the bright light, the creature takes 10 radiant damage. In addition, for the duration, you have advantage on saving throws against spells cast by fiends or undead. Once you use this feature, you can’t use it again until you finish a long rest.",
					RequiredLevel = 1,
					FeatureForClass = paladin,
					FeatureForArchetype = oathDevotion
				};
				Feature tenetsAncients = new Feature
				{
					FeatureName = "Tenets of the Ancients",
					FeatureDescription = "The tenets of the Oath of the Ancients have been preserved for uncounted centuries. This oath emphasizes the principles of good above any concerns of law or chaos.Its four central principles are simple.\r\nKindle the Light\r\nThrough your acts of mercy, kindness, and forgiveness, kindle the light of hope in the world, beating back despair. \r\nShelter the Light\r\nWhere there is good, beauty, love, and laughter in the world, stand against the wickedness that would swallow it. Where life flourishes, stand against the forces that would render it barren.\r\nPreserve Your Own Light\r\nDelight in song and laughter, in beauty and art. If you allow the light to die in your own heart, you can’t preserve it in the world.\r\nBe the Light\r\nBe a glorious beacon for all who live in despair. Let the light of your joy and courage shine forth in all your deeds.",
					RequiredLevel = 1,
					FeatureForClass = paladin,
					FeatureForArchetype = oathAncients
				};
				Feature oathSpellsAncients = new Feature
				{
					FeatureName = "Oath Spells",
					FeatureDescription = "Your oath has a list of associated spells. You gain access to these spells at the levels specified in the oath description. Once you gain access to an oath spell, you always have it prepared. Oath spells don’t count against the number of spells you can prepare each day. If you gain an oath spell that doesn’t appear on the paladin spell list, the spell is nonetheless a paladin spell for you.\r\nPaladin Level Spells\r\n3rd ensnaring strike, speak with animals\r\n5th moonbeam, misty step\r\n9th plant growth, protection from energy\r\n13th ice storm, stoneskin\r\n17th commune with nature, tree stride",
					RequiredLevel = 3,
					FeatureForClass = paladin,
					FeatureForArchetype = oathAncients
				};
				Feature naturesWrath = new Feature
				{
					FeatureName = "Channel Divinity: Nature's Wrath",
					FeatureDescription = "You can use your Channel Divinity to invoke primeval forces to ensnare a foe.As an action, you can cause spectral vines to spring up and reach for a creature within 10 feet of you that you can see. The creature must succeed on a Strength or Dexterity saving throw (its choice) or be restrained. While restrained by the vines, the creature repeats the saving throw at the end of each of its turns. On a success, it frees itself and the vines vanish.",
					RequiredLevel = 3,
					FeatureForClass = paladin,
					FeatureForArchetype = oathAncients
				};
				Feature turnFaithless = new Feature
				{
					FeatureName = "Channel Divinity: Turn the Faithless",
					FeatureDescription = "You can use your Channel Divinity to utter ancient words that are painful for fey and fiends to hear. As an action, you present your holy symbol, and each fey or fiend within 30 feet of you that can hear you must make a Wisdom saving throw. On a failed save, the creature is turned for 1 minute or until it takes damage. A turned creature must spend its turns trying to move as far away from you as it can, and it can’t willingly move to a space within 30 feet of you. It also can’t take reactions.For its action, it can use only the Dash action or try to escape from an effect that prevents it from moving. If there’s nowhere to move, the creature can use the Dodge action. If the creature’s true form is concealed by an illusion, shapeshifting, or other effect, that form is revealed while it is turned.",
					RequiredLevel = 3,
					FeatureForClass = paladin,
					FeatureForArchetype = oathAncients
				};
				Feature auraWarding = new Feature
				{
					FeatureName = "Aura of Warding",
					FeatureDescription = "Ancient magic lies so heavily upon you that it forms an eldritch ward. You and friendly creatures within 10 feet of you have resistance to damage from spells. At 18th level, the range o f this aura increases to 30 feet.",
					RequiredLevel = 7,
					FeatureForClass = paladin,
					FeatureForArchetype = oathAncients
				};
				Feature undyingSentinel = new Feature
				{
					FeatureName = "Undying Sentinel",
					FeatureDescription = "When you are reduced to 0 hit points and are not killed outright, you can choose to drop to 1 hit point instead.Once you use this ability, you can’t use it again until you finish a long rest. Additionally, you suffer none of the drawbacks of old age, and you can’t be aged magically.",
					RequiredLevel = 15,
					FeatureForClass = paladin,
					FeatureForArchetype = oathAncients
				};
				Feature elderChampion = new Feature
				{
					FeatureName = "Elder Champion",
					FeatureDescription = "At 20th level, you can assume the form of an ancient force of nature, taking on an appearance you choose. For example, your skin might turn green or take on a bark-like texture, your hair might become leafy or mosslike, or you might sprout antlers or a lion-like mane. Using your action, you undergo a transformation. For 1 minute, you gain the following benefits:\r\nAt the start of each of your turns, you regain 10 hit points.\r\nWhenever you cast a paladin spell that has a casting time of 1 action, you can cast it using a bonus action instead.\r\nEnemy creatures within 10 feet o f you have disadvantage on saving throws against your paladin spells and Channel Divinity options.\r\nOnce you use this feature, you can’t use it again until you finish a long rest.",
					RequiredLevel = 20,
					FeatureForClass = paladin,
					FeatureForArchetype = oathAncients
				};
				Feature tenetsVengeance = new Feature
				{
					FeatureName = "",
					FeatureDescription = "The tenets of the Oath of Vengeance vary by paladin, but all the tenets revolve around punishing wrongdoers by any means necessary. Paladins who uphold these tenets are willing to sacrifice even their own righteousness to mete out justice upon those who do evil, so the paladins are often neutral or lawful neutral in alignment. The core principles of the tenets are brutally simple.\r\nFight the Greater Evil\r\nFaced with a choice of fighting my sworn foes or combating a lesser evil. I choose the greater evil\r\nNo Mercy for the Wicked\r\nOrdinary foes might win my mercy, but my sworn enemies do not.\r\nBy Any Means Necessary\r\nMy qualms can’t get in the way of exterminating my foes.\r\nRestitution\r\nIf my foes wreak ruin on the world, it is because I failed to stop them. I must help those harmed by their misdeeds.",
					RequiredLevel = 3,
					FeatureForClass = paladin,
					FeatureForArchetype = oathVengeance
				};
				Feature oathSpellsVengeance = new Feature
				{
					FeatureName = "Oath Spells",
					FeatureDescription = "Your oath has a list of associated spells. You gain access to these spells at the levels specified in the oath description. Once you gain access to an oath spell, you always have it prepared. Oath spells don’t count against the number of spells you can prepare each day. If you gain an oath spell that doesn’t appear on the paladin spell list, the spell is nonetheless a paladin spell for you.\r\nPaladin Level Spells\r\n3rd bane, hunter’s mark\r\n5th hold person, misty step\r\n9th haste, protection from energy\r\n13th banishment, dimension door\r\n17th hold monster, scrying",
					RequiredLevel = 3,
					FeatureForClass = paladin,
					FeatureForArchetype = oathVengeance
				};
				Feature abjureEnemy = new Feature
				{
					FeatureName = "Channel Divinity: Abjure Enemy",
					FeatureDescription = "As an action, you present your holy symbol and speak a prayer of denunciation, using your Channel Divinity. Choose one creature within 60 feet of you that you can see. That creature must make a Wisdom saving throw, unless it is immune to being frightened. Fiends and undead have disadvantage on this saving throw. On a failed save, the creature is frightened for 1 minute or until it takes any damage. While frightened, the creature’s speed is 0, and it can’t benefit from any bonus to its speed. On a successful save, the creature’s speed is halved for 1 minute or until the creature takes any damage.",
					RequiredLevel = 1,
					FeatureForClass = paladin,
					FeatureForArchetype = oathVengeance
				};
				Feature vowEnmity = new Feature
				{
					FeatureName = "Channel Divinity: Vow of Enmity",
					FeatureDescription = "As a bonus action, you can utter a vow of enmity against a creature you can see within 10 feet of you, using your Channel Divinity. You gain advantage on attack rolls against the creature for 1 minute or until it drops to 0 hit points or falls unconscious.",
					RequiredLevel = 3,
					FeatureForClass = paladin,
					FeatureForArchetype = oathVengeance
				};
				Feature relentlessAvenger = new Feature
				{
					FeatureName = "Relentless Avenger",
					FeatureDescription = "Your supernatural focus helps you close off a foe’s retreat. When you hit a creature with an opportunity attack, you can move up to half your speed immediately after the attack and as part of the same reaction. This movement doesn’t provoke opportunity attacks.",
					RequiredLevel = 1,
					FeatureForClass = paladin,
					FeatureForArchetype = oathVengeance
				};
				Feature soulVengeance = new Feature
				{
					FeatureName = "Soul of Vengeance",
					FeatureDescription = "The authority with which you speak your Vow of Enmity gives you greater power over your foe. When a creature under the effect of your Vow of Enmity makes an attack, you can use your reaction to make a melee weapon attack against that creature if it is within range.",
					RequiredLevel = 1,
					FeatureForClass = paladin,
					FeatureForArchetype = oathVengeance
				};
				Feature avengingAngel = new Feature
				{
					FeatureName = "Avenging Angel",
					FeatureDescription = "At 20th level, you can assume the form of an angelic avenger. Using your action, you undergo a transformation. For 1 hour, you gain the following benefits:\r\nWings sprout from your back and grant you a flying speed of 60 feet.\r\nYou emanate an aura of menace in a 30-foot radius. The first time any enemy creature enters the aura or starts its turn there during a battle, the creature must succeed on a W isdom saving throw or become frightened of you for 1 minute or until it takes any damage. Attack rolls against the frightened creature have advantage.\r\nOnce you use this feature, you can’t use it again until you finish a long rest.",
					RequiredLevel = 1,
					FeatureForClass = paladin,
					FeatureForArchetype = oathVengeance
				};
				db.Features.Add(divineSense);
				db.Features.Add(layOnHands);
				db.Features.Add(divineSmite);
				db.Features.Add(divineHealth);
				db.Features.Add(extraAttackPaladin);
				db.Features.Add(auraProtection);
				db.Features.Add(auraCourage);
				db.Features.Add(improvedDivineSmite);
				db.Features.Add(cleansingTouch);
				db.Features.Add(tenetsDevotion);
				db.Features.Add(oathSpellsDevotion);
				db.Features.Add(sacredWeapon);
				db.Features.Add(turnUnholy);
				db.Features.Add(auraDevotion);
				db.Features.Add(puritySpirit);
				db.Features.Add(holyNimbus);
				db.Features.Add(tenetsAncients);
				db.Features.Add(oathSpellsAncients);
				db.Features.Add(auraWarding);
				db.Features.Add(undyingSentinel);
				db.Features.Add(elderChampion);
				db.Features.Add(tenetsVengeance);
				db.Features.Add(oathSpellsVengeance);
				db.Features.Add(abjureEnemy);
				db.Features.Add(vowEnmity);
				db.Features.Add(relentlessAvenger);
				db.Features.Add(soulVengeance);
				db.Features.Add(avengingAngel);
				#endregion Paladin Features
				#region Ranger Features
				Feature favoredEnemy = new Feature
				{
					FeatureName = "Favored Enemy",
					FeatureDescription = "Beginning at 1st level, you have significant experience studying, tracking, hunting, and even talking to a certain type of enemy. Choose a type of favored enemy: aberrations, beasts, celestials, constructs, dragons, elementals, fey, fiends, giants, monstrosities, oozes, plants, or undead. Alternatively, you can select two races of humanoid (such as gnolls and orcs) as favored enemies. You have advantage on Wisdom(Survival) checks to track your favored enemies, as well as on Intelligence checks to recall information about them. When you gain this feature, you also learn one language of your choice that is spoken by your favored enemies, if they speak one at all. You choose one additional favored enemy, as well as an associated language, at 6th and 14th level. As you gain levels, your choices should reflect the types of monsters you have encountered on your adventures.",
					RequiredLevel = 1,
					FeatureForClass = ranger,
				};
				Feature naturalExplorer = new Feature
				{
					FeatureName = "Natural Explorer",
					FeatureDescription = "You are particularly familiar with one type of natural environment and are adept at traveling and surviving in such regions. Choose one type of favored terrain: arctic, coast, desert, forest, grassland, mountain, swamp, or the Underdark. When you make an Intelligence or Wisdom check related to your favored terrain, your proficiency bonus is doubled if you are using a skill that you’re proficient in. While traveling for an hour or m ore in your favored terrain, you gain the following benefits:\r\n • Difficult terrain doesn’t slow your group’s travel.\r\n • Your group can’t become lost except by magical means.\r\n • Even when you are engaged in another activity while traveling(such as foraging, navigating, or tracking), you remain alert to danger.\r\n • If you are traveling alone, you can move stealthily at a normal pace.\r\n • When you forage, you find twice as much food as you normally would.\r\n • While tracking other creatures, you also learn their exact number, their sizes, and how long ago they passed through the area.\r\nYou choose additional favored terrain types at 6th and 10th level.",
					RequiredLevel = 1,
					FeatureForClass = ranger,
				};
				Feature fightingStyleRanger = new Feature
				{
					FeatureName = "Fighting Style",
					FeatureDescription = "You adopt a particular style of fighting as your specialty. Choose one of the following options. You can’t take a Fighting Style option more than once, even if you later get to choose again.\r\nArchery\r\nYou gain a +2 bonus to attack rolls you make with ranged weapons.\r\nDefense\r\nWhile you are wearing armor, you gain a +1 bonus to AC.\r\nDueling\r\nWhen you are wielding a melee weapon in one hand and no other weapons, you gain a +2 bonus to damage rolls with that weapon.\r\nTwo-Weapon Fighting\r\nWhen you engage in two-weapon fighting, you can add your ability modifier to the damage of the second attack.",
					RequiredLevel = 2,
					FeatureForClass = ranger,
				};
				Feature primevalAwareness = new Feature
				{
					FeatureName = "Primeval Awareness",
					FeatureDescription = "You can use your action and expend one ranger spell slot to focus your awareness on the region around you. For 1 minute per level of the spell slot you expend, you can sense whether the following types of creatures are present within 1 mile of you (or within up to 6 miles if you are in your favored terrain): aberrations, celestials, dragons, elementals, fey, fiends, and undead. This feature doesn’t reveal the creatures’ location or number.",
					RequiredLevel = 3,
					FeatureForClass = ranger,
				};
				Feature extraAttackRanger = new Feature
				{
					FeatureName = "Extra Attack",
					FeatureDescription = "You can attack twice, instead of once, whenever you take the Attack action on your turn.",
					RequiredLevel = 5,
					FeatureForClass = ranger,
				};
				Feature landStrideRanger = new Feature
				{
					FeatureName = "Land's Stride",
					FeatureDescription = "Moving through nonmagical difficult terrain costs you no extra movement. You can also pass through nonmagical plants without being slowed by them and without taking damage from them if they have thorns, spines, or a similar hazard. In addition, you have advantage on saving throws against plants that are magically created or manipulated to impede movement, such those created by the entangle spell.",
					RequiredLevel = 8,
					FeatureForClass = ranger,
				};
				Feature hidePlainSight = new Feature
				{
					FeatureName = "Hide in Plain Sight",
					FeatureDescription = "You can spend 1 minute creating camouflage for yourself. You must have access to fresh mud, dirt, plants, soot, and other naturally occurring materials with which to create your camouflage. Once you are camouflaged in this way, you can try to hide by pressing yourself up against a solid surface, such as a tree or wall, that is at least as tall and wide as you are. You gain a +10 bonus to Dexterity(Stealth) checks as long as you remain there without moving or taking actions.Once you move or take an action or a reaction, you must camouflage yourself again to gain this benefit.",
					RequiredLevel = 10,
					FeatureForClass = ranger,
				};
				Feature vanish = new Feature
				{
					FeatureName = "Vanish",
					FeatureDescription = "You can use the Hide action as a bonus action on your turn. Also, you can’t be tracked by nonmagical means, unless you choose to leave a trail.",
					RequiredLevel = 14,
					FeatureForClass = ranger,
				};
				Feature feralSenses = new Feature
				{
					FeatureName = "Feral Senses",
					FeatureDescription = "You gain preternatural senses that help you fight creatures you can’t see. When you attack a creature you can’t see, your inability to see it doesn’t impose disadvantage on your attack rolls against it. You are also aware of the location of any invisible creature within 30 feet of you, provided that the creature isn’t hidden from you and you aren’t blinded or deafened.",
					RequiredLevel = 18,
					FeatureForClass = ranger,
				};
				Feature foeSlayer = new Feature
				{
					FeatureName = "Foe Slayer",
					FeatureDescription = "You become an unparalleled hunter of your enemies. Once on each of your turns, you can add your Wisdom modifier to the attack roll or the damage roll of an attack you make against one o f your favored enemies. You can choose to use this feature before or after the roll, but before any effects of the roll are applied.",
					RequiredLevel = 20,
					FeatureForClass = ranger,
				};
				Feature hunterPrey = new Feature
				{
					FeatureName = "Hunter's Prey",
					FeatureDescription = "You gain one of the following features of your choice.\r\nColossus Slayer\r\nYour tenacity can wear down the most potent foes. When you hit a creature with a weapon attack, the creature takes an extra 1d8 damage if it’s below its hit point maximum. You can deal this extra damage only once per turn.\r\nGiant Killer\r\nWhen a Large or larger creature within 5 feet of you hits or misses you with an attack, you can use your reaction to attack that creature immediately after its attack, provided that you can see the creature.\r\nHorde Breaker\r\nOnce on each of your turns when you make a weapon attack, you can make another attack with the same weapon against a different creature that is within 5 feet of the original target and within range of your weapon.",
					RequiredLevel = 3,
					FeatureForClass = ranger,
					FeatureForArchetype = hunter
				};
				Feature defensiveTactics = new Feature
				{
					FeatureName = "Defensive Tactics",
					FeatureDescription = "You gain one of the following features of your choice.\r\nEscape the Horde\r\nOpportunity attacks against you are made with disadvantage.\r\nMultiattack Defense\r\nWhen a creature hits you with an attack, you gain a +4 bonus to AC against all subsequent attacks made by that creature for the rest of the turn.\r\nSteel Will\r\nYou have advantage on saving throws against being frightened.",
					RequiredLevel = 7,
					FeatureForClass = ranger,
					FeatureForArchetype = hunter
				};
				Feature multiAttack = new Feature
				{
					FeatureName = "Multi-attack",
					FeatureDescription = "You gain one of the following features of your choice.\r\nVolley\r\nYou can use your action to make a ranged attack against any number of creatures within 10 feet of a point you can see within your weapon’s range. You must have ammunition for each target, as normal, and you make a separate attack roll for each target.\r\nWhirlwind Attack\r\nYou can use your action to make a melee attack against any number of creatures within 5 feet of you, with a separate attack roll for each target.",
					RequiredLevel = 11,
					FeatureForClass = ranger,
					FeatureForArchetype = hunter
				};
				Feature superiorHunterDefense = new Feature
				{
					FeatureName = "Superior Hunter's Defense",
					FeatureDescription = "You gain one of the following features of your choice. \r\nEvasion\r\nYou can nimbly dodge out of the way of certain area effects, such as a red dragon’s fiery breath or a lightning bolt spell. When you are subjected to an effect that allows you to make a Dexterity saving throw to take only half damage, you instead take no damage if you succeed on the saving throw, and only half damage if you fail.\r\nStand Against the Tide\r\nWhen a hostile creature misses you with a melee attack, you can use your reaction to force that creature to repeat the same attack against another creature (other than itself) of your choice.\r\nUncanny Dodge\r\nWhen an attacker that you can see hits you with an attack, you can use your reaction to halve the attack’s damage against you.",
					RequiredLevel = 15,
					FeatureForClass = ranger,
					FeatureForArchetype = hunter
				};
				Feature rangerCompanion = new Feature
				{
					FeatureName = "Ranger's Companion",
					FeatureDescription = "You gain a beast companion that accompanies you on your adventures and is trained to fight alongside you. Choose a beast that is no larger than Medium and that has a challenge rating of 1/4 or lower. Add your proficiency bonus to the beast’s AC, attack rolls, and damage rolls, as well as to any saving throws and skills it is proficient in. Its hit point maximum equals its normal maximum or four times your ranger level, whichever is higher. The beast obeys your commands as best as it can. It takes its turn on your initiative, though it doesn’t take an action unless you command it to. On your turn, you can verbally command the beast where to move (no action required by you). You can use your action to verbally command it to take the Attack, Dash, Disengage, Dodge, or Help action. Once you have the Extra Attack feature, you can make one weapon attack yourself when you command the beast to take the Attack action. While traveling through your favored terrain with only the beast, you can move stealthily at a normal pace. If the beast dies, you can obtain another one by spending 8 hours magically bonding with another beast that isn’t hostile to you, either the same type of beast as before or a different one.",
					RequiredLevel = 3,
					FeatureForClass = ranger,
					FeatureForArchetype = beastMaster
				};
				Feature exceptionalTraining = new Feature
				{
					FeatureName = "Exceptional Training",
					FeatureDescription = "On any of your turns when your beast companion doesn’t attack, you can use a bonus action to command the beast to take the Dash, Disengage, Dodge, or Help action on its turn.",
					RequiredLevel = 7,
					FeatureForClass = ranger,
					FeatureForArchetype = beastMaster
				};
				Feature bestialFury = new Feature
				{
					FeatureName = "Bestial Fury",
					FeatureDescription = "Your beast companion can make two attacks when you command it to use the Attack action.",
					RequiredLevel = 11,
					FeatureForClass = ranger,
					FeatureForArchetype = beastMaster
				};
				Feature shareSpells = new Feature
				{
					FeatureName = "Share Spells",
					FeatureDescription = "When you cast a spell targeting yourself, you can also affect your beast companion with the spell if the beast is within 30 feet of you.",
					RequiredLevel = 15,
					FeatureForClass = ranger,
					FeatureForArchetype = beastMaster
				};
				db.Features.Add(favoredEnemy);
				db.Features.Add(naturalExplorer);
				db.Features.Add(fightingStyleRanger);
				db.Features.Add(primevalAwareness);
				db.Features.Add(extraAttackRanger);
				db.Features.Add(landStrideRanger);
				db.Features.Add(hidePlainSight);
				db.Features.Add(vanish);
				db.Features.Add(feralSenses);
				db.Features.Add(foeSlayer);
				db.Features.Add(hunterPrey);
				db.Features.Add(defensiveTactics);
				db.Features.Add(multiAttack);
				db.Features.Add(superiorHunterDefense);
				db.Features.Add(rangerCompanion);
				db.Features.Add(exceptionalTraining);
				db.Features.Add(bestialFury);
				db.Features.Add(shareSpells);
				#endregion Ranger Features
				#region Rogue Features
				Feature expertiseRogue = new Feature
				{
					FeatureName = "Expertise",
					FeatureDescription = "Choose two of your skill proficiencies, or one of your skill proficiencies and your proficiency with thieves’ tools. Your proficiency bonus is doubled for any ability check you make that uses either of the chosen proficiencies. At 6th level, you can choose two more of your proficiencies (in skills or with thieves’ tools) to gain this benefit.",
					RequiredLevel = 1,
					FeatureForClass = rogue,
				};
				Feature sneakAttack = new Feature
				{
					FeatureName = "Sneak Attack",
					FeatureDescription = "You know how to strike subtly and exploit a foe’s distraction. Once per turn, you can deal an extra 1d6 damage to one creature you hit with an attack if you have advantage on the attack roll. The attack must use a finesse or a ranged weapon. You don’t need advantage on the attack roll if another enemy of the target is within 5 feet of it, that enemy isn’t incapacitated, and you don’t have disadvantage on the attack roll. The amount of the extra damage increases as you gain levels in this class.",
					RequiredLevel = 1,
					FeatureForClass = rogue,
				};
				Feature thievesCantFeature = new Feature
				{
					FeatureName = "Thieves' Cant",
					FeatureDescription = "During your rogue training you learned thieves’ cant, a secret mix of dialect, jargon, and code that allows you to hide messages in seemingly normal conversation. Only another creature that knows thieves’ cant understands such messages. It takes four times longer to convey such a message than it does to speak the same idea plainly. In addition, you understand a set of secret signs and symbols used to convey short, simple messages, such as whether an area is dangerous or the territory of a thieves’ guild, whether loot is nearby, or whether the people in an area are easy marks or will provide a safe house for thieves on the run.",
					RequiredLevel = 1,
					FeatureForClass = rogue,
				};
				Feature cunningAction = new Feature
				{
					FeatureName = "Cunning Action",
					FeatureDescription = "Your quick thinking and agility allow you to move and act quickly. You can take a bonus action on each of your turns in combat. This action can be used only to take the Dash, Disengage, or Hide action.",
					RequiredLevel = 2,
					FeatureForClass = rogue,
				};
				Feature uncannyDodge = new Feature
				{
					FeatureName = "Uncanny Dodge",
					FeatureDescription = "When an attacker that you can see hits you with an attack, you can use your reaction to halve the attack’s damage against you.",
					RequiredLevel = 5,
					FeatureForClass = rogue,
				};
				Feature evasionRogue = new Feature
				{
					FeatureName = "Evasion",
					FeatureDescription = "You can nimbly dodge out of the way of certain area effects, such as a red dragon’s fiery breath or an ice storm spell. When you are subjected to an effect that allows you to make a Dexterity saving throw to take only half damage, you instead take no damage if you succeed on the saving throw, and only half damage if you fail.",
					RequiredLevel = 7,
					FeatureForClass = rogue,
				};
				Feature reliableTalent = new Feature
				{
					FeatureName = "Reliable Talent",
					FeatureDescription = "You have refined your chosen skills until they approach perfection. Whenever you make an ability check that lets you add your proficiency bonus, you can treat a d20 roll o f 9 or lower as a 10.",
					RequiredLevel = 11,
					FeatureForClass = rogue,
				};
				Feature blindsense = new Feature
				{
					FeatureName = "Blindsense",
					FeatureDescription = "If you are able to hear, you are aware of the location of any hidden or invisible creature within 10 feet of you.",
					RequiredLevel = 14,
					FeatureForClass = rogue,
				};
				Feature slipperyMind = new Feature
				{
					FeatureName = "Slippery Mind",
					FeatureDescription = "You have acquired greater mental strength. You gain proficiency in Wisdom saving throws.",
					RequiredLevel = 15,
					FeatureForClass = rogue,
				};
				Feature elusive = new Feature
				{
					FeatureName = "Elusive",
					FeatureDescription = "You are so evasive that attackers rarely gain the upper hand against you. No attack roll has advantage against you while you aren’t incapacitated.",
					RequiredLevel = 18,
					FeatureForClass = rogue,
				};
				Feature strokeOfLuck = new Feature
				{
					FeatureName = "Stroke of Luck",
					FeatureDescription = "You have an uncanny knack for succeeding when you need to.If your attack m isses a target within range, you can turn the miss into a hit. Alternatively, if you fail an ability check, you can treat the d20 roll as a 20. Once you use this feature, you can’t use it again until you finish a short or long rest.",
					RequiredLevel = 20,
					FeatureForClass = rogue,
				};
				Feature fastHands = new Feature
				{
					FeatureName = "Fast Hands",
					FeatureDescription = "You can use the bonus action granted by your Cunning Action to make a Dexterity (Sleight of Hand) check, use your thieves’ tools to disarm a trap or open a lock, or take the Use an Object action.",
					RequiredLevel = 3,
					FeatureForClass = rogue,
					FeatureForArchetype = thief
				};
				Feature secondStoryWork = new Feature
				{
					FeatureName = "Second-Story Work",
					FeatureDescription = "You gain the ability to climb faster than normal; climbing no longer costs you extra movement. In addition, when you make a running jump, the distance you cover increases by a number of feet equal to your Dexterity modifier.",
					RequiredLevel = 3,
					FeatureForClass = rogue,
					FeatureForArchetype = thief
				};
				Feature supremeSneak = new Feature
				{
					FeatureName = "Supreme Sneak",
					FeatureDescription = "You have advantage on a Dexterity(Stealth) check if you move no more than half your speed on the same turn.",
					RequiredLevel = 9,
					FeatureForClass = rogue,
					FeatureForArchetype = thief
				};
				Feature useMagicDevice = new Feature
				{
					FeatureName = "Use Magic Device",
					FeatureDescription = "You have learned enough about the workings of magic that you can improvise the use of items even when they are not intended for you. You ignore all class, race, and level requirements on the use of magic items.",
					RequiredLevel = 13,
					FeatureForClass = rogue,
					FeatureForArchetype = thief
				};
				Feature thiefReflexes = new Feature
				{
					FeatureName = "Thief's Reflexes",
					FeatureDescription = "You have become adept at laying ambushes and quickly escaping danger. You can take two turns during the first round of any combat. You take your first turn at your normal initiative and your second turn at your initiative minus 10. You can’t use this feature when you are surprised.",
					RequiredLevel = 17,
					FeatureForClass = rogue,
					FeatureForArchetype = thief
				};
				Feature bonusProficienciesAssassin = new Feature
				{
					FeatureName = "Bonus Proficiencies",
					FeatureDescription = "You gain proficiency with the disguise kit and the poisoner’s kit.",
					RequiredLevel = 3,
					FeatureForClass = rogue,
					FeatureForArchetype = assassin
				};
				Feature assassinate = new Feature
				{
					FeatureName = "Assassinate",
					FeatureDescription = "You are at your deadliest when you get the drop on your enemies. You have advantage on attack rolls against any creature that hasn’t taken a turn in the combat yet. In addition, any hit you score against a creature that is surprised is a critical hit.",
					RequiredLevel = 3,
					FeatureForClass = rogue,
					FeatureForArchetype = assassin
				};
				Feature infiltrationExpertise = new Feature
				{
					FeatureName = "Infiltration Expertise",
					FeatureDescription = "You can unfailingly create false identities for yourself. You must spend seven days and 25 gp to establish the history, profession, and affiliations for an identity. You can’t establish an identity that belongs to someone else. For example, you might acquire appropriate clothing, letters of introduction, and official-looking certification to establish yourself as a member of a trading house from a remote city so you can insinuate yourself into the company of other wealthy merchants. Thereafter, if you adopt the new identity as a disguise, other creatures believe you to be that person until given an obvious reason not to.",
					RequiredLevel = 9,
					FeatureForClass = rogue,
					FeatureForArchetype = assassin
				};
				Feature impostor = new Feature
				{
					FeatureName = "Impostor",
					FeatureDescription = "You gain the ability to unerringly mimic another person’s speech, writing, and behavior. You must spend at least three hours studying these three components of the person’s behavior, listening to speech, examining handwriting, and observing mannerisms. Your ruse is indiscernible to the casual observer. If a wary creature suspects something is amiss, you have advantage on any Charisma(Deception) check you make to avoid detection.",
					RequiredLevel = 13,
					FeatureForClass = rogue,
					FeatureForArchetype = assassin
				};
				Feature deathStrike = new Feature
				{
					FeatureName = "Death Strike",
					FeatureDescription = "You become a master of instant death. When you attack and hit a creature that is surprised, it must make a Constitution saving throw (DC 8 + your Dexterity modifier + your proficiency bonus). On a failed save, double the damage of your attack against the creature.",
					RequiredLevel = 17,
					FeatureForClass = rogue,
					FeatureForArchetype = assassin
				};
				Feature mageHandLegerdemain = new Feature
				{
					FeatureName = "Mage Hand Legerdemain",
					FeatureDescription = "Starting at 3rd level, when you cast mage hand, you canmake the spectral hand invisible, and you can perform the following additional tasks with it:\r\n • You can stow one object the hand is holding in a container worn or carried by another creature.\r\n • You can retrieve an object in a container worn or carried by another creature.\r\n • You can use thieves’ tools to pick locks and disarm traps at range.\r\nYou can perform one of these tasks without being noticed by a creature if you succeed on a Dexterity(Sleight of Hand) check contested by the creature’s Wisdom (Perception)check. In addition, you can use the bonus action granted by your Cunning Action to control the hand.",
					RequiredLevel = 3,
					FeatureForClass = rogue,
					FeatureForArchetype = arcaneTrickster
				};
				Feature magicalAmbush = new Feature
				{
					FeatureName = "Magical Ambush",
					FeatureDescription = "If you are hidden from a creature when you cast a spell on it, the creature has disadvantage on any saving throw it makes against the spell this turn.",
					RequiredLevel = 9,
					FeatureForClass = rogue,
					FeatureForArchetype = arcaneTrickster
				};
				Feature versatileTrickster = new Feature
				{
					FeatureName = "Versatile Trickster",
					FeatureDescription = "You gain the ability to distract targets with your mage hand. As a bonus action on your turn, you can designate a creature within 5 feet of the spectral hand created by the spell. Doing so gives you advantage on attack rolls against that creature until the end of the turn.",
					RequiredLevel = 13,
					FeatureForClass = rogue,
					FeatureForArchetype = arcaneTrickster
				};
				Feature spellThief = new Feature
				{
					FeatureName = "Spell Thief",
					FeatureDescription = "You gain the ability to magically steal the knowledge of how to cast a spell from another spellcaster. Immediately after a creature casts a spell that targets you or includes you in its area of effect, you can use your reaction to force the creature to make a saving throw with its spellcasting ability modifier. The DC equals your spell save DC. On a failed save, you negate the spell’s effect against you, and you steal the knowledge of the spell if it is at least 1st level and of a level you can cast (it doesn’t need to be a wizard spell). For the next 8 hours, you know the spell and can cast it using your spell slots. The creature can’t cast that spell until the 8 hours have passed. Once you use this feature, you can’t use it again until you finish a long rest.",
					RequiredLevel = 17,
					FeatureForClass = rogue,
					FeatureForArchetype = arcaneTrickster
				};
				db.Features.Add(expertiseRogue);
				db.Features.Add(sneakAttack);
				db.Features.Add(thievesCantFeature);
				db.Features.Add(cunningAction);
				db.Features.Add(uncannyDodge);
				db.Features.Add(evasionRogue);
				db.Features.Add(reliableTalent);
				db.Features.Add(blindsense);
				db.Features.Add(slipperyMind);
				db.Features.Add(elusive);
				db.Features.Add(strokeOfLuck);
				db.Features.Add(fastHands);
				db.Features.Add(secondStoryWork);
				db.Features.Add(supremeSneak);
				db.Features.Add(useMagicDevice);
				db.Features.Add(thiefReflexes);
				db.Features.Add(bonusProficienciesAssassin);
				db.Features.Add(assassinate);
				db.Features.Add(infiltrationExpertise);
				db.Features.Add(impostor);
				db.Features.Add(deathStrike);
				db.Features.Add(mageHandLegerdemain);
				db.Features.Add(magicalAmbush);
				db.Features.Add(versatileTrickster);
				db.Features.Add(spellThief);
				#endregion Rogue Features
				#region Sorcerer Features
				Feature sorceryPoints = new Feature
				{
					FeatureName = "Sorcery Points",
					FeatureDescription = "You have Sorcery Points equal to your sorcerer level which you can spend to use Sorcerer abilities. You regain all spent Sorcery Points when you finish a long rest.",
					RequiredLevel = 2,
					FeatureForClass = sorcerer,
				};
				Feature flexibleCasting = new Feature
				{
					FeatureName = "Flexible Casting",
					FeatureDescription = "You can use Sorcery Points to gain additional spell slots, or sacrifice spell slots to gain additional Sorcery Points.\r\nCreating Spell Slots\r\nYou can transform unexpended Sorcery Points into one spell slot as a bonus action on your turn. You can create spell slots no higher in level than 5th.\r\nSpell Slot Sorcery\r\nLevel Point Cost\r\n1st 2\r\n2nd 3\r\n3rd 5\r\n4th 6\r\n5th 7\r\nConverting a Spell Slot to Sorcery Points\r\nAs A bonus action on your turn, you can expend one spell slot and gain a number of sorcery points equal to the slot's level.",
					RequiredLevel = 2,
					FeatureForClass = sorcerer,
				};
				Feature metamagic = new Feature
				{
					FeatureName = "Metamagic",
					FeatureDescription = "You gain the ability to twist your spells to suit your needs.You gain two of the following Metamagic options of your choice. You gain another one at 10th and 17th level. You can use only one Metamagic option on a spell when you cast it, unless otherwise noted.\r\nCarefull Spell\r\nWhen you cast a spell that forces other creatures to make a saving throw, you can protect some of those creatures from the spell’s full force. To do so, you spend 1 sorcery point and choose a number of those creatures up to your Charisma modifier(minimum of one creature). A chosen creature automatically succeeds on its saving throw against the spell.\r\nDistant Spell\r\nWhen you cast a spell that has a range of 5 feet or greater, you can spend 1 sorcery point to double the range of the spell. When you cast a spell that has a range of touch, you can spend 1 sorcery point to make the range of the spell 30 feet.\r\nEmpowered Spell\r\nWhen you roll damage for a spell, you can spend 1 sorcery point to reroll a number of the damage dice up to your Charisma modifier (minimum of one).You must use the new rolls. You can use Empowered Spell even if you have already used a different Metamagic option during the casting of the spell.\r\nExtended Spell\r\nWhen you cast a spell that has a duration of 1 minute or longer, you can spend 1 sorcery point to double its duration, to a maximum duration of 24 hours.\r\nHeightened Spell\r\nWhen you cast a spell that forces a creature to make a saving throw to resist its effects, you can spend 3 sorcery points to give one target of the spell disadvantage on its first saving throw made against the spell.\r\nQuickened Spell\r\nWhen you cast a spell that has a casting time of 1 action, you can spend 2 sorcery points to change the casting time to 1 bonus action for this casting.\r\nSubtle Spell\r\nWhen you cast a spell, you can spend 1 sorcery point to cast it without any somatic or verbal components.\r\nTwinned Spell\r\nWhen you cast a spell that targets only one creature and doesn’t have a range of self, you can spend a number of sorcery points equal to the spell’s level to target a second creature in range with the same spell (1 sorcery point if the spell is a cantrip).",
					RequiredLevel = 3,
					FeatureForClass = sorcerer,
				};
				Feature sorcerousRestoration = new Feature
				{
					FeatureName = "Sorcerous Restoration",
					FeatureDescription = "You regain 4 expended Sorcery Points whenever you finish a short rest.",
					RequiredLevel = 20,
					FeatureForClass = sorcerer,
				};
				Feature dragonAncestor = new Feature
				{
					FeatureName = "Dragon Ancestor",
					FeatureDescription = "At 1st level, you choose one type of dragon as your ancestor. The damage type associated with each dragon is used by features you gain later.\r\nDraconic Ancestry\r\nDragon Damage Type\r\nBlack Acid\r\nBlue Lightning\r\nBrass Fire\r\nBronze Lightning\r\nCopper Acid\r\nCold Fire\r\nGreen Poison\r\nRed Fire\r\nSilver Cold\r\nWhite Cold\r\nYou can speak, read, and write Draconic. Additionally, whenever you make a Charisma check when interacting with dragons, your proficiency bonus is doubled if it applies to the check.",
					RequiredLevel = 1,
					FeatureForClass = sorcerer,
					FeatureForArchetype = draconicBloodline
				};
				Feature draconicResilience = new Feature
				{
					FeatureName = "Draconic Resilience",
					FeatureDescription = "As magic flows through your body, it causes physical traits of your dragon ancestors to emerge. At 1st level, your hit point maximum increases by 1 and increases by 1 again whenever you gain a level in this class. Additionally, parts of your skin are covered by a thin sheen of dragon-like scales. When you aren’t wearing armor, your AC equals 13+ your Dexterity modifier.",
                    RequiredLevel = 1,
					FeatureForClass = sorcerer,
					FeatureForArchetype = draconicBloodline
				};
				Feature elementalAffinity = new Feature
				{
					FeatureName = "Elemental Affinity",
					FeatureDescription = "When you cast a spell that deals damage of the type associated with your draconic ancestry, add your Charisma modifier to that damage. At the same time, you can spend 1 sorcery point to gain resistance to that damage type for 1 hour.",
					RequiredLevel = 6,
					FeatureForClass = sorcerer,
					FeatureForArchetype = draconicBloodline
				};
				Feature dragonWings = new Feature
				{
					FeatureName = "Dragon Wings",
					FeatureDescription = "You gain the ability to sprout a pair of dragon wings from your back, gaining a flying speed equal to your current speed. You can create these wings as a bonus action on your turn. They last until you dismiss them as a bonus action on your turn. You can’t manifest your wings while wearing armor unless the armor is made to accommodate them, and clothing not made to accommodate your wings might be destroyed when you manifest them.",
					RequiredLevel = 14,
					FeatureForClass = sorcerer,
					FeatureForArchetype = draconicBloodline
				};
				Feature draconicPresence = new Feature
				{
					FeatureName = "Draconic Presence",
					FeatureDescription = "You can channel the dread presence of your dragon ancestor, causing those around you to become awestruck or frightened. As an action, you can spend 5 sorcery points to draw on this power and exude an aura of awe or fear (your choice) to a distance of 60 feet. For 1 minute or until you lose your concentration (as if you were casting a concentration spell), each hostile creature that starts its turn in this aura must succeed on a Wisdom saving throw or be charmed (if you chose awe) or frightened (if you chose fear) until the aura ends. A creature that succeeds on this saving throw is immune to your aura for 24 hours.",
					RequiredLevel = 18,
					FeatureForClass = sorcerer,
					FeatureForArchetype = draconicBloodline
				};
				//TODO: Add a Wild Magic Surge table
				Feature wildMagicSurge = new Feature
				{
					FeatureName = "Wild Magic Surge",
					FeatureDescription = "Your spellcasting can unleash surges o f untamed magic. Immediately after you cast a sorcerer spell of 1st level or higher, the DM can have you roll a d20. If you roll a 1, roll on the Wild Magic Surge table to create a random magical effect.",
					RequiredLevel = 1,
					FeatureForClass = sorcerer,
					FeatureForArchetype = wildMagic
				};
				Feature tidesOfChaos = new Feature
				{
					FeatureName = "Tides of Chaos",
					FeatureDescription = "You can manipulate the forces of chance and chaos to gain advantage on one attack roll, ability check, or saving throw. Once you do so, you must finish a long rest before you can use this feature again. Any time before you regain the use of this feature, the DM can have you roll on the Wild Magic Surge table immediately after you cast a sorcerer spell of 1st level or higher. You then regain the use of this feature.",
					RequiredLevel = 1,
					FeatureForClass = sorcerer,
					FeatureForArchetype = wildMagic
				};
				Feature bendLuck = new Feature
				{
					FeatureName = "Bend Luck",
					FeatureDescription = "You have the ability to twist fate using your wild magic. When another creature you can see makes an attack roll, an ability check, or a saving throw, you can use your reaction and spend 2 sorcery points to roll 1d4 and apply the number rolled as a bonus or penalty (your choice) to the creature’s roll. You can do so after the creature rolls but before any effects of the roll occur.",
					RequiredLevel = 6,
					FeatureForClass = sorcerer,
					FeatureForArchetype = wildMagic
				};
				Feature controlledChaos = new Feature
				{
					FeatureName = "Controlled Chaos",
					FeatureDescription = "You gain a modicum of control over the surges of your wild magic. Whenever you roll on the Wild Magic Surge table, you can roll twice and use either number.",
					RequiredLevel = 14,
					FeatureForClass = sorcerer,
					FeatureForArchetype = wildMagic
				};
				Feature spellBombardment = new Feature
				{
					FeatureName = "Spell Bombardment",
					FeatureDescription = "The harmful energy of your spells intensifies. When you roll damage for a spell and roll the highest number possible on any of the dice, choose one of those dice, roll it again and add that roll to the damage. You can use the feature only once per turn.",
					RequiredLevel = 18,
					FeatureForClass = sorcerer,
					FeatureForArchetype = wildMagic
				};
				db.Features.Add(sorceryPoints);
				db.Features.Add(flexibleCasting);
				db.Features.Add(metamagic);
				db.Features.Add(sorcerousRestoration);
				db.Features.Add(dragonAncestor);
				db.Features.Add(draconicResilience);
				db.Features.Add(elementalAffinity);
				db.Features.Add(dragonWings);
				db.Features.Add(draconicPresence);
				db.Features.Add(wildMagicSurge);
				db.Features.Add(tidesOfChaos);
				db.Features.Add(bendLuck);
				db.Features.Add(controlledChaos);
				db.Features.Add(spellBombardment);
				#endregion Sorcerer Features
				#region Warlock 
				//TODO: Build an Invocations subtable. Structure seems similar to Elemental Monk "spells"
				Feature eldritchInvocations = new Feature
				{
					FeatureName = "Eldritch Invocations",
					FeatureDescription = "In your study of occult lore, you have unearthed eldritch invocations, fragments of forbidden knowledge that imbue you with an abiding magical ability. you gain two eldritch invocations of your choice.Your invocation options are detailed at the end of the class description. When you gain certain warlock levels, you gain additional invocations of your choice, as shown in the Invocations Known column of the Warlock table. Additionally, when you gain a level in this class, you can choose one of the invocations you know and replace it with another invocation that you could learn at that level.",
					RequiredLevel = 2,
					FeatureForClass = warlock,
				};
				Feature pactBoon = new Feature
				{
					FeatureName = "Pact Boon",
					FeatureDescription = "Your otherworldly patron bestows a gift upon you for your loyal service. You gain one of the following features of your choice.\r\nPact of the Chain\r\n You learn the find familiar spell and can cast it as a ritual. The spell doesn’t count against your number of spells known. When you cast the spell, you can choose one of the normal forms for your familiar or one of the following special forms: imp, pseudodragon, quasit, or sprite. Additionally, when you take the Attack action, you can forgo one of your own attacks to allow your familiar to make one attack of its own.\r\nPact of the Blade\r\nYou can use your action to create a pact weapon in your empty hand. You can choose the form that this melee weapon takes each time you create it (see chapter 5 for weapon options). You are proficient with it while you wield it. This weapon counts as magical for the purpose of overcoming resistance and immunity to nonmagical attacks and damage. Your pact weapon disappears if it is more than 5 feet away from you for 1 minute or more. It also disappears if you use this feature again, if you dismiss the weapon (no action required), or if you die. You can transform one magic weapon into your pact weapon by performing a special ritual while you hold the weapon. You perform the ritual over the course of 1 hour, which can be done during a short rest. You can then dismiss the weapon, shunting it into an extradimensional space, and it appears whenever you create your pact weapon thereafter. You can’t affect an artifact or a sentient weapon in this way. The weapon ceases being your pact weapon if you die, if you perform the 1-hour ritual on a different weapon, or if you use a 1-hour ritual to break your bond to it. The weapon appears at your feet if it is in the extradimensional space when the bond breaks.\r\nPact of the Tome\r\nYour patron gives you a grimoire called a Book of Shadows. When you gain this feature, choose three cantrips from any class’s spell list. While the book is on your person, you can cast those cantrips at will. They don’t count against your number of cantrips known. If you lose your Book of Shadows, you can perform a 1-hour ceremony to receive a replacement from your patron. This ceremony can be performed during a short or long rest, and it destroys the previous book. The book turns to ash when you die.",
					RequiredLevel = 3,
					FeatureForClass = warlock,
				};
				Feature mysticArcanum = new Feature
				{
					FeatureName = "Mystic Arcanum",
					FeatureDescription = "Your patron bestows upon you a magical secret called an arcanum. Choose one 6th-level spell from the warlock spell list as this arcanum. You can cast your arcanum spell once without expending a spell slot. You must finish a long rest before you can do so again. At higher levels, you gain more warlock spells of your choice that can be cast in this way: one 7th-level spell at 13th level, one 8th-level spell at 15th level, and one 9th-level spell at 17th level. You regain all uses of your Mystic Arcanum when you finish a long rest.",
					RequiredLevel = 11,
					FeatureForClass = warlock,
				};
				Feature eldritchMaster = new Feature
				{
					FeatureName = "Eldritch Master",
					FeatureDescription = "You can draw on your inner reserve of mystical power while entreating your patron to regain expended spell slots. You can spend 1 minute entreating your patron for aid to regain all your expended spell slots from your Pact Magic feature. Once you regain spell slots with this feature, you must finish a long rest before you can do so again.",
					RequiredLevel = 20,
					FeatureForClass = warlock,
				};
				Feature expandedSpellsArchfey = new Feature
				{
					FeatureName = "Expanded Spell List",
					FeatureDescription = "The Archfey lets you choose from an expanded list of spells when you learn a warlock spell. The following spells are added to the warlock spell list for you.\r\n1st faerie fire, sleep\r\n2nd calm emotions, phantasmal force\r\n3rd blink, plant growth\r\n4th dominate beast, greater invisibility\r\n5th dominate person, seeming",
					RequiredLevel = 1,
					FeatureForClass = warlock,
					FeatureForArchetype = archfey
				};
				Feature feyPresence = new Feature
				{
					FeatureName = "Fey Presence",
					FeatureDescription = "Your patron bestows upon you the ability to project the beguiling and fearsome presence of the fey. As an action, you can cause each creature in a 10-foot cube originating from you to make a Wisdom saving throw against your warlock spell save DC. The creatures that fail their saving throws are all charmed or frightened by you (your choice) until the end of your next turn. Once you use this feature, you can’t use it again until you finish a short or long rest.",
					RequiredLevel = 1,
					FeatureForClass = warlock,
					FeatureForArchetype = archfey
				};
				Feature mistyEscape = new Feature
				{
					FeatureName = "Misty Escape",
					FeatureDescription = "You can vanish in a puff of mist in response to harm. When you take damage, you can use your reaction to turn invisible and teleport up to 60 feet to an unoccupied space you can see. You remain invisible until the start of your next turn or until you attack or cast a spell. Once you use this feature, you can't use it again until you finish a short or long rest.",
					RequiredLevel = 6,
					FeatureForClass = warlock,
					FeatureForArchetype = archfey
				};
				Feature beguilingDefenses = new Feature
				{
					FeatureName = "Beguiling Defenses",
					FeatureDescription = "Your patron teaches you how to turn the mind-affecting magic of your enemies against them. You are immune to being charmed, and when another creature attempts to charm you, you can use your reaction to attempt to turn the charm back on that creature. The creature must succeed on a Wisdom saving throw against your warlock spell save DC or be charmed by you for 1 minute or until the creature takes any damage.",
					RequiredLevel = 10,
					FeatureForClass = warlock,
					FeatureForArchetype = archfey
				};
				Feature darkDelirium = new Feature
				{
					FeatureName = "Dark Delirium",
					FeatureDescription = "You can plunge a creature into an illusory realm. As an action, choose a creature that you can see within 60 feet of you. It must make a Wisdom saving throw against your warlock spell save DC. On a failed save, it is charmed or frightened by you (your choice) for 1 minute or until your concentration is broken (as if you are concentrating on a spell). This effect ends early if the creature takes any damage. Until this illusion ends, the creature thinks it is lost in a misty realm, the appearance of which you choose. The creature can see and hear only itself, you, and the illusion. You must finish a short or long rest before you can use this feature again.",
					RequiredLevel = 14,
					FeatureForClass = warlock,
					FeatureForArchetype = archfey
				};
				Feature expandedSpellsFiend = new Feature
				{
					FeatureName = "Expanded Spell List",
					FeatureDescription = "The Fiend lets you choose from an expanded list of spells when you learn a warlock spell. The following spells are added to the warlock spell list for you.\r\n1st burning hands, command\r\n2nd blindness/deafness, scorching ray\r\n3rd fireball, stinking cloud\r\n4th fire shield, wall of fire\r\n5th flame strike, hallow",
					RequiredLevel = 1,
					FeatureForClass = warlock,
					FeatureForArchetype = fiend
				};
				Feature darkOneBlessing = new Feature
				{
					FeatureName = "Dark One's Blessing",
					FeatureDescription = "When you reduce a hostile creature to 0 hit points, you gain temporary hit points equal to your Charisma modifier + your warlock level (minimum of 1).",
					RequiredLevel = 1,
					FeatureForClass = warlock,
					FeatureForArchetype = fiend
				};
				Feature darkOneOwnLuck = new Feature
				{
					FeatureName = "Dark One's Own Luck",
					FeatureDescription = "You can call on your patron to alter fate in your favor. When you make an ability check or a saving throw, you can use this feature to add a d10 to your roll. You can do so after seeing the initial roll but before any of the roll’s effects occur. Once you use this feature, you can’t use it again until you finish a short or long rest.",
					RequiredLevel = 6,
					FeatureForClass = warlock,
					FeatureForArchetype = fiend
				};
				Feature fiendishResilience = new Feature
				{
					FeatureName = "Fiendish Resilience",
					FeatureDescription = "You can choose one damage type when you finish a short or long rest. You gain resistance to that damage type until you choose a different one with this feature. Damage from magical weapons or silver weapons ignores this resistance.",
					RequiredLevel = 10,
					FeatureForClass = warlock,
					FeatureForArchetype = fiend
				};
				Feature hurlThroughHell = new Feature
				{
					FeatureName = "Hurl Through Hell",
					FeatureDescription = "When you hit a creature with an attack, you can use this feature to instantly transport the target through the lower planes. The creature disappears and hurtles through a nightmare landscape. At the end of your next turn, the target returns to the space it previously occupied, or the nearest unoccupied space. If the target is not a fiend, it takes 10d10 psychic damage as it reels from its horrific experience. Once you use this feature, you can’t use it again until you finish a long rest.",
					RequiredLevel = 1,
					FeatureForClass = warlock,
					FeatureForArchetype = fiend
				};
				Feature expandedSpellsGreatOldOne = new Feature
				{
					FeatureName = "Expanded Spell List",
					FeatureDescription = "The Great Old One lets you choose from an expanded list of spells when you learn a warlock spell. The following spells are added to the warlock spell list for you.\r\n1st dissonant whispers, Tasha's hideous laughter\r\n2nd detect thoughts, phantasmal force\r\n3rd clairvoyance, sending\r\n4th dominate beast, Evard’s black tentacles\r\n5th dominate person, telekinesis",
					RequiredLevel = 1,
					FeatureForClass = warlock,
					FeatureForArchetype = greatOldOne
				};
				Feature awakenedMind = new Feature
				{
					FeatureName = "Awakened  Mind",
					FeatureDescription = "Your alien knowledge gives you the ability to touch the minds of other creatures. You can communicate telepathically with any creature you can see within 30 feet of you. You don’t need to share a language with the creature for it to understand your telepathic utterances, but the creature must be able to understand at least one language.",
					RequiredLevel = 1,
					FeatureForClass = warlock,
					FeatureForArchetype = greatOldOne
				};
				Feature entropicWard = new Feature
				{
					FeatureName = "Entropic Ward",
					FeatureDescription = "You learn to magically ward yourself against attack and to turn an enemy’s failed strike into good luck for yourself. When a creature makes an attack roll against you, you can use your reaction to impose disadvantage on that roll. If the attack misses you, your next attack roll against the creature has advantage if you make it before the end of your next turn. Once you use this feature, you can’t use it again until you finish a short or long rest.",
					RequiredLevel = 6,
					FeatureForClass = warlock,
					FeatureForArchetype = greatOldOne
				};
				Feature thoughtShield = new Feature
				{
					FeatureName = "Thought Shield",
					FeatureDescription = "Your thoughts can’t be read by telepathy or other means unless you allow it. You also have resistance to psychic damage, and whenever a creature deals psychic damage to you, that creature takes the same amount of damage that you do.",
					RequiredLevel = 10,
					FeatureForClass = warlock,
					FeatureForArchetype = greatOldOne
				};
				Feature createThrall = new Feature
				{
					FeatureName = "Create Thrall",
					FeatureDescription = "You gain the ability to infect a humanoid’s mind with the alien magic of your patron. You can use your action to touch an incapacitated humanoid. That creature is then charmed by you until a remove curse spell is cast on it, the charmed condition is removed from it, or you use this feature again. You can communicate telepathically with the charmed creature as long as the two of you are on the same plane of existence.",
					RequiredLevel = 14,
					FeatureForClass = warlock,
					FeatureForArchetype = greatOldOne
				};
				db.Features.Add(eldritchInvocations);
				db.Features.Add(pactBoon);
				db.Features.Add(mysticArcanum);
				db.Features.Add(eldritchMaster);
				db.Features.Add(expandedSpellsArchfey);
				db.Features.Add(feyPresence);
				db.Features.Add(mistyEscape);
				db.Features.Add(beguilingDefenses);
				db.Features.Add(darkDelirium);
				db.Features.Add(expandedSpellsFiend);
				db.Features.Add(darkOneBlessing);
				db.Features.Add(darkOneOwnLuck);
				db.Features.Add(fiendishResilience);
				db.Features.Add(hurlThroughHell);
				db.Features.Add(expandedSpellsGreatOldOne);
				db.Features.Add(awakenedMind);
				db.Features.Add(entropicWard);
				db.Features.Add(thoughtShield);
				db.Features.Add(createThrall);
				#endregion Warlock Features
				#region Wizard Features
				Feature arcaneRecovery = new Feature
				{
					FeatureName = "Arcane Recovery",
					FeatureDescription = "You have learned to regain some of your magical energy by studying your spellbook. Once per day when you finish a short rest, you can choose expended spell slots to recover. The spell slots can have a combined level that is equal to or less than half your wizard level (rounded up), and none of the slots can be 6th level or higher. For example, if you’re a 4th-level wizard, you can recover up to two levels worth of spell slots. You can recover either a 2nd-level spell slot or two 1st-level spell slots.",
					RequiredLevel = 1,
					FeatureForClass = wizard,
				};
				Feature spellMastery = new Feature
				{
					FeatureName = "Spell Mastery",
					FeatureDescription = "You have achieved such mastery over certain spells that you can cast them at will. Choose a 1st-level wizard spell and a 2nd-level wizard spell that are in your spellbook. You can cast those spells at their lowest level without expending a spell slot when you have them prepared. If you want to cast either spell at a higher level, you must expend a spell slot as normal. By spending 8 hours in study, you can exchange one or both of the spells you chose for different spells of the same levels.",
					RequiredLevel = 18,
					FeatureForClass = wizard,
				};
				Feature signatureSpells = new Feature
				{
					FeatureName = "Signature Spells",
					FeatureDescription = "You gain mastery over two powerful spells and can cast them with little effort. Choose two 3rd-level wizard spells in your spellbook as your signature spells. You always have these spells prepared, they don’t count against the number of spells you have prepared, and you can cast each of them once at 3rd level without expending a spell slot. When you do so, you can’t do so again until you finish a short or long rest. If you want to cast either spell at a higher level, you must expend a spell slot as normal.",
					RequiredLevel = 20,
					FeatureForClass = wizard,
				};
				Feature abjurationSavant = new Feature
				{
					FeatureName = "Abjuration Savant",
					FeatureDescription = "The gold and time you must spend to copy an abjuration spell into your spellbook is halved.",
					RequiredLevel = 2,
					FeatureForClass = wizard,
					FeatureForArchetype = abjuration
				};
				Feature arcaneWard = new Feature
				{
					FeatureName = "Arcane Ward",
					FeatureDescription = "You can weave magic around yourself for protection. When you cast an abjuration spell o f 1st level or higher, you can simultaneously use a strand of the spell’s magic to create a magical ward on yourself that lasts until you finish a long rest. The ward has hit points equal to twice your wizard level + your Intelligence modifier. Whenever you take damage, the ward takes the damage instead. If this damage reduces the ward to 0 hit points, you take any remaining damage. While the ward has 0 hit points, it can’t absorb damage, but its magic remains. Whenever you cast an abjuration spell of 1st level or higher, the ward regains a number of hit points equal to twice the level of the spell. Once you create the ward, you can't create it again until you finish a long rest.",
					RequiredLevel = 2,
					FeatureForClass = wizard,
					FeatureForArchetype = abjuration
				};
				Feature projectedWard = new Feature
				{
					FeatureName = "Projected Ward",
					FeatureDescription = "When a creature that you can see within 30 feet of you takes damage, you can use your reaction to cause your Arcane Ward to absorb that damage. If this damage reduces the ward to 0 hit points, the warded creature takes any remaining damage.",
					RequiredLevel = 6,
					FeatureForClass = wizard,
					FeatureForArchetype = abjuration
				};
				Feature improvedAbjuration = new Feature
				{
					FeatureName = "Improved Abjuration",
					FeatureDescription = "When you cast an abjuration spell that requires you to make an ability check as a part of casting that spell (as in counterspell and dispel magic), you add your proficiency bonus to that ability check.",
					RequiredLevel = 10,
					FeatureForClass = wizard,
					FeatureForArchetype = abjuration
				};
				Feature spellResistance = new Feature
				{
					FeatureName = "Spell Resistance",
					FeatureDescription = "You have advantage on saving throws against spells. Furthermore, you have resistance against the damage of spells.",
					RequiredLevel = 14,
					FeatureForClass = wizard,
					FeatureForArchetype = abjuration
				};
				Feature conjurationSavant = new Feature
				{
					FeatureName = "Conjuration Savant",
					FeatureDescription = "The gold and time you must spend to copy an conjuration spell into your spellbook is halved.",
					RequiredLevel = 2,
					FeatureForClass = wizard,
					FeatureForArchetype = conjuration
				};
				Feature minorConjuration = new Feature
				{
					FeatureName = "Minor Conjuration",
					FeatureDescription = "Starting at 2nd level when you select this school, you can use your action to conjure up an inanimate object in your hand or on the ground in an unoccupied space that you can see within 10 feet of you. This object can be no larger than 3 feet on a side and weigh no more than 10 pounds, and its form must be that of a nonmagical object that you have seen.The object is visibly magical, radiating dim light out to 5 feet. The object disappears after 1 hour, when you use this feature again, or if it takes any damage.",
					RequiredLevel = 2,
					FeatureForClass = wizard,
					FeatureForArchetype = conjuration
				};
				Feature benignTransposition = new Feature
				{
					FeatureName = "Benign Transposition",
					FeatureDescription = "you can use your action to teleport up to 30 feet to an unoccupied space that you can see. Alternatively, you can choose a space within range that is occupied by a Small or Medium creature. If that creature is willing, you both teleport, swapping places. Once you use this feature, you can’t use it again until you finish a long rest or you cast a conjuration spell of 1st level or higher.",
					RequiredLevel = 6,
					FeatureForClass = wizard,
					FeatureForArchetype = conjuration
				};
				Feature focusedConjuration = new Feature
				{
					FeatureName = "Focused Conjuration",
					FeatureDescription = "While you are concentrating on a conjuration spell, your concentration can’t be broken as a result of taking damage.",
					RequiredLevel = 10,
					FeatureForClass = wizard,
					FeatureForArchetype = conjuration
				};
				Feature durableSummons = new Feature
				{
					FeatureName = "Durable Summons",
					FeatureDescription = "Any creature that you summon or create with a conjuration spell has 30 temporary hit points.",
					RequiredLevel = 14,
					FeatureForClass = wizard,
					FeatureForArchetype = conjuration
				};
				Feature divinationSavant = new Feature
				{
					FeatureName = "Divination Savant",
					FeatureDescription = "The gold and time you must spend to copy an divination spell into your spellbook is halved.",
					RequiredLevel = 2,
					FeatureForClass = wizard,
					FeatureForArchetype = divination
				};
				Feature portent = new Feature
				{
					FeatureName = "Portent",
					FeatureDescription = "Glimpses of the future begin to press in on your awareness. When you finish a long rest, roll two d20s and record the numbers rolled. You can replace any attack roll, saving throw, or ability check made by you or a creature that you can see with one of these foretelling rolls. You must choose to do so before the roll, and you can replace a roll in this way only once per turn. Each foretelling roll can be used only once. When you finish a long rest, you lose any unused foretelling rolls.",
					RequiredLevel = 2,
					FeatureForClass = wizard,
					FeatureForArchetype = divination
				};
				Feature expertDivination = new Feature
				{
					FeatureName = "Expert Divination",
					FeatureDescription = "Casting divination spells comes so easily to you that it expends only a fraction of your spellcasting efforts. When you cast a divination spell of 2nd level or higher using a spell slot, you regain one expended spell slot. The slot you regain must be of a level lower than the spell you cast and can’t be higher than 5th level.",
					RequiredLevel = 6,
					FeatureForClass = wizard,
					FeatureForArchetype = divination
				};
				Feature thirdEye = new Feature
				{
					FeatureName = "The Third Eye",
					FeatureDescription = "You can use your action to increase your powers of perception. When you do so, choose one of the following benefits, which lasts until you are incapacitated or you take a short or long rest. You can’t use the feature again until you finish a rest.\r\nDarkvision\r\nYou gain darkvision out to a range of 60 feet.\r\nEthereal Sight\r\nYou can see into the Ethereal Plane within 60 feet of you.\r\nGreater Comprehension\r\nYou can read any language.\r\nSee Invisibility\r\nYou can see invisible creatures and objects within 10 feet of you that are within line of sight.",
					RequiredLevel = 10,
					FeatureForClass = wizard,
					FeatureForArchetype = divination
				};
				Feature greaterPortent = new Feature
				{
					FeatureName = "Greater Portent",
					FeatureDescription = "The visions in your dreams intensify and paint a more accurate picture in your mind of what is to come. You roll three d20s for your Portent feature, rather than two.",
					RequiredLevel = 14,
					FeatureForClass = wizard,
					FeatureForArchetype = divination
				};
				Feature enchantmentSavant = new Feature
				{
					FeatureName = "Enchantment Savant",
					FeatureDescription = "The gold and time you must spend to copy an enchantment spell into your spellbook is halved.",
					RequiredLevel = 2,
					FeatureForClass = wizard,
					FeatureForArchetype = enchantment
				};
				Feature hypnoticGaze = new Feature
				{
					FeatureName = "Hypnotic Gaze",
					FeatureDescription = "Your soft words and enchanting gaze can magically enthrall another creature. As an action, choose one creature that you can see within 5 feet of you. If the target can see or hear you, it must succeed on a Wisdom saving throw against your wizard spell save DC or be charmed by until the end of your next turn. The charmed creature’s speed drops to 0, and the creature is incapacitated and visibly dazed. On subsequent turns, you can use your action to maintain this effect, extending its duration until the end of your next turn. However, the effect ends if you move more than 5 feet away from the creature, if the creature can neither see nor hear you, or if the creature takes damage. Once the effect ends, or if the creature succeeds on its initial saving throw against this effect, you can’t use this feature on that creature again until you finish a long rest.",
					RequiredLevel = 2,
					FeatureForClass = wizard,
					FeatureForArchetype = enchantment
				};
				Feature instinctiveCharm = new Feature
				{
					FeatureName = "Instinctive Charm",
					FeatureDescription = "When a creature you can see within 30 feet of you makes an attack roll against you, you can use your reaction to divert the attack, provided that another creature is within the attack’s range. The attacker must make a Wisdom saving throw against your wizard spell save DC. On a failed save, the attacker must target the creature that is closest to it, not including you or itself. If multiple creatures are closest, the attacker chooses which one to target. On a successful save, you can’t use this feature on the attacker again until you finish a long rest. You must choose to use this feature before knowing whether the attack hits or misses. Creatures that can’t be charmed are immune to this effect.",
					RequiredLevel = 6,
					FeatureForClass = wizard,
					FeatureForArchetype = enchantment
				};
				Feature splitEnchantment = new Feature
				{
					FeatureName = "Split Enchantment",
					FeatureDescription = "When you cast an enchantment spell of 1st level or higher that targets only one creature, you can have it target a second creature.",
					RequiredLevel = 10,
					FeatureForClass = wizard,
					FeatureForArchetype = enchantment
				};
				Feature alterMemories = new Feature
				{
					FeatureName = "Alter Memories",
					FeatureDescription = "You gain the ability to make a creature unaware of your magical influence on it. When you cast an enchantment spell to charm one or more creatures, you can alter one creature’s understanding so that it remains unaware of being charmed. Additionally, once before the spell expires, you can use your action to try to make the chosen creature forget some of the time it spent charmed. The creature must succeed on an Intelligence saving throw against your wizard spell save DC or lose a number of hours of its memories equal to 1+ your Charisma modifier (minimum 1). You can make the creature forget less time, and the amount of time can’t exceed the duration of your enchantment spell.",
					RequiredLevel = 14,
					FeatureForClass = wizard,
					FeatureForArchetype = enchantment
				};
				Feature evocationSavant = new Feature
				{
					FeatureName = "Evocation Savant",
					FeatureDescription = "The gold and time you must spend to copy an evocation spell into your spellbook is halved.",
					RequiredLevel = 2,
					FeatureForClass = wizard,
					FeatureForArchetype = evocation
				};
				Feature sculptSpells = new Feature
				{
					FeatureName = "Sculpt Spells",
					FeatureDescription = "You can create pockets of relative safety within the effects of your evocation spells. When you cast an evocation spell that affects other creatures that you can see, you can choose a number of them equal to 1+ the spell’s level. The chosen creatures automatically succeed on their saving throws against the spell, and they take no damage if they would normally take half damage on a successful save.",
					RequiredLevel = 2,
					FeatureForClass = wizard,
					FeatureForArchetype = evocation
				};
				Feature potentCantrip = new Feature
				{
					FeatureName = "Potent Cantrip",
					FeatureDescription = "Your damaging cantrips affect even creatures that avoid the brunt of the effect. When a creature succeeds on a saving throw against your cantrip, the creature takes half the cantrip’s damage (if any) but suffers no additional effect from the cantrip.",
					RequiredLevel = 6,
					FeatureForClass = wizard,
					FeatureForArchetype = evocation
				};
				Feature empoweredEvocation = new Feature
				{
					FeatureName = "Empowered Evocation",
					FeatureDescription = "You can add your Intelligence modifier to the damage roll of any wizard evocation spell you cast.",
					RequiredLevel = 10,
					FeatureForClass = wizard,
					FeatureForArchetype = evocation
				};
				Feature overchannel = new Feature
				{
					FeatureName = "Overchannel",
					FeatureDescription = "You can increase the power of your simpler spells. When you cast a wizard spell of 5th level or lower that deals damage, you can deal maximum damage with that spell. The first time you do so, you suffer no adverse effect. If you use this feature again before you finish a long rest, you take 2d12 necrotic damage for each level of the spell, immediately after you cast it. Each time you use this feature again before finishing a long rest, the necrotic damage per spell level increases by 1d12. This damage ignores resistance and immunity.",
					RequiredLevel = 14,
					FeatureForClass = wizard,
					FeatureForArchetype = evocation
				};
				Feature illusionSavant = new Feature
				{
					FeatureName = "Illusion Savant",
					FeatureDescription = "The gold and time you must spend to copy an illusion spell into your spellbook is halved.",
					RequiredLevel = 2,
					FeatureForClass = wizard,
					FeatureForArchetype = conjuration
				};
				Feature improvedMinorIllusion = new Feature
				{
					FeatureName = "Improved Minor Illusion",
					FeatureDescription = "You learn the minor illusion cantrip. If you already know this cantrip, you learn a different wizard cantrip of your choice. The cantrip doesn't count against your number of cantrips known. When you cast minor illusion, you can create both a sound and an image with a single casting of the spell.",
					RequiredLevel = 2,
					FeatureForClass = wizard,
					FeatureForArchetype = conjuration
				};
				Feature malleableIllusions = new Feature
				{
					FeatureName = "Malleable Illusions",
					FeatureDescription = "When you cast an illusion spell that has a duration of 1 minute or longer, you can use your action to change the nature of that illusion (using the spell’s normal parameters for the illusion), provided that you can see the illusion.",
					RequiredLevel = 6,
					FeatureForClass = wizard,
					FeatureForArchetype = conjuration
				};
				Feature illusorySelf = new Feature
				{
					FeatureName = "Illusory Self",
					FeatureDescription = "You can create an illusory duplicate of yourself as an instant, almost instinctual reaction to danger. When a creature makes an attack roll against you, you can use your reaction to interpose the illusory duplicate between the attacker and yourself. The attack automatically misses you, then the illusion dissipates. Once you use this feature, you can’t use it again until you finish a short or long rest.",
					RequiredLevel = 10,
					FeatureForClass = wizard,
					FeatureForArchetype = conjuration
				};
				Feature illusoryReality = new Feature
				{
					FeatureName = "Illusory Reality",
					FeatureDescription = "You have learned the secret of weaving shadow magic into your illusions to give them a semireality. When you cast an illusion spell of 1st level or higher, you can choose one inanimate, nonmagical object that is part of the illusion and make that object real. You can do this on your turn as a bonus action while the spell is ongoing. The object remains real for 1 minute. For example, you can create an illusion of a bridge over a chasm and then make it real long enough for your allies to cross. The object can’t deal damage or otherwise directly harm anyone.",
					RequiredLevel = 14,
					FeatureForClass = wizard,
					FeatureForArchetype = conjuration
				};
				Feature necromancySavant = new Feature
				{
					FeatureName = "Necromancy Savant",
					FeatureDescription = "The gold and time you must spend to copy an necromancy spell into your spellbook is halved.",
					RequiredLevel = 2,
					FeatureForClass = wizard,
					FeatureForArchetype = necromancy
				};
				Feature grimHarvest = new Feature
				{
					FeatureName = "Grim Harvest",
					FeatureDescription = "You gain the ability to reap life energy from creatures you kill with your spells. Once per turn when you kill one or more creatures with a spell of 1st level or higher, you regain hit points equal to twice the spell’s level, or three times its level if the spell belongs to the School of Necromancy. You don’t gain this benefit for killing constructs or undead.",
					RequiredLevel = 2,
					FeatureForClass = wizard,
					FeatureForArchetype = necromancy
				};
				Feature undeadThralls = new Feature
				{
					FeatureName = "Undead Thralls",
					FeatureDescription = "When you cast animate dead, you can target one additional corpse or pile of bones, creating another zombie or skeleton, as appropriate.\r\nWhenever you create an undead using a necromancy spell, it has additional benefits:\r\n • The creature’s hit point maximum is increased by an amount equal to your wizard level.\r\n • The creature adds your proficiency bonus to its weapon damage rolls.",
					RequiredLevel = 6,
					FeatureForClass = wizard,
					FeatureForArchetype = necromancy
				};
				Feature inuredToUndeath = new Feature
				{
					FeatureName = "Inured to Undeath",
					FeatureDescription = "You have resistance to necrotic damage, and your hit point maximum can't be reduced. You have spent so much time dealing with undead and the forces that animate them that you have become inured to some of their worst effects.",
					RequiredLevel = 10,
					FeatureForClass = wizard,
					FeatureForArchetype = necromancy
				};
				Feature commandUndead = new Feature
				{
					FeatureName = "Command Undead",
					FeatureDescription = "You can use magic to bring undead under your control, even those created by other wizards. As an action, you can choose one undead that you can see within 60 feet of you. That creature must make a Charisma saving throw against your wizard spell save DC. If it succeeds, you can’t use this feature on it again. If it fails, it becomes friendly to you and obeys your commands until you use this feature again. Intelligent undead are harder to control in this way. If the target has an Intelligence of 8 or higher, it has advantage on the saving throw. If it fails the saving throw and has an Intelligence of 12 or higher, it can repeat the saving throw at the end of every hour until it succeeds and breaks free.",
					RequiredLevel = 14,
					FeatureForClass = wizard,
					FeatureForArchetype = necromancy
				};
				Feature transmutationSavant = new Feature
				{
					FeatureName = "Transmutation Savant",
					FeatureDescription = "The gold and time you must spend to copy an transmutation spell into your spellbook is halved.",
					RequiredLevel = 2,
					FeatureForClass = wizard,
					FeatureForArchetype = transmutation
				};
				Feature minorAlchemy = new Feature
				{
					FeatureName = "Minor Alchemy",
					FeatureDescription = "Starting at 2nd level when you select this school, you can temporarily alter the physical properties of one nonmagical object, changing it from one substance into another. You perform a special alchemical procedure on one object composed entirely of wood, stone (but not a gemstone), iron, copper, or silver, transforming it into a different one of those materials. For each 10 minutes you spend performing the procedure, you can transform up to 1 cubic foot of material. After 1 hour, or until you lose your concentration (as if you were concentrating on a spell), the material reverts to its original substance.",
					RequiredLevel = 2,
					FeatureForClass = wizard,
					FeatureForArchetype = transmutation
				};
				Feature transmuterStone = new Feature
				{
					FeatureName = "Transmuter's Stone",
					FeatureDescription = "You can spend 8 hours creating a transmuter’s stone that stores transmutation magic. You can benefit from the stone yourself or give it to another creature. A creature gains a benefit of your choice as long as the stone is in the creature’s possession. When you create the stone, choose the benefit from the following options:\r\n • Darkvision out to a range of 60 feet\r\n • An increase to speed of 10 feet while the creature is unencumbered\r\n • Proficiency in Constitution saving throws\r\n • Resistance to acid, cold, fire, lightning, or thunder damage (your choice whenever you choose this benefit)\r\nEach time you cast a transmutation spell of 1st level or higher, you can change the effect of your stone if the stone is on your person. If you create a new transmuter’s stone, the previous one ceases to function.",
					RequiredLevel = 6,
					FeatureForClass = wizard,
					FeatureForArchetype = transmutation
				};
				Feature shapechanger = new Feature
				{
					FeatureName = "Shapechanger",
					FeatureDescription = "At 10th level, you add the polymorph spell to your spellbook, if it is not there already. You can cast polymorph without expending a spell slot. When you do so, you can target only yourself and transform into a beast whose challenge rating is 1 or lower. Once you cast polymorph in this way, you can’t do so again until you finish a short or long rest, though you can still cast it normally using an available spell slot.",
					RequiredLevel = 10,
					FeatureForClass = wizard,
					FeatureForArchetype = transmutation
				};
				Feature masterTransmuter = new Feature
				{
					FeatureName = "Master Transmuter",
					FeatureDescription = "Starting at 14th level, you can use your action to consume the reserve of transmutation magic stored within your transmuter’s stone in a single burst. When you do so, choose one of the following effects. Your transmuter’s stone is destroyed and can’t be remade until you finish a long rest.\r\nMajor Transformation\r\nYou can transmute one nonmagical object - no larger than a 5-foot cube - into another nonmagical object of similar size and mass and of equal or lesser value. You must spend 10 minutes handling the object to transform it.\r\nPanacea\r\nYou remove all curses, diseases, and poisons affecting a creature that you touch with the transmuter’s stone. The creature also regains all its hit points.\r\nRestore Life\r\nYou cast the raise dead spell on a creature you touch with the transmuter’s stone, without expending a spell slot or needing to have the spell in your spellbook.\r\nRestore Youth\r\nYou touch the transmuter’s stone to a willing creature, and that creature’s apparent age is reduced by 3d10 years, to a minimum of 13 years. This effect doesn’t extend the creature’s lifespan.",
					RequiredLevel = 14,
					FeatureForClass = wizard,
					FeatureForArchetype = transmutation
				};
				db.Features.Add(arcaneRecovery);
				db.Features.Add(spellMastery);
				db.Features.Add(signatureSpells);
				db.Features.Add(abjurationSavant);
				db.Features.Add(arcaneWard);
				db.Features.Add(projectedWard);
				db.Features.Add(improvedAbjuration);
				db.Features.Add(spellResistance);
				db.Features.Add(conjurationSavant);
				db.Features.Add(minorConjuration);
				db.Features.Add(benignTransposition);
				db.Features.Add(focusedConjuration);
				db.Features.Add(durableSummons);
				db.Features.Add(divinationSavant);
				db.Features.Add(portent);
				db.Features.Add(expertDivination);
				db.Features.Add(thirdEye);
				db.Features.Add(greaterPortent);
				db.Features.Add(enchantmentSavant);
				db.Features.Add(hypnoticGaze);
				db.Features.Add(instinctiveCharm);
				db.Features.Add(splitEnchantment);
				db.Features.Add(alterMemories);
				db.Features.Add(evocationSavant);
				db.Features.Add(sculptSpells);
				db.Features.Add(potentCantrip);
				db.Features.Add(empoweredEvocation);
				db.Features.Add(overchannel);
				db.Features.Add(illusionSavant);
				db.Features.Add(improvedMinorIllusion);
				db.Features.Add(malleableIllusions);
				db.Features.Add(illusorySelf);
				db.Features.Add(illusoryReality);
				db.Features.Add(necromancySavant);
				db.Features.Add(grimHarvest);
				db.Features.Add(undeadThralls);
				db.Features.Add(inuredToUndeath);
				db.Features.Add(commandUndead);
				db.Features.Add(transmutationSavant);
				db.Features.Add(minorAlchemy);
				db.Features.Add(transmuterStone);
				db.Features.Add(shapechanger);
				db.Features.Add(masterTransmuter);
				#endregion Wizard Features
				//Template for Features
				//ClassFeature xxx = new ClassFeature
				//{
				//	ClassFeatureName = "",
				//	ClassFeatureDescription = "",
				//	RequiredLevel = 1,
				//	FeatureForClass = ,
				//  Optional:
				//  FeatureForArchetype = 
				//};
				db.SaveChanges();
                Console.WriteLine("Succesfully added Class Features!");
                #endregion ClassFeatures
                #region BackgroundFeatures
                Console.WriteLine("Adding Background Features...");
				Feature shelterOfTheFaithful = new Feature
				{
					FeatureName = "Shelter of the Faithful",
					FeatureDescription = "As an acolyte, you command the respect of those who share your faith, and you can perform the religious ceremonies of your deity. You and your adventuring companions can expect to receive free healing and care at a temple, shrine, or other established presence of your faith, though you must provide any material components needed for spells.Those who share your religion will support you (but only you) at a modest lifestyle. You might also have ties to a specific temple dedicated to your chosen deity or pantheon, and you have a residence there. This could be the temple where you used to serve, if you remain on good terms with it, or a temple where you have found a new home. While near your temple, you can call upon the priests for assistance, provided the assistance you ask for is not hazardous and you remain in good standing with your temple.",
				};
				Feature falseIdentity = new Feature
				{
					FeatureName = "False Identity",
					FeatureDescription = "You have created a second identity that includes documentation, established acquaintances, and disguises that allow you to assume that persona. Additionally, you can forge documents including official papers and personal letters, as long as you have seen an example of the kind of document or the handwriting you are trying to copy."
				};
				Feature criminalContact = new Feature
				{
					FeatureName = "Criminal Contact",
					FeatureDescription = "You have a reliable and trustworthy contact who acts as your liaison to a network of other criminals. You know how to get messages to and from your contact, even over great distances; specifically, you know the local messengers, corrupt caravan masters, and seedy sailors who can deliver m essages for you.",
				};
				Feature byPopularDemand = new Feature
				{
					FeatureName = "By Popular Demand",
					FeatureDescription = "You can always find a place to perform, usually in an inn or tavern but possibly with a circus, at a theater, or even in a noble’s court. At such a place, you receive free lodging and food of a modest or comfortable standard (depending on the quality of the establishment), as long as you perform each night.In addition, your performance makes you something of a local figure. When strangers recognize you in a town where you have performed, they typically take a liking to you.",
				};
				Feature rusticHospitality = new Feature
				{
					FeatureName = "Rustic Hospitality",
					FeatureDescription = "Since you come from the ranks o f the common folk, you fit in among them with ease. You can find a place to hide, rest, or recuperate among other commoners, unless you have shown yourself to be a danger to them. They will shield you from the law or anyone else searching for you, though they will not risk their lives for you.",
				};
				Feature guildMembership = new Feature
				{
					FeatureName = "Guild Membership",
					FeatureDescription = "As an established and respected member of a guild, you can rely on certain benefits that membership provides. Your fellow guild members will provide you with lodging and food if necessary, and pay for your funeral if needed. In some cities and towns, a guildhall offers a central place to meet other members of your profession, which can be a good place to meet potential patrons, allies, or hirelings. Guilds often wield tremendous political power. If you are accused of a crime, your guild will support you if a good case can be made for your innocence or the crime is justifiable. You can also gain access to powerful political figures through the guild, if you are a member in good standing. Such connections might require the donation of money or magic items to the guild’s coffers. You must pay dues of 5gp per month to the guild. If you miss payments, you must make up back dues to remain in the guild’s good graces.",
				};
				Feature discovery = new Feature
				{
					FeatureName = "Discovery",
					FeatureDescription = "The quiet seclusion of your extended hermitage gave you access to a unique and powerful discovery. The exact nature of this revelation depends on the nature of your seclusion. It might be a great truth about the cosmos, the deities, the powerful beings of the outer planes, or the forces of nature. It could be a site that no one else has ever seen. You might have uncovered a fact that has long been forgotten, or unearthed some relic of the past that could rewrite history. It might be information that would be damaging to the people who or consigned you to exile, and hence the reason for your return to society. Work with your DM to determine the details of your discovery and its impact on the campaign.",
				};
				Feature positionOfPrivilege = new Feature
				{
					FeatureName = "Position of Privilege",
					FeatureDescription = "Thanks to your noble birth, people are inclined to think the best of you. You are welcome in high society, and people assume you have the right to be wherever you are. The common folk make every effort to accommodate you and avoid your displeasure, and other people of high birth treat you as a member of the same social sphere. You can secure an audience with a local noble if you need to.",
				};
				Feature wanderer = new Feature
				{
					FeatureName = "Wanderer",
					FeatureDescription = "You have an excellent memory for maps and geography, and you can always recall the general layout of terrain settlements, and other features around you. In addition, you can find food and fresh water for yourself and up to five other people each day, provided that the land offers berries, small game, water, and so forth.",
				};
				Feature researcher = new Feature
				{
					FeatureName = "Researcher",
					FeatureDescription = "When you attempt to learn or recall a piece of lore, if you do not know that information, you often know where and from whom you can obtain it. Usually, this information comes from a library, scriptorium, university, or a sage or other learned person or creature. Your DM might rule that the knowledge you seek is secreted away in an almost inaccessible place, or that it simply cannot be found. Unearthing the deepest secrets of the multiverse can require an adventure or even a whole campaign.",
				};
				Feature shipPassage = new Feature
				{
					FeatureName = "Ship's Passage",
					FeatureDescription = "When you need to, you can secure free passage on a sailing ship for yourself and your adventuring companions. You might sail on the ship you served on, or another ship you have good relations with (perhaps one captained by a former crewmate). Because you’re calling in a favor, you can’t be certain of a schedule or route that will meet your every need. Your Dungeon Master will determine how long it takes to get where you need to go. In return for your free passage, you and your companions are expected to assist the crew during the voyage.",
				};
				Feature militaryRank = new Feature
				{
					FeatureName = "Military Rank",
					FeatureDescription = "You have a military rank from your career as a soldier. Soldiers loyal to your former military organization still recognize your authority and influence, and they defer to you if they are of a lower rank. You can invoke your rank to exert influence over other soldiers and requisition simple equipment or horses for temporary use. You can also usually gain access to friendly military encampments and fortresses where your rank is recognized.",
				};
				Feature citySecrets = new Feature
				{
					FeatureName = "City Secrets",
					FeatureDescription = "You know the secret patterns and flow to cities and can find passages through the urban sprawl that others would miss. When you are not in combat, you (and companions you lead) can travel between any two locations in the city twice as fast as your speed would normally allow.",
				};
				db.Features.Add(shelterOfTheFaithful);
				db.Features.Add(falseIdentity);
				db.Features.Add(criminalContact);
				db.Features.Add(byPopularDemand);
				db.Features.Add(rusticHospitality);
				db.Features.Add(guildMembership);
				db.Features.Add(discovery);
				db.Features.Add(positionOfPrivilege);
				db.Features.Add(wanderer);
				db.Features.Add(researcher);
				db.Features.Add(shipPassage);
				db.Features.Add(militaryRank);
				db.Features.Add(citySecrets);
				db.SaveChanges();
                Console.WriteLine("Successfully added Background Features!");
                #endregion BackgroundFeatures
                #region Backgrounds
                Console.WriteLine("Adding Backgrounds...");
				Background acolyte = new Background
				{
					BackgroundName = "Acolyte",
					BackgroundSkillProficiencies = new List<Skill>
					{
						insight,
						religion
					},
					BackgroundFeature = shelterOfTheFaithful
				};
				Background charlatan = new Background
				{
					BackgroundName = "Charlatan",
					BackgroundSkillProficiencies = new List<Skill>
					{
						deception,
						sleightOfHand
					},
					BackgroundToolProficiencies = new List<Tool>
					{
						disguiseKit,
						forgeryKit
					},
					BackgroundFeature = falseIdentity
				};
				Background criminal = new Background
				{
					BackgroundName = "Criminal",
					BackgroundSkillProficiencies = new List<Skill>
					{
						deception,
						stealth
					},
					BackgroundToolProficiencies = new List<Tool> { thievesTools },
					BackgroundFeature = criminalContact
				};
				Background entertainer = new Background
				{
					BackgroundName = "Entertainer",
					BackgroundSkillProficiencies = new List<Skill>
					{
						acrobatics,
						performance
					},
					BackgroundToolProficiencies = new List<Tool> { disguiseKit },
					BackgroundFeature = byPopularDemand
				};
				Background folkHero = new Background
				{
					BackgroundName = "Folk Hero",
					BackgroundSkillProficiencies = new List<Skill>
					{
						animalHandling,
						survival
					},
					BackgroundToolProficiencies = new List<Tool>
					{
						vehicleLand
					},
					BackgroundFeature = rusticHospitality
				};
				Background guildArtisan = new Background
				{
					BackgroundName = "Guild Artisan",
					BackgroundSkillProficiencies = new List<Skill>
					{
						insight,
						persuasion
					},
					BackgroundFeature = guildMembership
				};
				Background hermit = new Background
				{
					BackgroundName = "Hermit",
					BackgroundSkillProficiencies = new List<Skill>
					{
						medicine,
						religion
					},
					BackgroundToolProficiencies = new List<Tool> { herbalismKit },
					BackgroundFeature = discovery
				};
				Background noble = new Background
				{
					BackgroundName = "Noble",
					BackgroundSkillProficiencies = new List<Skill>
					{
						history,
						persuasion
					},
					BackgroundFeature = positionOfPrivilege
				};
				Background outlander = new Background
				{
					BackgroundName = "Outlander",
					BackgroundSkillProficiencies = new List<Skill>
					{
						athletics,
						survival
					},
					BackgroundFeature = wanderer
				};
				Background sage = new Background
				{
					BackgroundName = "Sage",
					BackgroundSkillProficiencies = new List<Skill>
					{
						arcana,
						history
					},
					BackgroundFeature = researcher
				};
				Background sailor = new Background
				{
					BackgroundName = "Sailor",
					BackgroundSkillProficiencies = new List<Skill>
					{
						athletics,
						perception
					},
					BackgroundToolProficiencies = new List<Tool>
					{
						vehicleWater,
						navigatorTools
					},
					BackgroundFeature = shipPassage
				};
				Background soldier = new Background
				{
					BackgroundName = "Soldier",
					BackgroundSkillProficiencies = new List<Skill>
					{
						athletics,
						intimidation
					},
					BackgroundToolProficiencies = new List<Tool> { vehicleLand },
					BackgroundFeature = militaryRank
				};
				Background urchin = new Background
				{
					BackgroundName = "Urchin",
					BackgroundSkillProficiencies = new List<Skill>
					{
						sleightOfHand,
						stealth
					},
					BackgroundToolProficiencies = new List<Tool>
					{
						disguiseKit,
						thievesTools
					},
					BackgroundFeature = citySecrets
				};
				db.Backgrounds.Add(acolyte);
				db.Backgrounds.Add(charlatan);
				db.Backgrounds.Add(criminal);
				db.Backgrounds.Add(entertainer);
				db.Backgrounds.Add(folkHero);
				db.Backgrounds.Add(guildArtisan);
				db.Backgrounds.Add(hermit);
				db.Backgrounds.Add(noble);
				db.Backgrounds.Add(outlander);
				db.Backgrounds.Add(sage);
				db.Backgrounds.Add(sailor);
				db.Backgrounds.Add(soldier);
				db.Backgrounds.Add(urchin);
				db.SaveChanges();
                Console.WriteLine("Successfully added Backgrounds!");
				#endregion Backgrounds
			}
		}
	}
}