﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildDnDDatabase.Models
{
	class Armor
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ArmorId { get; set; }
		public string ArmorName { get; set; }
		public string ArmorType { get; set; }
		public int Cost { get; set; }
		public string AC { get; set; }
		public int Strength { get; set; }
		public bool StealthDisadvantage { get; set; }
		public float Weight { get; set; }

		//NavigationProperties
		public List<Class> ProficientClasses { get; set; }
		public List<Race> ProficientRaces { get; set; }
	}
}
