﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildDnDDatabase.Models
{
    class Feature
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FeatureId { get; set; }
        public string FeatureName { get; set; }
        public string FeatureDescription { get; set; }
        public int RequiredLevel { get; set; }

        //Navigation Properties

        public virtual List<Race> FeatureForRace { get; set; }
        public virtual List<Background> FeatureForBackground { get; set; }
        public virtual Class FeatureForClass { get; set; }
        public virtual ClassArchetype FeatureForArchetype { get; set; }
    }
}
