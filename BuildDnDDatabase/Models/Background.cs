﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BuildDnDDatabase.Models
{
	class Background
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int BackgroundId { get; set; }
		public string BackgroundName { get; set; }

		//Navigation Properties

		public virtual List<Weapon> BackgroundWeaponProficiencies { get; set; }
		public virtual List<Armor> BackgroundArmorProficiencies { get; set; }
		public virtual List<Tool> BackgroundToolProficiencies { get; set; }
		public virtual List<Skill> BackgroundSkillProficiencies { get; set; }
		public virtual List<Language> BackgroundLanguages { get; set; }
		public virtual Feature BackgroundFeature { get; set; }
	}
}
