﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildDnDDatabase.Models
{
	class AbilityBonus
	{
		public int Value { get; set; }

		//Navigation Properties
		[Key, Column(Order = 0)]
		public int AbilityId { get; set; }
		[Key, Column(Order = 1)]
		public int RaceId { get; set; }
		public virtual Ability Ability { get; set; }
		public virtual Race Race { get; set; }
	}
}
