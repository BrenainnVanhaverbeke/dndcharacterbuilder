﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildDnDDatabase.Models
{
	class Language
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int LanguageId { get; set; }
		public string LanguageName { get; set; }
		public string ScriptType { get; set; }

		//Navigation Properties

		public virtual List<Race> SpokenByRace { get; set; }
		public virtual List<Class> SpokenByClass { get; set; }
		public virtual List<Background> SpokenByBackground { get; set; }
	}
}
