﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildDnDDatabase.Models
{
	class MovementSpeed
	{
		public int Speed { get; set; }

		[Key, Column(Order = 0)]
		public int MovementTypeId { get; set; }
		[Key, Column(Order = 1)]
		public int RaceId { get; set; }
		public virtual MovementType MovementType { get; set; }
		public virtual Race Race { get; set; }
	}
}
