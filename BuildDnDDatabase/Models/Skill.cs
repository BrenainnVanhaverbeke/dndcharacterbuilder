﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BuildDnDDatabase.Models
{
	class Skill
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int SkillId { get; set; }
		public string SkillName { get; set; }
		public Ability ParentAbility { get; set; }

		//Navigation Properties
		public List<Class> ProficientClasses { get; set; }
		public List<Background> ProficientBackgrounds { get; set; }
		public List<Race> ProficientRaces { get; set; }
	}
}
