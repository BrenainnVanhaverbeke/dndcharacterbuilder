﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BuildDnDDatabase.Models
{ 
	class Class
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ClassId { get; set; }
		public string ClassName { get; set; }
		public int HitDie { get; set; }
		public int SkillCap { get; set; }

		//Navigation Properties

		public virtual List<ClassArchetype> ClassArchetypes { get; set; }
		public virtual List<Ability> ClassSavingThrowProficiencies { get; set; }
		public virtual List<Skill> ClassSkillProficiencies { get; set; }
		public virtual List<Weapon> ClassWeaponProficiencies { get; set; }
		public virtual List<Armor> ClassArmorProficiencies { get; set; }
		public virtual List<Tool> ClassToolProficiencies { get; set; }
		public virtual List<Language> ClassLanguages { get; set; }
        public virtual List<Feature> ClassFeatures { get; set; }
	}
}
