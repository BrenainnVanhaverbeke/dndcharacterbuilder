﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildDnDDatabase.Models
{
	class MovementType
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int MovementTypeId { get; set; }
		public string MovementName { get; set; }

		//Navigation Properties

		public virtual List<MovementSpeed> MovementSpeed { get; set; }
	}
}
