﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BuildDnDDatabase.Models;

namespace BuildDnDDatabase
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Starting Database deploy.");
			new FillDb().Run();
			Console.WriteLine("Finished Database deploy.");
            Console.ReadKey(true);
		}
	}
}
